<?php

namespace App\Helpers;

use App\Navigation;
use App\Page;
use App\PageCategory;
use App\Module;

class NavigationBuilder
{


    public function build($service = "", $service_slug = "")
    {

        if (request()->is_special_url) {
            $current_path = request()->raw_request_path;
        } else {
            $current_path = request()->path();
        }

        if ($current_path[0] == '/') {
            $current_path = ltrim($current_path, '/');
        }
        

        $navigation_raw = Navigation::where('id', '=', 1)->first();
        $navigation = json_decode($navigation_raw->navigation);

        $navigation_html = '';

        ///////active
        foreach ($navigation as $key => $element) {

            if ($element->href == $current_path) {
                $element->active = true;
            }

            if (isset($element->children)) {
                foreach ($element->children as $key_lvl1 => $children_lvl1) {
                    if ($children_lvl1->href == $current_path) {
                        $element->active = true;
                        $children_lvl1->active = true;
                    }

                    if (isset($children_lvl1->children)) {
                        foreach ($children_lvl1->children as $key_lvl2 => $children_lvl2) {

                            if ($children_lvl2->href == $current_path) {
                                $element->active = true;
                                $children_lvl1->active = true;
                                $children_lvl2->active = true;
                            }

                            if (isset($children_lvl2->children)) {
                                foreach ($children_lvl2->children as $key_lvl3 => $children_lvl3) {

                                    if ($children_lvl3->href == $current_path) {
                                        $element->active = true;
                                        $children_lvl1->active = true;
                                        $children_lvl2->active = true;
                                        $children_lvl3->active = true;
                                    }

                                    if (isset($children_lvl3->children)) {
                                        foreach ($children_lvl3->children as $key_lvl4 => $children_lvl4) {

                                            if ($children_lvl4->href == $current_path) {
                                                $element->active = true;
                                                $children_lvl1->active = true;
                                                $children_lvl2->active = true;
                                                $children_lvl3->active = true;
                                                $children_lvl4->active = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /////////////////////

            //lvl 1
            //echo $element->text."<br>";			
            if (!isset($element->children) ) {				
                $navigation_html = $navigation_html . '<li class="nav-item"><a class="' . (isset($element->active) && $element->active ? "active " : "") . 'nav-link" href="' . url($element->href) . '">' . $this->getElementDetails($element) . '</a></li>';				
            }

            if (isset($element->children)) {               
                $navigation_html = $navigation_html . '<li class="nav-item dropdown"><a class="' . (isset($element->active) && $element->active ? "active " : "") . 'nav-link" href="' . url($element->href) . '" data-target="' . url($element->href) . '" >' . $this->getElementDetails($element) . '</a>';

                if (sizeof($element->children) > 1) {
                    $navigation_html = $navigation_html . '<ul class="dropdown-menu">';

                    foreach ($element->children as $key_lvl1 => $children_lvl1) {
                        $navigation_html = $navigation_html . '<li><a class="' . (isset($children_lvl1->active) && $children_lvl1->active ? "active " : "") . 'dropdown-item" href="' . url($children_lvl1->href) . '">' . $children_lvl1->text . '</a></li>';
                        
                        //lvl2
                        //echo '-'.$children_lvl1->text."<br>";
                        /*if(isset($children_lvl1->children) && $this->display_item($children_lvl1, $service) ){

                            $navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl1->active) && $children_lvl1->active ? "active " : "").'dropdown-item" href="'.url($children_lvl1->href).'" data-target="'.url($children_lvl1->href).'">'.$children_lvl1->text.'</a>';
                            $navigation_html = $navigation_html.'<ul class="dropdown-menu">';

                            foreach($children_lvl1->children as $key_lvl2 => $children_lvl2){

                                if(!isset($children_lvl2->children) && $this->display_item($children_lvl2, $service)){
                                    $navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl2->active) && $children_lvl2->active ? "active " : "").'dropdown-item" href="'.url($children_lvl2->href).'">'.$children_lvl2->text.'</a></li>';
                                }

                                //lvl3
                                //echo '--'.$children_lvl2->text."<br>";
                                if(isset($children_lvl2->children) && $this->display_item($children_lvl2, $service)){

                                    $navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl2->active) && $children_lvl2->active ? "active " : "").'dropdown-item" href="'.url($children_lvl2->href).'" data-target="'.url($children_lvl2->href).'">'.$children_lvl2->text.'</a>';
                                    $navigation_html = $navigation_html.'<ul class="dropdown-menu">';

                                    foreach($children_lvl2->children as $key_lvl3 => $children_lvl3){

                                        if(!isset($children_lvl3->children) && $this->display_item($children_lvl3, $service)){
                                            $navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl3->active) && $children_lvl3->active ? "active " : "").'dropdown-item" href="'.url($children_lvl3->href).'">'.$element->module.$children_lvl3->text.'</a></li>';
                                        }

                                        //lvl4
                                        //echo '---'.$children_lvl3->text."<br>";
                                        if(isset($children_lvl3->children) && $this->display_item($children_lvl3, $service)){


                                            $navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl3->active) && $children_lvl3->active ? "active " : "").'dropdown-item" href="'.url($children_lvl3->href).'" data-target="'.url($children_lvl3->href).'">'.$children_lvl3->text.'</a>';
                                            $navigation_html = $navigation_html.'<ul class="dropdown-menu">';

                                            foreach($children_lvl3->children as $key_lvl4 => $children_lvl4){

                                                //lvl5
                                                //echo '-----'.$children_lvl4->text."<br>";
                                                $navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl4->active) && $children_lvl4->active ? "active " : "").'nav-link" href="'.url($children_lvl4->href).'">'.$children_lvl4->text.'</a></li>';

                                            }//lvl4 - foreach

                                            $navigation_html = $navigation_html.'</ul></li>';
                                        } //lvl4 - if
                                    }//lvl3 - foreach

                                $navigation_html = $navigation_html.'</ul></li>';
                                }//lvl3 - if
                            }//lvl2- foreach

                        $navigation_html = $navigation_html.'</ul></li>';
                        }//lvl2 - if*/

                    }//lvl1 - foreach


                    $navigation_html = $navigation_html . '</ul></li>';
                }//lvl1 - if
            }
        }

        return $navigation_html;
    }
      

    public function buildSideNavigation($category_href = null, $show_icons = false, $page_href = "")
    {

        if ($category_href != null) {
            $href = $category_href;
        } else {
            $href = request()->path();
        }

		if (request()->is_special_url) {
            $current_path = request()->raw_request_path;
        } else {
            $current_path = request()->path();
        }

        if ($current_path[0] == '/') {
            $current_path = ltrim($current_path, '/');
        }
				
        $side_nav = (new NavigationHelper())->navigationItems('side-nav', null, null, null, $href);

        if (is_array($side_nav)) {
            if (!$side_nav[0]) {
                return null;
            }
        } else if ($side_nav == null) {
            return null;
        }

        $navigation_html = '';
		
		if (sizeof($side_nav) > 1)  {			
			$itemCounter = 0;
			
			foreach ($side_nav as $key => $element) {
                $itemCounter++;
				
				$menu_icon = "";
				if ($show_icons)  {				
				   if ($element->module == "pages" && $element->type == "category")  {
					  // Page Category 
					  $objCategory = PageCategory::where('id', '=', str_replace("category-", "", $element->id))->first(); 	
					  $menu_icon_resp = (isset($objCategory->icon) ? '<div class="menu-img-resp"><img src="' . url('') . $objCategory->icon . '" alt="' . $element->text . '" title="' . $element->text . '"></div>' : "");		  
					  $menu_icon = (isset($objCategory->icon) ? '<div class="menu-img"><img src="' . url('') . $objCategory->icon . '" alt="' . $element->text . '" title="' . $element->text . '"></div>' : "");					  
				   } else if ($element->module == "pages" && $element->type == "page")  {
					 // Page
					 $objPage = Page::where('id', '=', str_replace("page-", "", $element->id))->first(); 	
					 $menu_icon_resp = (isset($objPage->icon) ? '<span class="menu-img-resp"><img src="' . url('') . $objPage->icon . '" alt="' . $element->text . '" title="' . $element->text . '"></span>' : "");		  
					 $menu_icon  = (isset($objPage->icon) ? '<div class="menu-img"><img src="' . url('') . $objPage->icon . '" alt="' . $element->text . '" title="' . $element->text . '"></div>' : "");	  
					 $menu_icon .= "<div class='menu-desc'>" . $objPage->short_description . "</div>";  
					 $menu_icon .= '<div class="menu-more">Learn More <img src="' . url('') . '/images/site/arrow-red.png" title="Arrow" alt="Arrow"></div>';  				 				 
				   }
				}

				//lvl 1				
				//echo $element->text."<br>";
				if (!isset($element->children)) {
					$style = '';
					if ($page_href == "" && $itemCounter == 1)  {
					   $style = 'active ';	
					} elseif ($page_href != "" & $element->href == $current_path) {
					   $style = 'active ';
					}

					$navigation_html = $navigation_html . '<li class="' . $style . 'list-group-item"><a class="navsidebar" href="' . url($element->href) . '">' . '<div class="menu-name">' . $menu_icon_resp . $element->text . '</div>'  . $menu_icon . '</a></li>';
				}

				if (isset($element->children)) {

					$style = '';
					if ($element->href == $href) {
						$style = 'active ';
					}

					$navigation_html = $navigation_html . '<li class="' . $style . 'list-group-item"><a class="navsidebar" href="' . url($element->href) . '">' . $element->text . $menu_icon . '</a>';
					$navigation_html = $navigation_html . '<ol class="list-group list-unstyled">';

					foreach ($element->children as $key_lvl1 => $children_lvl1) {

						if (!isset($children_lvl1->children)) {

							$style = '';
							if ($children_lvl1->href == $href) {
								$style = 'active ';
							}

							$navigation_html = $navigation_html . '<li class="' . $style . 'list-group-item"><a class="navsidebar" href="' . url($children_lvl1->href) . '">' . $children_lvl1->text . '</a></li>';
						}
						//lvl2
						//echo '-'.$children_lvl1->text."<br>";
						if (isset($children_lvl1->children)) {

							$style = '';
							if ($children_lvl1->href == $href) {
								$style = 'active ';
							}

							$navigation_html = $navigation_html . '<li class="' . $style . 'list-group-item"><a class="navsidebar" href="' . url($children_lvl1->href) . '">' . $children_lvl1->text . '</a>';
							$navigation_html = $navigation_html . '<ol class="list-group list-unstyled">';


							foreach ($children_lvl1->children as $key_lvl2 => $children_lvl2) {

								if (!isset($children_lvl2->children)) {

									$style = '';
									if ($children_lvl2->href == $href) {
										$style = 'active ';
									}

									$navigation_html = $navigation_html . '<li class="' . $style . 'list-group-item"><a class="navsidebar" href="' . url($children_lvl2->href) . '">' . $children_lvl2->text . '</a></li>';
								}

								//lvl3
								//echo '--'.$children_lvl2->text."<br>";
								if (isset($children_lvl2->children)) {

									$style = '';
									if ($children_lvl2->href == $href) {
										$style = 'active ';
									}

									$navigation_html = $navigation_html . '<li class="' . $style . 'list-group-item"><a class="navsidebar" href="' . url($children_lvl2->href) . '">' . $children_lvl2->text . '</a>';
									$navigation_html = $navigation_html . '<ol class="list-group list-unstyled">';

									foreach ($children_lvl2->children as $key_lvl3 => $children_lvl3) {

										if (!isset($children_lvl3->children)) {

											$style = '';
											if ($children_lvl3->href == $href) {
												$style = 'active ';
											}

											$navigation_html = $navigation_html . '<li class="' . $style . 'list-group-item"><a class="dropdown-item" href="' . url($children_lvl3->href) . '">' . $children_lvl3->text . '</a></li>';
										}

										//lvl4
										//echo '---'.$children_lvl3->text."<br>";
										if (isset($children_lvl3->children)) {

											$style = '';
											if ($children_lvl3->href == $href) {
												$style = 'active ';
											}

											$navigation_html = $navigation_html . '<li class="' . $style . 'list-group-item"><a class="dropdown-item" href="' . url($children_lvl3->href) . '">' . $children_lvl3->text . '</a>';
											$navigation_html = $navigation_html . '<ol class="list-group list-unstyled">';

											foreach ($children_lvl3->children as $key_lvl4 => $children_lvl4) {

												$style = '';
												if ($children_lvl4->href == $href) {
													$style = 'active ';
												}

												//lvl5
												//echo '-----'.$children_lvl4->text."<br>";
												$navigation_html = $navigation_html . '<li class="' . $style . 'list-group-item"><a href="' . url($children_lvl4->href) . '">' . $children_lvl4->text . '</a></li>';

											}

											$navigation_html = $navigation_html . '</ol></li>';
										}
									}

									$navigation_html = $navigation_html . '</ol></li>';
								}
							}

							$navigation_html = $navigation_html . '</ol></li>';
						}

					}

					$navigation_html = $navigation_html . '</ol></li>';
				}				
			}	
        }

        return $navigation_html;
    }

    public function build_raw()
    {
        $current_path = request()->path();

        $navigation_raw = Navigation::where('id', '=', 1)->first();
        $navigation = json_decode($navigation_raw->navigation);

        return ($navigation);
    }

    public function build_service($navigation)
    {
        //dd($navigation);
        foreach ($navigation as $key_lvl1 => $children_lvl1) {
            dd($children_lvl1);
        }
    }

    public function footerRaw($service = "", $service_slug = "")
    {

        $navigation_raw = Navigation::where('id', '=', 1)->first();
        $navigation = json_decode($navigation_raw->navigation);

        ///////active
        foreach ($navigation as $key => $element) {

            if ($element->type == 'category' || $element->type == 'module') {
                $element->display = true;
            } else {
                $element->display = false;
            }

            if (isset($element->children)) {
                foreach ($element->children as $key_lvl1 => $children_lvl1) {

                    if ($children_lvl1->type == 'category' || $children_lvl1->type == 'module') {
                        $children_lvl1->display = true;
                    } else {
                        $children_lvl1->display = false;
                    }
                }
            }
        }
        /////////////////////

        //dd($navigation);

        return $navigation;
    }
	
	public function getElementDetails($element)  {
		$navHeadline = "";
		$navSubHeadline = "";
		$navIcon = "";
		
		switch ($element->module . $element->type)  {			
				
			// Pages Module	- Categories
			case "pagescategory":
				 $obj = PageCategory::where('id', '=', str_replace("Pages-", "", str_replace("category-", "", $element->id)))->first(); 				   				
				 break;
			
			default:
				 $obj = Module::where('id', '=', str_replace("module-", "", $element->id))->first(); 				   				 
		}
			
		$navHeadline = "<div class='nav-headline'>" . (isset($obj->nav_headline) ? $obj->nav_headline : $element->text) . "</div>";	
		$navSubHeadline = (isset($obj->nav_subheadline) ? "<div class='nav-subheadline'>" . $obj->nav_subheadline . "</div>" : "");	
		$navIcon = (isset($obj->icon) ? '<img src="' . url('') . $obj->icon . '" alt="' . $element->text . '" title="' . $element->text . '" width="128" height="114">' : "");	
		
		if ($element->slug == "about")  {
		   $navHeadline = "<div class='nav-headline'>Hello!</div>";		
		   $navSubHeadline = (isset($obj->nav_subheadline) ? "<div class='nav-subheadline'>We're Echo3</div>" : "");	
		}
		
		$navItem =  "<div class='nav-item'>" . $navIcon . $navHeadline . $navSubHeadline . "</div>";
		return ($navItem);
	}
}

?>