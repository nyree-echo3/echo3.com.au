<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class NewsCategory extends Model
{
    use SortableTrait;

    protected $table = 'news_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function news()
    {
        return $this->hasMany(News::class, 'category_id')->where('status', '=', 'active');
    }
	
	public function active_news()
    {
        return $this->hasMany(News::class, 'category_id')->where('status', '=', 'active');
    }

    public function getBodyAttribute()
    {
        return preg_replace("/&nbsp;/",' ',$this->attributes['body']);
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','news')->where('type','=','category')->first();
        if($special_url){
            return $special_url->url;
        }

        return 'news/'.$this->attributes['slug'];
    }
}
