<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class News extends Model
{
    use Sortable;

    protected $table = 'news';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function category()
    {
        return $this->belongsTo(NewsCategory::class, 'category_id');
    }
	public function categoryactive()
    {
        return $this->belongsTo(NewsCategory::class, 'category_id')->where('status', '=', 'active');
    }
    public function categorysort()
    {
        return $this->hasOne(NewsCategory::class,'id','category_id');
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('news-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }

    public function getBodyAttribute()
    {
        return preg_replace("/&nbsp;/",' ',$this->attributes['body']);
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','news')->where('type','=','item')->first();
        if($special_url){
            return $special_url->url;
        }


        return 'news/'.$this->category->slug.'/'.$this->attributes['slug'];
    }
}
