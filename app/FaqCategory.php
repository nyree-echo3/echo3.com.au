<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class FaqCategory extends Model
{
    use SortableTrait;

    protected $table = 'faq_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function faqs()
    {
        return $this->hasMany(Faq::class, 'category_id')->where('status', '=', 'active');
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','faq')->where('type','=','category')->first();
        if($special_url){
            return $special_url->url;
        }

        return 'faq/'.$this->attributes['slug'];
    }
}
