<?php

namespace App\Http\Middleware;

use App\DocumentCategory;
use App\FaqCategory;
use App\GalleryCategory;
use App\News;
use App\NewsCategory;
use App\Page;
use App\PageCategory;
use App\Product;
use App\ProductCategory;
use App\Project;
use App\ProjectCategory;
use App\Property;
use App\PropertyCategory;
use App\SpecialUrl;
use App\TeamCategory;
use App\TeamMember;
use Closure;

class SpecialUrlRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->segment(1) == 'dreamcms') {
            return $next($request);
        }

        if($request->is_special_url==false){

            $redirects = array();

            $special_urls = SpecialUrl::all();

            foreach($special_urls as $special_url){

                ///documents module
                if($special_url->module == 'documents'){

                    if($special_url->type=='category'){

                        $document_category = DocumentCategory::where('id','=', $special_url->item_id)->first();

                        if($document_category){
                            $redirects[$special_url->url] = 'documents/'.$document_category->slug;
                        }
                    }
                }
                ////////

                ///FAQ module
                if($special_url->module == 'faq'){

                    if($special_url->type=='category'){

                        $faq_category = FaqCategory::where('id','=', $special_url->item_id)->first();
                        if($faq_category){
                            $redirects[$special_url->url] = 'faqs/'.$faq_category->slug;
                        }
                    }
                }
                ////////

                ///Gallery module
                if($special_url->module == 'gallery'){

                    if($special_url->type=='category'){

                        $gallery_category = GalleryCategory::where('id','=', $special_url->item_id)->first();

                        if($gallery_category){
                            $redirects[$special_url->url] = 'gallery/'.$gallery_category->slug;
                        }
                    }
                }
                ////////

                ///news module
                if($special_url->module == 'news'){

                    if($special_url->type=='item'){

                        $news = News::where('id','=', $special_url->item_id)->first();
                        if($news){
                            $redirects[$special_url->url] = 'blog/'.$news->category->slug.'/'.$news->slug;
                        }
                    }

                    if($special_url->type=='category'){

                        $news_category = NewsCategory::where('id','=', $special_url->item_id)->first();
                        if($news_category){
                            $redirects[$special_url->url] = 'blog/'.$news_category->slug;
                        }
                    }
                }
                ////////

                ///pages module
                if($special_url->module == 'pages'){

                    if($special_url->type=='item'){

                        $page = Page::where('id','=', $special_url->item_id)->first();
                        if($page) {
                            $redirects[$special_url->url] = 'pages/' . $page->category->slug . '/' . $page->slug;
                        }
                    }

                    if($special_url->type=='category'){

                        $page_category = PageCategory::where('id','=', $special_url->item_id)->first();
                        if($page_category){
                            $redirects[$special_url->url] = 'pages/'.$page_category->slug;
                        }
                    }
                }
                ////////

                ///products module
                if($special_url->module == 'products'){

                    if($special_url->type=='item'){

                        $product = Product::where('id','=', $special_url->item_id)->first();
                        if($product){
                            $redirects[$special_url->url] = 'products/'.$product->category->slug.'/'.$product->slug;
                        }
                    }

                    if($special_url->type=='category'){

                        $product_category = ProductCategory::where('id','=', $special_url->item_id)->first();
                        if($product_category){
                            $redirects[$special_url->url] = 'products/'.$product_category->slug;
                        }
                    }
                }
                ////////

                ///projects module
                if($special_url->module == 'projects'){

                    if($special_url->type=='item'){

                        $project = Project::where('id','=', $special_url->item_id)->first();
                        if($project){
                            $redirects[$special_url->url] = 'projects/'.$project->category->slug.'/'.$project->slug;
                        }
                    }

                    if($special_url->type=='category'){

                        $project_category = ProjectCategory::where('id','=', $special_url->item_id)->first();
                        if($project_category){
                            $redirects[$special_url->url] = 'projects/'.$project_category->slug;
                        }
                    }
                }
                ////////

                ///properties module
                if($special_url->module == 'properties'){

                    if($special_url->type=='item'){

                        $property = Property::where('id','=', $special_url->item_id)->first();
                        if($property){
                            $redirects[$special_url->url] = 'properties/'.$property->category->slug.'/'.$property->slug;
                        }
                    }

                    if($special_url->type=='category'){

                        $property_category = PropertyCategory::where('id','=', $special_url->item_id)->first();
                        if($property_category){
                            $redirects[$special_url->url] = 'properties/'.$property_category->slug;
                        }
                    }
                }
                ////////

                ///team module
                if($special_url->module == 'team'){

                    if($special_url->type=='item'){

                        $team_member = TeamMember::where('id','=', $special_url->item_id)->first();
                        if($team_member){
                            $redirects[$special_url->url] = 'team/'.$team_member->category->slug.'/'.$team_member->slug;
                        }
                    }

                    if($special_url->type=='category'){

                        $team_category = TeamCategory::where('id','=', $special_url->item_id)->first();
                        if($team_category){
                            $redirects[$special_url->url] = 'team/'.$team_category->slug;
                        }
                    }
                }
                ////////
            }

            $redirect_segment = array_search($request->path(), $redirects);

            if($redirect_segment){
                return redirect(env('APP_URL').'/'.$redirect_segment,301);
            }

            return $next($request);

        }

        $dup_request = $request->duplicate();

        $uri = $dup_request->raw_request_path;

        $find_web_public_index = strpos($uri, "web/public/index.php/");
        if($find_web_public_index != false){
            $new_uri = str_replace("web/public/index.php/","",$uri);
            return redirect(env('APP_URL').$new_uri,301);
        }else{

            $find_web_public = strpos($uri, "web/public/");
            if($find_web_public != false){
                $new_uri = str_replace("web/public/","",$uri);
                return redirect(env('APP_URL').$new_uri,301);
            }else{

                $find_index = strpos($uri, "index.php");

                if($find_index != false){
                    $new_uri = str_replace("index.php/","",$uri);
                    return redirect(env('APP_URL').$new_uri,301);
                }
            }
        }

        return $next($request);
    }
}