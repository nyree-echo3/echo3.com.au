<?php

namespace App\Http\Middleware;

use Closure;

class Redirects
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $redirects = [
			'faqs' => 'pages/about-us',
            'testimonials' => 'pages/about-us',
			'newsletters' => 'news',
			'pages/about' => 'pages/about-us',
			'archived-news' => 'news',
			'login.php' => 'campaign-login',
			'projects/branding' => 'projects',
			'image-gallery' => 'projects',
			'image-gallery/Marketing' => 'projects',
			'image-gallery/Email' => 'projects',
			'image-gallery/Logos' => 'projects',
			'projects/branding-design' => 'projects',
			'projects/marketing/4' => 'projects',
			'whats-a-new-customer-worth-to-you%E2%81%A0' => 'whats-a-new-customer-worth-to-you',			
			'404.php' => '',
			'projects/Signage/personaleyes-posters' => 'projects/branding--design/personaleyes-posters',
			'news/marketing-tips/being-found-locally-counts' => 'news/articles/being-found-locally-counts',
        ];



        foreach($redirects as $from => $to){

            if($request->path()==$from){

                return redirect($to,301);
            }
        }

        return $next($request);
    }
}
