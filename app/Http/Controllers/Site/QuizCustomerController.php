<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use App\Helpers\NavigationBuilder;
use App\ImagesHomeSlider;
use Illuminate\Support\Facades\Mail;
use App\Mail\SuitabilityQuizSuitable;
use App\Mail\SuitabilityQuizMaybeSuitable;
use App\PageCategory;
use App\QuizCustomer;

class QuizCustomerController extends Controller
{	
	public function index(){
        $settings = Setting::where('key', '=', 'quiz-customer')->first();
        $quiz_customer = $settings->value;    
				
        $side_nav = '';       				
		$builder = new NavigationBuilder();
		
    	return view('site/quiz-customer/index', array(         
			'category_name' => "How much is a <span>new customer</span> worth",
			'short_description' => "Sit back and we'll help you work it out.", 
			'header_image_ctrl' => "images/site/slider-calculator.png",
			'quiz_customer' => $quiz_customer,
			'navigation' => null,            
           ));	

    }
	
	public function indexOP(){
        $settings = Setting::where('key', '=', 'quiz-customer')->first();
        $quiz_customer = $settings->value;    
				
        $side_nav = '';       				
		$builder = new NavigationBuilder();
		
    	return view('site/quiz-customer/index-op', array(         
			'category_name' => "How much is a <span>new customer</span> worth",
			'short_description' => "Sit back and we'll help you work it out.", 
			'header_image_ctrl' => "images/site/slider-calculator.png",
			'quiz_customer' => $quiz_customer,
			'navigation' => null,            
           ));	

    }	
	
		public function successOP(){
        $settings = Setting::where('key', '=', 'quiz-customer')->first();
        $quiz_customer = $settings->value;    
				
        $side_nav = '';       				
		$builder = new NavigationBuilder();
		
    	return view('site/quiz-customer/success-op', array(         
			'category_name' => "How much is a <span>new customer</span> worth",
			'short_description' => "Sit back and we'll help you work it out.", 
			'header_image_ctrl' => "images/site/slider-calculator.png",
			'quiz_customer' => $quiz_customer,
			'navigation' => null,            
           ));	

    }	
	
	public function notSuitable(Request $request){
		$this->storeResult("not-suitable", $request);
		
		$settings = Setting::where('key', '=', 'suitability-quiz-not-suitable')->first();
        $suitability_quiz_not_suitable = $settings->value;    
		
		$settings = Setting::where('key', '=', 'suitability-quiz-ocular-herpes')->first();
        $ocular_herpes = $settings->value;    
		
		$settings = Setting::where('key', '=', 'suitability-quiz-retinal-disease')->first();
        $retinal_disease = $settings->value;    
		
    	return view('site/quiz-customer/not-suitable', array(         
			'category_name' => "",
			'header_image_ctrl' => "images/site/booking-header.jpg",
			'request' => $request,
			'suitability_quiz_not_suitable' => $suitability_quiz_not_suitable,
			'ocular_herpes' => $ocular_herpes,
			'retinal_disease' => $retinal_disease
           ));	

    }

	public function maybeSuitable(Request $request){
		$this->storeResult("maybe-suitable", $request);
		
		$settings = Setting::where('key', '=', 'suitability-quiz-maybe-suitable')->first();
        $suitability_quiz_maybe_suitable = $settings->value;    
		
		$settings = Setting::where('key', '=', 'suitability-quiz-keratoconus')->first();
        $keratoconus = $settings->value;    
		
		$settings = Setting::where('key', '=', 'suitability-quiz-cataracts')->first();
        $cataracts = $settings->value;   
		
		$this->sendEmailMaybeSuitable($request);
		
    	return view('site/suitability-quiz/maybe-suitable', array(         
			'category_name' => "",
			'header_image_ctrl' => "images/site/booking-header.jpg",
			'request' => $request,
			'suitability_quiz_maybe_suitable' => $suitability_quiz_maybe_suitable,
			'keratoconus' => $keratoconus,
			'cataracts' => $cataracts
           ));	

    }
	
	public function suitable(Request $request){
		$this->storeResult("suitable", $request);
		
		$settings = Setting::where('key', '=', 'suitability-quiz-suitable')->first();
        $suitability_quiz_suitable = $settings->value;  
		
		$settings = Setting::where('key', '=', 'suitability-quiz-keratoconus')->first();
        $keratoconus = $settings->value;    
		
		$settings = Setting::where('key', '=', 'suitability-quiz-cataracts')->first();
        $cataracts = $settings->value;   
		
		$this->sendEmailSuitable($request);
		
    	return view('site/suitability-quiz/suitable', array(         
			'category_name' => "",
			'header_image_ctrl' => "images/site/booking-header.jpg",
			'request' => $request,
			'suitability_quiz_suitable' => $suitability_quiz_suitable,
			'keratoconus' => $keratoconus,
			'cataracts' => $cataracts
           ));	

    }
	
	public function sendEmailSuitable(Request $request)
    {		
        Mail::to($request->txtEmail)->send(new SuitabilityQuizSuitable($request->txtFirstName));
    }
	
	public function sendEmailMaybeSuitable(Request $request)
    {		
		$eye_conditions = "";
		$eye_conditions_alt = "";
		$this_these = "this condition";
		
	    if ($request->chkEyeConditionKeratoconus == "Keratoconus" || $request->chkEyeConditionCataracts == "Cataracts") {                       
		    $eye_conditions = $request->chkEyeConditionKeratoconus  . ($request->chkEyeConditionKeratoconus != "" ? " and " : "") . " " . $request->chkEyeConditionCataracts; 
			$eye_conditions_alt = $request->chkEyeConditionKeratoconus  . ($request->chkEyeConditionKeratoconus != "" ? ", " : "") . " " . $request->chkEyeConditionCataracts;    
	    }
		if ($request->chkEyeConditionKeratoconus == "Keratoconus" || $request->chkEyeConditionCataracts == "Cataracts") {                       
		    $this_these = "these conditions";
	    }
		
        Mail::to($request->txtEmail)->send(new SuitabilityQuizMaybeSuitable($request->txtFirstName, $eye_conditions, $eye_conditions_alt, $this_these));
    }
	
	public function storeResult($status, Request $request)
    {    		
		$answer5  = "";
		$answer5 .= ($request->chkEyeConditionGlaucoma == 'Glaucoma' ? "," . $request->chkEyeConditionGlaucoma : "");
		$answer5 .= ($request->chkEyeConditionDryEyes == 'Dry eyes' ? "," . $request->chkEyeConditionDryEyes : "");
		$answer5 .= ($request->chkEyeConditionKeratoconus == 'Keratoconus' ? "," . $request->chkEyeConditionKeratoconus : "");
		$answer5 .= ($request->chkEyeConditionCornealScarring == 'Corneal Scarring' ? "," . $request->chkEyeConditionCornealScarring : "");
		$answer5 .= ($request->chkEyeConditionCataracts == 'Cataracts' ? "," . $request->chkEyeConditionCataracts : "");
		$answer5 .= ($request->chkEyeConditionOcularHerpes == 'Ocular herpes' ? "," . $request->chkEyeConditionOcularHerpes : "");
		$answer5 .= ($request->chkEyeConditionRetinalDisease == 'Retinal disease' ? "," . $request->chkEyeConditionRetinalDisease : "");
		$answer5 .= ($request->chkEyeConditionNoneOfThese == 'None of these' ? "," . $request->chkEyeConditionNoneOfThese : "");
		$answer5 .= ($request->chkEyeConditionNotSure == 'Not Sure' ? "," . $request->chkEyeConditionNotSure : "");
		
        $result = new SuitabilityQuiz();
        
        $result->result = $status;
        $result->answer1 = $request->radAgeGroup;
		$result->answer2 = $request->radGlassesContacts;
        $result->answer3 = $request->radAstigmatism;
        $result->answer4 = $request->radPrescription;
        $result->answer5 = $answer5;
        $result->state = $request->selState;       
		       
        $result->save();        
    }
}
