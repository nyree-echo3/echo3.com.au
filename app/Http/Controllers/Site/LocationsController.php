<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Location;
use App\Module;
use App\Popup;
use App\Service;
use Illuminate\Support\Facades\Cookie;

class LocationsController extends Controller
{
    public function index($mode = ""){		
		$module = Module::where('slug', '=', "locations")->first();
		
		$service_id = "";
		if (Cookie::get('services_slug') != "")  {
			$service = Service::where('slug', '=', Cookie::get('services_slug'))->first();	
			$service_id = $service->id;
	    }
		
		if ($service_id == "") {
		   $items = Location::where('status', '=', 'active')->orderBy('state', 'desc')->orderBy('name', 'asc')->get();
		} else  {
		   $items = Location::where('service_id', 'like', '%"' . $service_id . '"%')->where('status', '=', 'active')->orderBy('state', 'desc')->orderBy('name', 'asc')->get();	
		}
		
		$side_nav = (new NavigationBuilder())->buildSideNavigation();
		
		return view('site/locations/list', array(           
			'module' => $module,
			'items' => $items,
			'mode' => $mode,
			'side_nav' => $side_nav,
			'category_name' => $module->display_name,
        ));

    }
	
	public function item ($item_slug = "", $mode = "")
    {   
		if (Cookie::get('services_slug') != "")  {
			$service = Service::where('slug', '=', Cookie::get('services_slug'))->first();	
			$service_id = $service->id;
	    }
		
        $module = Module::where('slug', '=', "locations")->first();
		
		$items = Location::where('status', '=', 'active')->orderBy('position', 'asc')->get();
		$location = Location::where('status', '=', 'active')->where('slug', '=', $item_slug)->orderBy('position', 'asc')->first();
		
		$side_nav = (new NavigationBuilder())->buildSideNavigation();
		
		$popup = Popup::where('id', '=', $location->popup_type)->first(); 
		
		return view('site/locations/item', array(
            'module' => $module,
			'items' => $items,
			'location' => $location,
			'mode' => $mode,
			'side_nav' => $side_nav,
			'category_name' => $module->display_name,
			'popup' => $popup
        ));
    }
	
	public function getLocations(){
		$locations = Location::where('status', '=', 'active')->orderBy('state', 'desc')->orderBy('name', 'asc')->get();				
		return($locations);
	}
				
}
