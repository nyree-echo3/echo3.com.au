<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use App\Helpers\NavigationBuilder;
use App\Page;
use App\PageCategory;
use App\ImagesHomeSlider;
use App\Module;

class HomeController extends Controller
{
    public function index(){ 		
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
					
		$builder = new NavigationBuilder();		
	    $navigation = $builder->build();
	    $footer =  $builder->footerRaw();
		
		$images = ImagesHomeSlider::where('status', '=', 'active')->orderBy('position', 'desc')->get();      
		
		if (isset($_ENV['APP_MODE']) && $_ENV['APP_MODE'] == "dev")  {
		   return view('site/holding', array(         
			'company_name' => $company_name,			
           ));	
		   
		} else  {
    	   return view('site/index', array(			
			'navigation_override' => $navigation,
			'home_style_override' => true,
			'images_override' => $images,
			'footer_override' => $footer,
        ));
		}

    }
	
	public function indexDev(){ 			
		return view('site/index');			
    }
	
	public function styleGuide(){   		
    	return view('site/style-guide');
    }
	
	public function campaignLogin(){ 
		$module = Module::where('slug', '=', "contact")->first();
		
    	return view('site/campaign-login', array(
			'module' => $module,            
        ));
    }
}
