<?php

namespace App\Http\Controllers\Site;

use App\Donation;
use App\DonationCategory;

use App\Http\Controllers\Controller;
use App\Mail\DonationMessageAdmin;
use App\Mail\DonationMessageUser;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator, Illuminate\Support\Facades\Input, Redirect;

use Eway\Rapid;

class DonationsController extends Controller
{
    public function index(){       
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();       

		// Categories
		$categories = $this->getCategories();
		
		// Donation Form
		$fields = Setting::where('key', '=', 'contact-form-fields')->first();
		$form = $fields->value;
		
		// Amount
		$form = substr($form, 1);
		$form = '[{"type":"text","subtype":"text","required":true,"label":" Donation amount","className":"form-control","name":"donation","readonly":"readonly"},' . $form;		
		
		$donation = 0;
        if(old('donation')){			
            $fields = json_decode($form);
            foreach ($fields as $field){
                $field->value = old($field->name);
				
				if ($field->name == "donation") {
				   $donation = old($field->name);
				}
            }

            $form = json_encode($fields);
        }

		
        return view('site/donations/donation', array(           
            'company_name' => $company_name->value,
			'categories' => $categories,
			'form' => $form,
			'donation' => $donation,
        ));
    }

    public function saveDonation(Request $request)
    {	
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		
		// Save Donation
		// ***************
		$rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];
		
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('donation')->withErrors($validator)->withInput();
        }

        $fields = Setting::where('key', '=', 'donation-form-fields')->first();
		$form =  $fields->value;
		
		// Amount
		$form = substr($form, 1);
		$form = '[{"type":"text","subtype":"text","required":true,"label":" Donation amount","className":"form-control","name":"donation","readonly":"readonly"},' . $form;		
		
        $fields = json_decode($form);

        $data = array();

		$user_name = "Unknown";
		$user_email = "Unknown";
		
        foreach ($fields as $field){
            if($field->type!='header' && $field->type!='paragraph'){

                if($field->type=='date'){
                    $date_array = explode('-',$request->{$field->name});
                     $request->{$field->name} = $date_array[2].'-'.$date_array[1].'-'.$date_array[0];
                }
				
				if ($field->name=='name') {
                    $user_name = $request->{$field->name};                    
                }
				
				if ($field->name=='email') {
                    $user_email = $request->{$field->name};                    
                }

				$data_field['label'] = $field->label;
				$data_field['field'] = $field->name;
				$data_field['value'] = $request->{$field->name};
				array_push($data, $data_field);
            }
        }	
		
		$donation = new Donation();
        $donation->data = json_encode($data);
		$donation->amount = substr($request->donation, 1);
        $donation->save();
		
		
	     // Process Payment
		 // ***************
		 $apiKey = $_ENV['EWAY_API_KEY'];
         $apiPassword = $_ENV['EWAY_API_PASSWORD'];
         $apiEndpoint = $_ENV['EWAY_API_MODE'];
		
         $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);
		
		 // Order Items
		 $items = array();		 				
		 //$items[$arrSize]['SKU'] = 1;
		 $items[0]['Description'] = "Donation to " . $company_name;
		 $items[0]['Quantity'] = 1;
		 $items[0]['UnitCost'] = substr($request->donation, 1) * 100;			 

		 $orderTotal = substr($request->donation, 1) * 100;		
		 	
		 // Payment Transaction
		 $transaction = [
			'Customer' => [
				'Reference' => $donation->id,			
				'FirstName' => $user_name,
			    'Email' => $user_email,	
//				'LastName' => "Last 1",								
//				'Street1' => "Street 1",				
//				'City' => "City 1",
//				'State' => "VIC",
//				'PostalCode' => "1111",
//				'Country' => "AU",			    
//				'Phone' => '09 889 0986',
//				'Mobile' => '09 889 6542',							
			],
//			'ShippingAddress' => [
//				'ShippingMethod' => \Eway\Rapid\Enum\ShippingMethod::NEXT_DAY,
//				'FirstName' => "First 1",
//				//'LastName' => $order->billpayer->lastname,
//				'Street1' => "Street 1",			
//				'City' => "City 1",
//				'State' => "VIC",
//				'Country' => "AU",
//				'PostalCode' => "1111",
//				//'Phone' => '09 889 0986',
//			],
			'Items' => $items,
			'Payment' => [
				'TotalAmount' => $orderTotal,
				'InvoiceNumber' => $donation->id,
				'InvoiceDescription' => "Donation to " . $company_name,				
				'CurrencyCode' => 'AUD',
//			   'InvoiceReference' => '513456',
			],
			'RedirectUrl' => url('/') . "/donation/success/" . $donation->id,
			'CancelUrl' =>  url('/') . "/donation/error/" . $donation->id,			
			'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
			'Capture' => true,
			//'LogoUrl' => url('/') . "/images/site/logo.png",  // MUST START WITH HTTPS
			'HeaderText' => $company_name,
			'Language' => 'EN',
			'CustomerReadOnly' => true
		];
        //dd ($transaction);
		
		$response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::RESPONSIVE_SHARED, $transaction);
		//dd($response);
		
		if (!$response->getErrors()) {
			// Redirect to the Responsive Shared Page
			header('Location: '.$response->SharedPaymentUrl);
			die();
		} else {
			$errorMessage = "<span class='alert-error'>";
			foreach ($response->getErrors() as $error) {
				$errorMessage .= "<i class='fas fa-exclamation'></i> Error: ".\Eway\Rapid::getMessage($error)."<br>";
			}
			$errorMessage .= "<i class='fas fa-exclamation'></i> eWAY payment error.  Please <a href='" . url('/') . "/contact'>contact</a> website owner.";
			$errorMessage .= "</span>";
			
			flash($errorMessage);
			//return redirect()->route('checkout.show');
			return \Redirect::to('donation');
		}	
    }

    public function success($donation_id){
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		
		// Process Payment Return
		// **********************
		$apiKey = $_ENV['EWAY_API_KEY'];
        $apiPassword = $_ENV['EWAY_API_PASSWORD'];
        $apiEndpoint = $_ENV['EWAY_API_MODE'];		
		
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);    
		
		$ewayAccessCode = $_GET['AccessCode'];  
		
		$response = $client->queryTransaction($ewayAccessCode);
		//dd($response);

		// Update Donation
		$donation = Donation::where('id','=',$donation_id)->first();              
	    $donation->payment_type = "eWAY";
		$donation->payment_status = "completed";
		$donation->payment_transaction_number = $response->Transactions[0]->TransactionID;
		$donation->payment_transaction_result = ($response->Transactions[0]->ResponseCode == "00" ? "Successful" : "Error");
        $donation->save();
		
		if ($response->Transactions[0]->ResponseCode == "00")  {
		   $message = "Thanks for your donation.  Your donation number is: " . $donation_id . ".<br>We will send you an email confirmation of your donation.";
           flash($message);
		} else  {
		   $message = "Your payment was not successful.  Your unpaid donation number is: " . $donation_id . ".<br>We will send you an email confirmation of your unpaid donation.";	
		   flash("<span class='alert-error'><i class='fas fa-exclamation'></i> " . $message . "</span>");	
		}
		       

		// Send Emails
		// ***********		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		$fields = json_decode($donation->data);
	
		foreach ($fields as $field){      			
			if ($field->field=='email') {
				$user_email = $field->value;                    
			}							
        }	
		
		// Email Website Owner
        Mail::to($contactEmail)->send(new DonationMessageAdmin($donation));
		
		// Email User
        Mail::to($user_email)->send(new DonationMessageUser($donation));
        
		
		// Return 
		// ******
		return view('site/donations/success', array(
		    'company_name' => $company_name,
		));
    }
	
	public function getCategories(){
		$categories = DonationCategory::where('status', '=', 'active')->get();
		return($categories);
	}	

}
