<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\SpecialUrl;
use Illuminate\Http\Request;

use App\Page;
use App\PageCategory;
use App\Popup;
use App\Module;

class PagesController extends Controller
{
    public function category($category_slug, $mode = "")
    {
		$module = Module::where('slug', '=', "pages")->first();
		
        $category = $this->getCategory($category_slug)->first();
        $side_nav = (new NavigationBuilder())->buildSideNavigation(null, true);
		
//        $side_nav = null;
//        $sidebar_mode = '';
//        $side_nav_parent_text = '';
//       
//		$level = (new NavigationHelper())->navigationItems('get-level', 'category', 'pages', $category->slug);
//		if ($level != 1) {
//			$special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'pages')->where('type', '=', 'category')->first();
//			$side_nav = (new NavigationBuilder())->buildSideNavigation($special_url->url);
//			$side_nav_parent_text = (new NavigationHelper())->navigationItems('parent-text', null, null, null, $special_url->url);
//			$sidebar_mode = 'prepared';
//		}
//
//		if ($level == 1) {
//			$special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'pages')->where('type', '=', 'category')->first();
//			$side_nav = (new NavigationBuilder())->buildSideNavigation((isset($special_url->url) ? $special_url->url : $category->slug));
//			$side_nav_parent_text = $category->name;
//			$sidebar_mode = 'main';
//		}

//		$main_parent_text = (new NavigationHelper())->navigationItems('main-parent-text', 'category', 'pages', $category->slug);  
        	
        $builder = new NavigationBuilder();        
		$navigation = $builder->build();
		$footer = $builder->footerRaw();        
        
		return view('site/pages/category', array(
			'page_type' => "Pages",
			'side_nav' => $side_nav,
			//'side_nav_parent_text' => $side_nav_parent_text,
			//'sidebar_mode' => $sidebar_mode,
			'category' => $category,
			//'page' => $page,
			'mode' => $mode,
			'category_name' => $category->headline,
			'short_description' => $category->short_description,
			'header_image_ctrl' => $category->header			
		));     
    }

    public function getCategory($category_slug)
    {
        $categories = PageCategory::where('slug', '=', $category_slug)->orderBy('position', 'asc')->get();
        return ($categories);
    }

    public function getPages($category_id)
    {
        $pages = Page::where('status', '=', 'active')->where('category_id', '=', $category_id)->whereNull('parent_page_id')->orderBy('position', 'asc')->get();

        foreach ($pages as $page):
            $page['nav_sub'] = $this->getSubPages($page->id);
            $page['url'] = $page->url;
        endforeach;

        return ($pages);
    }

    public function getSubPages($page_id)
    {
        $pages = Page::where('status', '=', 'active')->where('parent_page_id', '=', $page_id)->orderBy('position', 'asc')->get();
        return ($pages);
    }

    public function index($category_slug, $page_slug = "", $mode = "")
    {
        $category = $this->getCategory($category_slug)->first();		
        $side_nav = (new NavigationBuilder())->buildSideNavigation("pages/".$category->slug, true, $page_slug);
        $page = ($page_slug == "" ? $this->getPages($category->id)->first() : $this->getPage($category->id, $page_slug, $mode));		

        //$side_nav = null;
        //$sidebar_mode = null;
        //$side_nav_parent_text = null;
        //$main_parent_text = '';
        
		//get level
		//$level = (new NavigationHelper())->navigationItems('get-level', 'page', 'pages', $page->slug);

//		if ($level == 3) {
//
//			$parent_href = (new NavigationHelper())->navigationItems('parent-href', 'page', 'pages', $page->slug);
//
//			//$special_url = SpecialUrl::where('item_id', '=', $page->category->id)->where('module', '=', 'pages')->where('type', '=', 'category')->first();
//			$side_nav = (new NavigationBuilder())->buildSideNavigation($parent_href);
//			$side_nav_parent_text = (new NavigationHelper())->navigationItems('parent-text', null, null, null, $parent_href);
//			$sidebar_mode = 'prepared';
//
//		} else if ($level != 1) {
//			//$special_url = SpecialUrl::where('item_id', '=', $page->id)->where('module', '=', 'pages')->where('type', '=', 'item')->first();
//			$side_nav = (new NavigationBuilder())->buildSideNavigation($page->url);
//			$side_nav_parent_text = (new NavigationHelper())->navigationItems('parent-text', null, null, null, $page->url);
//			$sidebar_mode = 'prepared';
//		}
//
//		$main_parent_text = (new NavigationHelper())->navigationItems('main-parent-text', 'page', 'pages', $page->slug);
//
//		if (!is_string($main_parent_text)) {
//			$main_parent_text = $page->title;
//		}
      

        $popup = Popup::where('id', '=', $page->popup_type)->first();        

//		$home_style_override = true;
//		$builder = new NavigationBuilder();
//
//		$navigation = $builder->build();
//		$footer = $builder->footerRaw();      

        return view('site/pages/pages', array(
            'page_type' => "Pages",
            'side_nav' => $side_nav,
            //'side_nav_parent_text' => $side_nav_parent_text,
            //'sidebar_mode' => $sidebar_mode,
            'category' => $category,
            'page' => $page,
            'mode' => $mode,            
            'popup' => $popup,            
			'category_name' => $category->headline,
			'short_description' => $category->short_description,
			'header_image_ctrl' => $category->header,
			'page_slug' => $page_slug,
        ));
    }

    public function getPage($category_id, $page_slug, $mode)
    {
        if ($mode == "preview") {
            $pages = Page::where('slug', '=', $page_slug)->where('category_id', '=', $category_id)->orderBy('position', 'asc')->first();
        } else {
            $pages = Page::where('status', '=', 'active')->where('slug', '=', $page_slug)->where('category_id', '=', $category_id)->orderBy('position', 'asc')->first();
        }

        return ($pages);
    }   
}
