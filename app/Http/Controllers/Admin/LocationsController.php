<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\Module;
use App\Location;
use App\Helpers\General;
use App\SpecialUrl;
use App\Popup;
use App\Service;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class LocationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'locations')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $locations = Location::Filter()->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        } else {
            $locations = Location::sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('locations-filter');
       
        return view('admin/locations/locations', array(
            'locations' => $locations,           
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {  
		$popups = Popup::orderBy('created_at', 'desc')->where('status', '=', 'active')->get();
		$services = Service::where('status', '=', 'active')->orderBy('title', 'asc')->get();
		
        return view('admin/locations/add', array(   
			'popups' => $popups,
			'services' => $services,
        ));
    }

    public function edit($location_id)
    {
        $location = Location::where('id', '=', $location_id)->first(); 
		$popups = Popup::orderBy('created_at', 'desc')->where('status', '=', 'active')->get();
		$services = Service::where('status', '=', 'active')->orderBy('title', 'asc')->get();
		
        return view('admin/locations/edit', array(
            'location' => $location, 
			'popups' => $popups,
			'services' => $services,
        ));
    }

	public function preview($faq_id)
    {
		$location = Location::where('id', '=', $faq_id)->first();		
		
		$general = new General();
		$view = $general->locationPreview($faq->category->slug, $faq->slug);	
		
        return ($view);
    }
	
    public function store(Request $request)
    {
        $rules = array(            
            'name' => 'required',  
			'slug' => 'required|unique_store:locations',
			'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
			'address' => 'required',      
			'suburb' => 'required',      
			'state' => 'required',      
			'postcode' => 'required',      
			'country' => 'required',      			
        );

        $messages = [           
            'name.required' => 'Please enter name',      
			'slug.required' => 'Please enter unique SEO Name',
			'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
			'address.required' => 'Please enter address',
			'suburb.required' => 'Please enter suburb',
			'state.required' => 'Please enter state',
			'postcode.required' => 'Please enter postcode',
			'country.required' => 'Please enter country',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/locations/add')->withErrors($validator)->withInput();
        }

        $location = new Location();       
        $location->name = $request->name;
		$location->slug = $request->slug;
		$location->meta_title = $request->meta_title;
        $location->meta_keywords = $request->meta_keywords;
        $location->meta_description = $request->meta_description;
		$location->service_id = json_encode($request->service_id);
		$location->description = $request->description;
        $location->address = $request->address;
		$location->address2 = $request->address2;
		$location->suburb = $request->suburb;
		$location->state = $request->state;
		$location->postcode = $request->postcode;
		$location->country = $request->country;
		$location->phone = $request->phone;
		$location->fax = $request->fax;
		$location->mobile = $request->mobile;
		$location->email = $request->email;
		$location->website = $request->website;		
		$location->fileName = $request->fileName;
        $location->thumbnail = $request->thumbnail;
		$location->map = $request->map;	
		$location->directions = $request->directions;
		$location->popup_type = $request->popup_type;
		$location->popup_start = $request->popup_start;
		$location->popup_end = $request->popup_end;
		$location->popup_position = $request->popup_position;

        if($request->live=='on'){
           $location->status = 'active'; 
        }

        $location->save();
   
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/locations/' . $location->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/locations/')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		}		        


    }

    public function update(Request $request)
    {
        $rules = array(            
            'name' => 'required', 
			'slug' => 'required|unique_update:locations,' . $request->id,
			'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
			'address' => 'required',      
			'suburb' => 'required',      
			'state' => 'required',      
			'postcode' => 'required',      
			'country' => 'required',      			
        );

        $messages = [          
            'name.required' => 'Please enter name',  
			'slug.required' => 'Please enter unique SEO Name',
			'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
			'address.required' => 'Please enter address',
			'suburb.required' => 'Please enter suburb',
			'state.required' => 'Please enter state',
			'postcode.required' => 'Please enter postcode',
			'country.required' => 'Please enter country',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/locations/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $location = Location::where('id','=',$request->id)->first();      
        $location->name = $request->name;
		$location->slug = $request->slug;
		$location->meta_title = $request->meta_title;
        $location->meta_keywords = $request->meta_keywords;
        $location->meta_description = $request->meta_description;
		$location->service_id = json_encode($request->service_id);
		$location->description = $request->description;
        $location->address = $request->address;
		$location->address2 = $request->address2;
		$location->suburb = $request->suburb;
		$location->state = $request->state;
		$location->postcode = $request->postcode;
		$location->country = $request->country;
		$location->phone = $request->phone;
		$location->fax = $request->fax;
		$location->mobile = $request->mobile;
		$location->email = $request->email;
		$location->website = $request->website;	
		$location->fileName = $request->fileName;
        //$location->thumbnail = $request->thumbnail;
		$location->map = $request->map;	
		$location->directions = $request->directions;
		$location->popup_type = $request->popup_type;
		$location->popup_start = $request->popup_start;
		$location->popup_end = $request->popup_end;
		$location->popup_position = $request->popup_position;
		
		if($request->live=='on'){
           $location->status = 'active'; 
		} else {
			$location->status = 'passive';
        }
        $location->save();
    
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/locations/' . $location->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/locations/')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		}
    }

    public function delete($location_id)
    {
        $location = Location::where('id','=',$location_id)->first();
        $location->is_deleted = true;
        $location->save();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeLocationStatus(Request $request, $location_id)
    {
        $location = Location::where('id', '=', $location_id)->first();
        if ($request->status == "true") {
            $location->status = 'active';
        } else if ($request->status == "false") {
            $location->status = 'passive';
        }
        $location->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $locations = Location::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/locations/sort', array(
            'locations' => $locations
        ));
    }
   
    public function emptyFilter()
    {
        session()->forget('locations-filter');
        return redirect()->to('dreamcms/locations');
    }

    public function isFiltered($request)
    {

        $filter_control = false;
       
        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('locations-filter', [                
                'search' => $request->search
            ]);
        }

        if (session()->has('locations-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

}