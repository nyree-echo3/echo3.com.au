<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Setting;

class SuitabilityQuizSuitable extends Mailable
{
    use Queueable, SerializesModels;

    public $first_name;

    public function __construct($first_name)
    {
        $this->first_name = $first_name;
    }

    public function build()
    {

        $setting = Setting::where('key','=','contact-email')->first();
        $contactEmail = $setting->value;
		
		$settings = Setting::where('key', '=', 'suitability-quiz-suitable')->first();
        $suitability_quiz_suitable = $settings->value;  
		
        return $this->subject('personalEYES | Suitability Quiz')
            ->from($contactEmail)
            ->view('site/emails/suitability-quiz-suitable', array(         
				'first_name' => $this->first_name,				
				'suitability_quiz_suitable' => $suitability_quiz_suitable				
			   ));	
    }
}
