<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageCategory extends Model
{
    protected $table = 'page_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function pages()
    {
        return $this->hasMany(Page::class, 'category_id')->where('status', '=', 'active')->orderBy('position', 'desc');
    }
	
	public function pagesnotsub()
    {
        return $this->hasMany(Page::class, 'category_id')->where('parent_page_id', '=', 'NULL')->where('status', '=', 'active');
    }
   
    public function getBodyAttribute()
    {
        return preg_replace("/&nbsp;/",' ',$this->attributes['body']);
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','pages')->where('type','=','category')->first();
        if($special_url){
            return $special_url->url;
        }

        return 'pages/'.$this->attributes['slug'];
    }
}
