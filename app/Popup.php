<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Popup extends Model
{  

    protected $table = 'popups';
   
    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }   
}
