<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class Faq extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'faqs';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function category()
    {
        return $this->belongsTo(FaqCategory::class, 'category_id');
    }

    public function categorysort()
    {
        return $this->hasOne(FaqCategory::class,'id','category_id');
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('faqs-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }

    public function getDescriptionAttribute()
    {
        return preg_replace("/&nbsp;/",' ',$this->attributes['description']);
    }
}
