<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class Location extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'locations';

    public $sortable = ['title', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }   

    public function scopeFilter($query)
    {

        $filter = session()->get('locations-filter');
        $select = "";
        
        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }
}
