<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class TeamMember extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'team_members';

    public $sortable = ['name', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function category()
    {
        return $this->belongsTo(TeamCategory::class, 'category_id');
    }

    public function categorysort()
    {
        return $this->hasOne(TeamCategory::class,'id','category_id');
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('team-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('name','like', '%'.$filter['search'].'%');
        }

        return $select;
    }

    public function getBodyAttribute()
    {
        return preg_replace("/&nbsp;/",' ',$this->attributes['body']);
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','team')->where('type','=','item')->first();
        if($special_url){
            return $special_url->url;
        }


        return 'team/'.$this->category->slug.'/'.$this->attributes['slug'];
    }
}
