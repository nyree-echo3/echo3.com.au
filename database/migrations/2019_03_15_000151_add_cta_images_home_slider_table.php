<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCtaImagesHomeSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images_home_slider', function (Blueprint $table) {
			$table->string('urlname')->nullable()->after('url');
			$table->string('url2')->nullable()->after('urlname');
			$table->string('urlname2')->nullable()->after('url2');
		});	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images_home_slider', function (Blueprint $table) {
            //
        });
    }
}
