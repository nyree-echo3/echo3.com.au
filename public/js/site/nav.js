const $body = document.querySelector('body');
const $btn = document.getElementById('btnNav');

console.log($body);

$btn.addEventListener('click', function(){
  
  $body.classList.toggle('show-menu');
  
});

// Remove Focus state on button on click
//
// This code still keeps the focus state for
// keyboard users..
// This code is from the following 
// stackoverflow answer:
// https://stackoverflow.com/questions/19053181/how-to-remove-focus-around-buttons-on-click

// Note: scroll down the page on the 
// above link...the first answer is not
// good for accessibility


// To remove the flashing outline when the user mouses
// down (the active state) but not when a keyboard
// user keyboards into the button (the focus state), 
// then add the following to your CSS
//
// button:active:focus { outline: none; }

$btn.addEventListener('mouseup', function(){
  this.blur();
});

