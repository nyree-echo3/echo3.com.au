
/*!
 * Bootstrap 4 multi dropdown navbar ( https://bootstrapthemes.co/demo/resource/bootstrap-4-multi-dropdown-navbar/ )
 * Copyright 2017.
 * Licensed under the GPL license
 */


$( document ).ready( function () {
	jQuery.noConflict();	
	
    jQuery( '.dropdown-menu a.dropdown-toggle' ).on( 'click', function ( e ) {
        var $el = jQuery( this );
        var $parent = jQuery( this ).offsetParent( ".dropdown-menu" );
        if ( !jQuery( this ).next().hasClass( 'show' ) ) {
            jQuery( this ).parents( '.dropdown-menu' ).first().find( '.show' ).removeClass( "show" );
        }
        var $subMenu = jQuery( this ).next( ".dropdown-menu" );
        $subMenu.toggleClass( 'show' );
        
        jQuery( this ).parent( "li" ).toggleClass( 'show' );

        jQuery( this ).parents( 'li.nav-item.dropdown.show' ).on( 'hidden.bs.dropdown', function ( e ) {
            jQuery( '.dropdown-menu .show' ).removeClass( "show" );
        } );
        
         if ( !jQueryparent.parent().hasClass( 'navbar-nav' ) ) {
            $el.next().css( { "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 } );
        }

        return false;
    } );
} );