// JavaScript Document
jQuery(window).scroll(function() {
  if (jQuery(window).scrollTop() > 300) {
      jQuery("#btnTopPage").fadeIn(3000);
  } else {
    jQuery("#btnTopPage").fadeOut();
  }
});

jQuery("#btnTopPage").on('click', function(e) {
  e.preventDefault();
  jQuery('html, body').animate({scrollTop:0}, '300');
});

