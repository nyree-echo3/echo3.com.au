$( document ).ready(function() {		
	setTimeout(function() {
		
	jQuery.noConflict();
	
	// Accordion
	// ******************************************	
	var icons = {
		header: "iconClosed",
		activeHeader: "iconOpen"
	};

	jQuery('.juiAccordion').accordion({
	//	heightStyle: "content",
		icons: icons,
		active: false,
        collapsible: true,
	});
	
	jQuery(".juiAccordion").accordion({
		beforeActivate: function(event, ui) { 
			//alert(ui.newHeader[0].id);	
			//alert(ui.newHeader.attr("aria-controls"));			
			jQuery('.juiAccordion .ui-accordion-content').hide();		
			jQuery('#' + ui.newHeader.attr("aria-controls")).height("auto").show();		
		}
    });
	
	// Add marketing text
	// ******************************************	
	htmlMarketing  = '<div class="moonray-form-element-wrapper">';
	htmlMarketing += '<div class="div_customer_worth">';
	htmlMarketing += '<h3>Every new customer is worth... drum roll please...</h3><div class="txt_customer_worth "><input type="textbox" name="txtCustomerLifetimeValue" value=""></div>';
	htmlMarketing += '<div class="div_contact_txt">';		
	htmlMarketing += "<p><b>Surprised?</b><br>This is the value each new customer brings you over their lifetime with you. Feel free to play around with different products or services you offer to see how your customer's lifetime value differs.</p>";  
	htmlMarketing += '<div class="hs-form-button">';
	htmlMarketing += '</div>';                                
	htmlMarketing += '</div>';
	htmlMarketing += '<div class="calculatorImg">';
	htmlMarketing += '<img src="https://www.echo3.com.au/images/site/calculator-owl.jpg" title="Owl" alt="Owl">';
	htmlMarketing += '</div>';
	htmlMarketing += '</div>';
	htmlMarketing += '<div class="div_contact_hdr">';
	htmlMarketing += '<h3>Want to know how we worked this out?</h3>';
	htmlMarketing += '</div>';
	htmlMarketing += '<div class="div_contact_hdr">';
	htmlMarketing += "<h4>Pop in your details below and we'll send you the calculator logic and a record of all your inputs.</h4>";
	htmlMarketing += '</div>';
    htmlMarketing += '</div>';
	
	jQuery(".moonray-form-element-wrapper:nth-child(10)").after(htmlMarketing);

	htmlDisclaimer  = '<div class="moonray-form-element-wrapper">';
	htmlDisclaimer += '<div class="div_contact_disclaimer">';
	htmlDisclaimer += "* We'll also occasionally send you newsletters or specials from Echo3. Your details are never shared & you can unsubscribe at any time.";
	htmlDisclaimer += '</div>';
    htmlDisclaimer += '</div>';
	
	jQuery(".moonray-form-element-wrapper:nth-child(16)").after(htmlDisclaimer);
	
	// Activate Question 1
	// ******************************************
	jQuery('.moonray-form-input').addClass('moonray-form-input-hidden');
	jQuery('.moonray-form-element-wrapper:nth-child(1)').addClass('hs-form-field-active');
	jQuery('.moonray-form-element-wrapper:nth-child(1) > .moonray-form-input').removeClass('moonray-form-input-hidden');
	
	// Add HTML - Button Calculate
	// ******************************************
	htmlBtnCalculate  = '<div class="div_calculate">';
	htmlBtnCalculate += '<a class="btnCompute" href="" id="btnQuestion1">Tell me how much a new customer is worth!</a>';
	htmlBtnCalculate += '<input type="hidden" id="hidCurrentQuestion" value="1">';
	htmlBtnCalculate += '</div>';
	
	jQuery(".moonray-form-element-wrapper:nth-child(9)").after(htmlBtnCalculate);	
	
	jQuery(".btnCompute").click(function(e){
		calculateCustomerWorthOG(); 
				
		jQuery(".div_customer_worth").addClass("show");
		jQuery(".calculatorImg").addClass("show");
		jQuery(".div_contact_hdr").addClass("show");
		
	    //jQuery(".moonray-form-element-wrapper:nth-child(11)").addClass("moonray-form-element-wrapper-show");
		jQuery(".moonray-form-element-wrapper:nth-child(12)").addClass("moonray-form-element-wrapper-show");
		jQuery(".moonray-form-element-wrapper:nth-child(13)").addClass("moonray-form-element-wrapper-show");
		jQuery(".moonray-form-element-wrapper:nth-child(14)").addClass("moonray-form-element-wrapper-show");
		jQuery(".moonray-form-element-wrapper:nth-child(15)").addClass("moonray-form-element-wrapper-show");
		jQuery(".moonray-form-element-wrapper:nth-child(16)").addClass("moonray-form-element-wrapper-show");
		jQuery(".moonray-form-element-wrapper:nth-child(17)").addClass("moonray-form-element-wrapper-show");
		jQuery(".moonray-form-element-wrapper:nth-child(18)").addClass("moonray-form-element-wrapper-show");
		
	    e.preventDefault();
    });
	
	// Activate a Question
	// ******************************************	
	jQuery(".moonray-form-element-wrapper").click(function(e){			
		prevQuestion = jQuery('#hidCurrentQuestion').val();
		currentQuestion = jQuery(this).index() + 1;	
		
		jQuery('.moonray-form-element-wrapper:nth-child(' + prevQuestion + ')').addClass('hs-form-field-answered');	

		jQuery('.moonray-form-element-wrapper:nth-child(' + prevQuestion + ')').removeClass('hs-form-field-active');	
		jQuery('.moonray-form-element-wrapper:nth-child(' + prevQuestion + ') > .moonray-form-input').addClass('moonray-form-input-hidden');

		jQuery('.moonray-form-element-wrapper:nth-child(' + currentQuestion + ')').addClass('hs-form-field-active');	
		if (currentQuestion != 4 && currentQuestion != 6 && currentQuestion != 7 && currentQuestion != 8 && currentQuestion != 9)  {
		   jQuery('.moonray-form-element-wrapper:nth-child(' + currentQuestion + ') > .moonray-form-input').removeClass('moonray-form-input-hidden');
		}

		//if (currentQuestion != 3)  {
		if (currentQuestion != prevQuestion)  {
		   jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(' + currentQuestion + ')').offset().top-100)},500);
		//} else  {
			//jQuery('.moonray-form-element-wrapper:nth-child(3) > .moonray-form-input').change();		
		}

		jQuery('#hidCurrentQuestion').val(currentQuestion);
		
		//e.stopPropagation();
		//e.preventDefault();
	});			
	
	// Range Sliders
	// ******************************************
	// Q4
	htmlRange4 = '<div class="input divRange"><input type="hidden" id="hidRange4" value="0"><div id="range4" class="divRangeScale"><div class="divRangeCentre">Number of times a year</div></div></div>';
	jQuery( ".moonray-form-element-wrapper:nth-child(4)" ).append(htmlRange4);				
	jQuery('#range4').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 1, min: 0, max: 12});
	jQuery('#range4').rangeSlider('onChange', event => updateRange4(event));
	
	// Q6
	htmlRange6 = '<div class="input divRange"><input type="hidden" id="hidRange6" value="0"><div id="range6" class="divRangeScale"></div><div class="divRangeCentre">Number of Years</div></div>';
	jQuery( ".moonray-form-element-wrapper:nth-child(6)" ).append(htmlRange6);											
	jQuery('#range6').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 1, min: 0, max: 10});
	jQuery('#range6').rangeSlider('onChange', event => updateRange6(event));

	// Q7
	htmlRange7 = '<div class="input divRange"><input type="hidden" id="hidRange7" value="0"><div id="range7" class="divRangeScale"></div><div class="divRangeLeft">Not Likely</div><div class="divRangeRight">Highly Likely</div></div>';
	jQuery( ".moonray-form-element-wrapper:nth-child(7)" ).append(htmlRange7);											
	jQuery('#range7').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 10, min: 0, max: 100});
	jQuery('#range7').rangeSlider('onChange', event => updateRange7(event));

	// Q8
	htmlRange8 = '<div class="input divRange"><input type="hidden" id="hidRange8" value="0"><div id="range8" class="divRangeScale"></div><div class="divRangeCentre">Number of referrals</div></div>';
	jQuery( ".moonray-form-element-wrapper:nth-child(8)" ).append(htmlRange8);											
	jQuery('#range8').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 1, min: 0, max: 10});
	jQuery('#range8').rangeSlider('onChange', event => updateRange8(event));

	// Q9
	htmlRange9 = '<div class="input divRange"><input type="hidden" id="hidRange9" value="0"><div id="range9" class="divRangeScale"></div><div class="divRangeLeft">Not Likely</div><div class="divRangeRight">Highly Likely</div></div>';
	jQuery( ".moonray-form-element-wrapper:nth-child(9)" ).append(htmlRange9);											
	jQuery('#range9').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 10, min: 0, max: 100});
	jQuery('#range9').rangeSlider('onChange', event => updateRange9(event));
	
	// Next Buttons
	// ******************************************
	jQuery( ".moonray-form-element-wrapper:nth-child(1)" ).append("<div class='btnNext'><a href='' id='btnQuestion1'><i class='fas fa-chevron-down'></i></a></div>");
	jQuery( ".moonray-form-element-wrapper:nth-child(2)" ).append("<div class='btnNext'><a href='' id='btnQuestion2'><i class='fas fa-chevron-down'></i></a></div>");
	jQuery( ".moonray-form-element-wrapper:nth-child(3)" ).append("<div class='btnNext'><a href='' id='btnQuestion3'><i class='fas fa-chevron-down'></i></a></div>");
	jQuery( ".moonray-form-element-wrapper:nth-child(4)" ).append("<div class='btnNext'><a href='' id='btnQuestion4'><i class='fas fa-chevron-down'></i></a></div>");
	jQuery( ".moonray-form-element-wrapper:nth-child(5)" ).append("<div class='btnNext'><a href='' id='btnQuestion5'><i class='fas fa-chevron-down'></i></a></div>");
	jQuery( ".moonray-form-element-wrapper:nth-child(6)" ).append("<div class='btnNext'><a href='' id='btnQuestion6'><i class='fas fa-chevron-down'></i></a></div>");
	jQuery( ".moonray-form-element-wrapper:nth-child(7)" ).append("<div class='btnNext'><a href='' id='btnQuestion7'><i class='fas fa-chevron-down'></i></a></div>");
	jQuery( ".moonray-form-element-wrapper:nth-child(8)" ).append("<div class='btnNext'><a href='' id='btnQuestion8'><i class='fas fa-chevron-down'></i></a></div>");
	jQuery( ".moonray-form-element-wrapper:nth-child(9)" ).append("<div class='btnNext'><a href='' id='btnQuestion9'><i class='fas fa-chevron-down'></i></a></div>");
	
	// Next Button Clicks
	//*******************
	// Q1
	jQuery("#btnQuestion1").click(function(e){	
		jQuery('.moonray-form-element-wrapper:nth-child(1)').removeClass('hs-form-field-active');	
		jQuery('.moonray-form-element-wrapper:nth-child(1) > .moonray-form-input').addClass('moonray-form-input-hidden');
		
		jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(2)').offset().top-100)},500);	
		setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(2)' ).addClass("hs-form-field-active") }, 500 );	
        setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(2) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );	
		
		jQuery('#hidCurrentQuestion').val(2);
		
		return false;
		//e.stopPropagation();
		//e.preventDefault();
	});

	// Q2
	jQuery("#btnQuestion2").click(function(e){	
		jQuery('.moonray-form-element-wrapper:nth-child(2)').removeClass('hs-form-field-active');	
		jQuery('.moonray-form-element-wrapper:nth-child(2) > .moonray-form-input').addClass('moonray-form-input-hidden');
		
		jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(3)').offset().top-100)},500);	
		setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(3)' ).addClass("hs-form-field-active") }, 500 );	
        setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(3) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );	

		jQuery('#hidCurrentQuestion').val(3);
		
		return false;
		//e.stopPropagation();
		//e.preventDefault();
	});

	// Q3
	jQuery("#btnQuestion3").click(function(e){		
		jQuery('.moonray-form-element-wrapper:nth-child(3)').removeClass('hs-form-field-active');	
		jQuery('.moonray-form-element-wrapper:nth-child(3) > .moonray-form-input').addClass('moonray-form-input-hidden');
		
		jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(4)').offset().top-100)},500);	
		setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(4)' ).addClass("hs-form-field-active") }, 500 );	
        setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(4) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );	
		
		jQuery('#hidCurrentQuestion').val(4);
		
		return false;
		//e.stopPropagation();
		//e.preventDefault();
	});

	// Q4
	jQuery("#btnQuestion4").click(function(e){	
		jQuery('.moonray-form-element-wrapper:nth-child(4)').removeClass('hs-form-field-active');	
		jQuery('.moonray-form-element-wrapper:nth-child(4) > .moonray-form-input').addClass('moonray-form-input-hidden');
		
		jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(5)').offset().top-100)},500);	
		setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(5)' ).addClass("hs-form-field-active") }, 500 );	
        setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(5) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );	

		jQuery('#hidCurrentQuestion').val(5);
		
		return false;
		//e.stopPropagation();
		//e.preventDefault();
	});

	// Q5
	jQuery("#btnQuestion5").click(function(e){	
		jQuery('.moonray-form-element-wrapper:nth-child(5)').removeClass('hs-form-field-active');	
		jQuery('.moonray-form-element-wrapper:nth-child(5) > .moonray-form-input').addClass('moonray-form-input-hidden');
		
		jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(6)').offset().top-100)},500);	
		setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(6)' ).addClass("hs-form-field-active") }, 500 );	
        setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(6) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );	

		jQuery('#hidCurrentQuestion').val(6);
		
		return false;
		//e.stopPropagation();
		//e.preventDefault();
	});

	// Q6
	jQuery("#btnQuestion6").click(function(e){	
		jQuery('.moonray-form-element-wrapper:nth-child(6)').removeClass('hs-form-field-active');	
		jQuery('.moonray-form-element-wrapper:nth-child(6) > .moonray-form-input').addClass('moonray-form-input-hidden');
		
		jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(7)').offset().top-100)},500);	
		setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(7)' ).addClass("hs-form-field-active") }, 500 );	
        setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(7) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );	

		jQuery('#hidCurrentQuestion').val(7);
		
		return false;
		//e.stopPropagation();
		//e.preventDefault();
	});

	// Q7
	jQuery("#btnQuestion7").click(function(e){	
		jQuery('.moonray-form-element-wrapper:nth-child(7)').removeClass('hs-form-field-active');	
		jQuery('.moonray-form-element-wrapper:nth-child(7) > .moonray-form-input').addClass('moonray-form-input-hidden');
		
		jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(8)').offset().top-100)},500);	
		setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(8)' ).addClass("hs-form-field-active") }, 500 );	
        setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(8) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );	
		
		jQuery('#hidCurrentQuestion').val(8);
		
		return false;
		//e.stopPropagation();
		//e.preventDefault();
	});

	// Q8
	jQuery("#btnQuestion8").click(function(e){	
		jQuery('.moonray-form-element-wrapper:nth-child(8)').removeClass('hs-form-field-active');	
		jQuery('.moonray-form-element-wrapper:nth-child(8) > .moonray-form-input').addClass('moonray-form-input-hidden');
		
		jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(9)').offset().top-100)},500);	
		setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(9)' ).addClass("hs-form-field-active") }, 500 );	
        setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(9) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );		

		jQuery('#hidCurrentQuestion').val(9);
		
		return false;
		//e.stopPropagation();
		//e.preventDefault();
	});

	// Q9
	jQuery("#btnQuestion9").click(function(e){	
		jQuery('.moonray-form-element-wrapper:nth-child(9)').removeClass('hs-form-field-active');	
		jQuery('.moonray-form-element-wrapper:nth-child(9) > .moonray-form-input').addClass('moonray-form-input-hidden');
		
		jQuery('html, body').animate({scrollTop: (jQuery('.div_calculate').offset().top-100)},500);			

		jQuery('#hidCurrentQuestion').val(10);
		
		return false;
		//e.stopPropagation();
		//e.preventDefault();
	});
	
	// Detect "enter" / "tab" keys and go to next question
	// ***************************************************
	// Q1
	jQuery(".moonray-form-element-wrapper:nth-child(1) > .moonray-form-input").keydown(function (e) {		
		if (e.keyCode == 13 || e.keyCode == 9) {	
		   jQuery('#hidCurrentQuestion').val(2);
			
		   jQuery('.moonray-form-element-wrapper:nth-child(1)').removeClass('hs-form-field-active');	
		   jQuery('.moonray-form-element-wrapper:nth-child(1) > .moonray-form-input').addClass('moonray-form-input-hidden');
			
		   jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(2)').offset().top-100)},500);	
		   setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(2)' ).addClass("hs-form-field-active") }, 500 );	
           setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(2) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );	
			
		   e.preventDefault();	
		}  
	});			

	// Q2
	jQuery(".moonray-form-element-wrapper:nth-child(2) > .moonray-form-input").keydown(function (e) {
		if (e.keyCode == 13 || e.keyCode == 9) {	
		   jQuery('#hidCurrentQuestion').val(3);
			
		   jQuery('.moonray-form-element-wrapper:nth-child(2)').removeClass('hs-form-field-active');	
		   jQuery('.moonray-form-element-wrapper:nth-child(2) > .moonray-form-input').addClass('moonray-form-input-hidden');
			
		   jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(3)').offset().top-100)},500);	
		   setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(3)' ).addClass("hs-form-field-active") }, 500 );	
           setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(3) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );	
			
		   e.preventDefault();	
		}  
	});

	//Q3
	jQuery(".moonray-form-element-wrapper:nth-child(3) > .moonray-form-input").change(function(e) {			
		jQuery('.moonray-form-element-wrapper:nth-child(3)').removeClass('hs-form-field-active');	
		jQuery('.moonray-form-element-wrapper:nth-child(3) > .moonray-form-input').addClass('moonray-form-input-hidden');		
		
		if (jQuery(this).val() == "No")  {	
		   jQuery('#hidCurrentQuestion').val(7);
			
		   jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(7)').offset().top-100)},500);	
		   setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(7)' ).addClass("hs-form-field-active") }, 500 );	
           setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(7) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );									  
		} else if (jQuery(this).val() == "Yes")  {		
		   jQuery('#hidCurrentQuestion').val(4);
			
		   jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(4)').offset().top-100)},500);	
		   setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(4)' ).addClass("hs-form-field-active") }, 500 );	
           setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(4) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );	
		} else  {
		   jQuery('#hidCurrentQuestion').val(3);
			
		   setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(3)' ).addClass("hs-form-field-active") }, 500 );	
           setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(3) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );	
		   jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(3)').offset().top-100)},500);			   
		}
		
		e.preventDefault();	
		
		return false;
        //e.stopPropagation();
		//e.preventDefault();
	});

	// Q4

	// Q5
	jQuery(".moonray-form-element-wrapper:nth-child(5) > .moonray-form-input").keydown(function (e) {
		if (e.keyCode == 13 || e.keyCode == 9) {
		   jQuery('#hidCurrentQuestion').val(6);
			
		   jQuery('.moonray-form-element-wrapper:nth-child(5)').removeClass('hs-form-field-active');	
		   jQuery('.moonray-form-element-wrapper:nth-child(5) > .moonray-form-input').addClass('moonray-form-input-hidden');
			
		   jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(6)').offset().top-100)},500);	
		   setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(6)' ).addClass("hs-form-field-active") }, 500 );	
           setTimeout( () => { jQuery('.moonray-form-element-wrapper:nth-child(6) > .moonray-form-input' ).removeClass("moonray-form-input-hidden") }, 500 );		
			
		   e.preventDefault();		
		}  
	});

	// Q6, Q7, Q8, Q9	
	
	// Highlight Questions inputs
	// ******************************************
	jQuery("#mr-field-element-146145460785").click(function () {
		jQuery(this).select();
	});
	
	jQuery("#mr-field-element-598594053174").click(function () {
		jQuery(this).select();
	});	
		
	}, 3000); //setTimeout	
});
	
// Question 4
function updateRange4(event)  {		
	jQuery("#mr-field-element-823534451296").val(event.detail.values[0]).change();										
	jQuery('#hidRange4').val(event.detail.values[0]);	
}

// Question 6
function updateRange6(event)  {									
	jQuery("#mr-field-element-958807409023").val(event.detail.values[0]).change();	
	jQuery('#hidRange6').val(event.detail.values[0]);									
}

// Question 7
function updateRange7(event)  {									
	jQuery("#mr-field-element-720365395892").val(event.detail.values[0]).change();
	jQuery('#hidRange7').val(event.detail.values[0]);										
}

// Question 8
function updateRange8(event)  {	
	jQuery('#mr-field-element-282573707829').val(event.detail.values[0]).change();
	jQuery('#hidRange8').val(event.detail.values[0]);																
}

// Question 9
function updateRange9(event)  {	
	jQuery('#mr-field-element-188572236905').val(event.detail.values[0]).change();
	jQuery('#hidRange9').val(event.detail.values[0]);																
}

function thousands_separators(num)  {
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
}

// Compute Customer Worth
function calculateCustomerWorthOG(btnCompute = false)  {		
	jQuery("#mr-field-element-823534451296").val(jQuery("#hidRange4").val());
	jQuery("#mr-field-element-958807409023").val(jQuery("#hidRange6").val());	
	jQuery("#mr-field-element-282573707829").val(jQuery("#hidRange7").val());	
	jQuery("#mr-field-element-720365395892").val(jQuery("#hidRange8").val());
	jQuery("#mr-field-element-188572236905").val(jQuery("#hidRange9").val());
	
	q2 = jQuery('#mr-field-element-146145460785').val();	
	q4 = jQuery('#mr-field-element-823534451296').val();
	q5 = jQuery('#mr-field-element-598594053174').val();
	q6 = jQuery('#mr-field-element-958807409023').val();
	q7 = jQuery('#mr-field-element-282573707829').val();
	q8 = jQuery('#mr-field-element-720365395892').val();
	q9 = jQuery('#mr-field-element-188572236905').val();	
	
	//alert("Answers: q2="+q2+" q4="+q4+" q5="+q5+" q6="+q6+" q7="+q7+" q8="+q8+" q9="+q9);
	
	if (q2 != "" && q4 != "" && q5 != "" && q6 != "" && q7 != "" && q8 != "" && q9 != "")  {
	   q2 = parseInt(q2);	  
	   q4 = parseInt(q4);
	   q5 = parseInt(q5);
	   q6 = parseInt(q6);
	   q7 = parseInt(q7) / 100;
	   q8 = parseInt(q8);
	   q9 = parseInt(q9) / 100;
		
	   //alert("Answers: q2="+q2+" q4="+q4+" q5="+q5+" q6="+q6+" q7="+q7+" q8="+q8+" q9="+q9);
		
	   // Answer 1 - Every new customer is worth	
	   customerWorth = (q2 + q4 * q5 * q6) * (1 + q7 * q8 * q9);	 	   		
	   customerWorth = parseInt(customerWorth);	 		
	   customerWorthFormat = thousands_separators(customerWorth);	 				   	
		
	   jQuery('#mr-field-element-777278225168').val(customerWorth);		
	   jQuery('input[name="txtCustomerLifetimeValue"]').val("$" + customerWorthFormat).change();
		
	   //$('.lead_value').html("$" + leadWorth);
	   jQuery('#mr-field-element-777278225168').addClass('hs-form-field-active');	
		   
	   jQuery('html, body').animate({scrollTop: (jQuery('.moonray-form-element-wrapper:nth-child(9)').offset().top+230)},500);
	   jQuery('.blog-masthead').addClass('blog-masthead-padding');	
	   jQuery('.calculatorImg').show();	
		
	   jQuery('.div_contact' ).addClass("show");
	   jQuery('.div_contact_submit' ).addClass("show");
	   jQuery('.div_contact_hdr' ).addClass("show");
		
	   // Answer 2 - Subsequent sales are worth
	   answer2 = q4 * q5 * q6;
	   answer2 = parseInt(answer2);	
		
	   jQuery('#mr-field-element-304531873398').val(answer2);		
		
	   // Answer 3 - Additional business from direct referrals is worth
	   answer3 = q7 * q9 * q8 * (q2 + answer2);
	   answer3 = parseInt(answer3);	 	  
		
	   jQuery('#mr-field-element-182534526752').val(answer3);		
		
	} else  {
	   if (btnCompute)  {	
	      alert("Please enter all values first.");	
	   }
	   jQuery('#mr-field-element-777278225168').val(''); 
	}
}
