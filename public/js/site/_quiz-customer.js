jQuery( document ).ready(function() {
	var icons = {
		header: "iconClosed",
		activeHeader: "iconOpen"
	};

	jQuery('.juiAccordion').accordion({
	//	heightStyle: "content",
		icons: icons,
		active: false,
        collapsible: true,
	});

	jQuery(".juiAccordion").accordion({
		beforeActivate: function(event, ui) { 
			//alert(ui.newHeader[0].id);	
			//alert(ui.newHeader.attr("aria-controls"));			
			jQuery('.juiAccordion .ui-accordion-content').hide();
			jQuery('#' + ui.newHeader.attr("aria-controls")).height("auto").show();
		}
   });
});


function calculateCustomerWorth(btnCompute = false)  {
	jQuery("input[type=number][name=customer_purchase_average]").val(jQuery("#hidRange3").val());
	jQuery("input[type=number][name=customer_lifetime]").val(jQuery("#hidRange4").val());	
	jQuery("input[type=number][name=customer_refer_rate]").val(jQuery("#hidRange6").val());	
	jQuery("input[type=number][name=customer_referrals]").val(jQuery("#hidRange7").val());			
	
	q2 = jQuery('input[name="average_sale_for_new_customer"]').val();	
	q4 = jQuery('input[name="customer_purchase_average"]').val();
	q5 = jQuery('input[name="purchase_value_average"]').val();
	q6 = jQuery('input[name="customer_lifetime"]').val();
	q7 = jQuery('input[name="customer_refer_rate"]').val();
	q8 = jQuery('input[name="customer_referrals"]').val();
	q9 = jQuery('input[name="referral_new_customer"]').val();						
	
	if (q2 != "" && q4 != "" && q5 != "" && q6 != "" && q7 != "" && q8 != "" && q9 != "")  {
	   q2 = parseInt(q2);	  
	   q4 = parseInt(q4);
	   q5 = parseInt(q5);
	   q6 = parseInt(q6);
	   q7 = parseInt(q7) / 100;
	   q8 = parseInt(q8);
	   q9 = parseInt(q9) / 100;
		
	   //alert("q2="+q2+" q4="+q4+" q5="+q5+" q6="+q6+" q7="+q7+" q8="+q8+" q9="+q9);
		
	   customerWorth = (q2 + q4 * q5 * q6) * (1 + q7 * q8);	 	   
	   customerWorth = parseInt(customerWorth);	 
	   customerWorthFormat = thousands_separators(customerWorth);	 	
		
	   jQuery('input[name="customer_worth"]').val(customerWorth).change();
		jQuery('input[name="txtCustomerLifetimeValue"]').val("$" + customerWorthFormat);
		
	   //jQuery('.lead_value').html("jQuery" + leadWorth);
	   jQuery('input[name="customer_worth"]').addClass('hs-form-field-active');	
		   
	   jQuery('html, body').animate({scrollTop: (jQuery('.div_customer_worth').offset().top-80)},500);
	   jQuery('.blog-masthead').addClass('blog-masthead-padding');	
	   jQuery('.calculatorImg').show();	
		
	   jQuery('.div_contact' ).addClass("show");
	   jQuery('.div_contact_submit' ).addClass("show");
	   jQuery('.div_contact_hdr' ).addClass("show");	
	} else  {
	   if (btnCompute)  {	
	      alert("Please enter all values first.");	
	   }
	   jQuery('input[name="customer_worth"]').val('').change(); 
	}
}

// Question 3
function updateRange3(event)  {		
	jQuery("input[type=number][name=customer_purchase_average]").val(event.detail.values[0]).change();										
	jQuery('#hidRange3').val(event.detail.values[0]);	
	//hidRange3_onchange();
}

// Question 4
function updateRange4(event)  {									
	jQuery("input[type=number][name=customer_lifetime]").val(event.detail.values[0]).change();	
	jQuery('#hidRange4').val(event.detail.values[0]);									
}

// Question 6
function updateRange6(event)  {									
	jQuery("input[type=number][name=customer_refer_rate]").val(event.detail.values[0]).change();
	jQuery('#hidRange6').val(event.detail.values[0]);										
}

// Question 7
function updateRange7(event)  {	
	jQuery('input[type=number][name=customer_referrals]').val(event.detail.values[0]).change();
	jQuery('#hidRange7').val(event.detail.values[0]);																
}

// Question 8
function updateRange8(event)  {	
	jQuery('input[type=number][name=referral_new_customer]').val(event.detail.values[0]).change();
	jQuery('#hidRange8').val(event.detail.values[0]);																
}

function thousands_separators(num)  {
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
}