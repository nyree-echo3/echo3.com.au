$(document).ready(function() {	
	jQuery.noConflict();	
	
    if (jQuery(window).width() > 575.98 )  {
		var toggleAffix = function(affixElement, scrollElement, wrapper) {

			var height = affixElement.outerHeight(),
				top = wrapper.offset().top;

			if (jQuery(window).scrollTop() > 0){
			//if (scrollElement.scrollTop() >= top){
				wrapper.height(0);
				affixElement.addClass("affix");
			}
			else {
				affixElement.removeClass("affix");
				wrapper.height('auto');
			}

		};


		jQuery('[data-toggle="affix"]').each(function() {
			var ele = jQuery(this),
				wrapper = jQuery('<div></div>');

			ele.before(wrapper);
			jQuery(window).on('scroll resize', function() {
				toggleAffix(ele, jQuery(this), wrapper);
			});

			// init
			toggleAffix(ele, jQuery(window), wrapper);
		});
	} else  {
			if (jQuery(window).scrollTop() > 0){						
				jQuery('header').addClass("affix-resp");
			} else {				
				jQuery('header').removeClass("affix-resp");				
			}				
	}
	
	jQuery(window).on('scroll resize', function() {
		if (jQuery(window).width() <= 575.98 )  {
			if (jQuery(window).scrollTop() > 0){						
				jQuery('header').addClass("affix-resp");
			} else {				
				jQuery('header').removeClass("affix-resp");				
			}				
		}
	});
});


