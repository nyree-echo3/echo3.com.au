$(document).ready(function() {		
    if ($(window).width() > 575.98 )  {
		var toggleAffix = function(affixElement, scrollElement, wrapper) {

			var height = affixElement.outerHeight(),
				top = wrapper.offset().top;

			if ($(window).scrollTop() > 0){
			//if (scrollElement.scrollTop() >= top){
				wrapper.height(0);
				affixElement.addClass("affix");
			}
			else {
				affixElement.removeClass("affix");
				wrapper.height('auto');
			}

		};


		$('[data-toggle="affix"]').each(function() {
			var ele = $(this),
				wrapper = $('<div></div>');

			ele.before(wrapper);
			$(window).on('scroll resize', function() {
				toggleAffix(ele, $(this), wrapper);
			});

			// init
			toggleAffix(ele, $(window), wrapper);
		});
	} else  {
			if ($(window).scrollTop() > 0){						
				$('header').addClass("affix-resp");
			} else {				
				$('header').removeClass("affix-resp");				
			}				
	}
	
	$(window).on('scroll resize', function() {
		if ($(window).width() <= 575.98 )  {
			if ($(window).scrollTop() > 0){						
				$('header').addClass("affix-resp");
			} else {				
				$('header').removeClass("affix-resp");				
			}				
		}
	});
});


