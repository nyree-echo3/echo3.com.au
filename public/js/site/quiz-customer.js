$( document ).ready(function() {
	var icons = {
		header: "iconClosed",
		activeHeader: "iconOpen"
	};

	$('.juiAccordion').accordion({
	//	heightStyle: "content",
		icons: icons,
		active: false,
        collapsible: true,
	});

	$(".juiAccordion").accordion({
		beforeActivate: function(event, ui) { 
			//alert(ui.newHeader[0].id);	
			//alert(ui.newHeader.attr("aria-controls"));			
			$('.juiAccordion .ui-accordion-content').hide();
			$('#' + ui.newHeader.attr("aria-controls")).height("auto").show();
		}
   });
});


function calculateCustomerWorth(btnCompute = false)  {
	$("input[type=number][name=customer_purchase_average]").val($("#hidRange3").val());
	$("input[type=number][name=customer_lifetime]").val($("#hidRange4").val());	
	$("input[type=number][name=customer_refer_rate]").val($("#hidRange6").val());	
	$("input[type=number][name=customer_referrals]").val($("#hidRange7").val());			
	
	q2 = $('input[name="average_sale_for_new_customer"]').val();	
	q4 = $('input[name="customer_purchase_average"]').val();
	q5 = $('input[name="purchase_value_average"]').val();
	q6 = $('input[name="customer_lifetime"]').val();
	q7 = $('input[name="customer_refer_rate"]').val();
	q8 = $('input[name="customer_referrals"]').val();
	q9 = $('input[name="referral_new_customer"]').val();						
	
	if (q2 != "" && q4 != "" && q5 != "" && q6 != "" && q7 != "" && q8 != "" && q9 != "")  {
	   q2 = parseInt(q2);	  
	   q4 = parseInt(q4);
	   q5 = parseInt(q5);
	   q6 = parseInt(q6);
	   q7 = parseInt(q7) / 100;
	   q8 = parseInt(q8);
	   q9 = parseInt(q9) / 100;
		
	   //alert("q2="+q2+" q4="+q4+" q5="+q5+" q6="+q6+" q7="+q7+" q8="+q8+" q9="+q9);
		
	   customerWorth = (q2 + q4 * q5 * q6) * (1 + q7 * q8);	 	   
	   customerWorth = parseInt(customerWorth);	 
	   customerWorthFormat = thousands_separators(customerWorth);	 	
		
	   $('input[name="customer_worth"]').val(customerWorth).change();
		$('input[name="txtCustomerLifetimeValue"]').val("$" + customerWorthFormat);
		
	   //$('.lead_value').html("$" + leadWorth);
	   $('input[name="customer_worth"]').addClass('hs-form-field-active');	
		   
	   $('html, body').animate({scrollTop: ($('.div_customer_worth').offset().top-80)},500);
	   $('.blog-masthead').addClass('blog-masthead-padding');	
	   $('.calculatorImg').show();	
		
	   $('.div_contact' ).addClass("show");
	   $('.div_contact_submit' ).addClass("show");
	   $('.div_contact_hdr' ).addClass("show");	
	} else  {
	   if (btnCompute)  {	
	      alert("Please enter all values first.");	
	   }
	   $('input[name="customer_worth"]').val('').change(); 
	}
}

// Question 3
function updateRange3(event)  {		
	$("input[type=number][name=customer_purchase_average]").val(event.detail.values[0]).change();										
	$('#hidRange3').val(event.detail.values[0]);	
	//hidRange3_onchange();
}

// Question 4
function updateRange4(event)  {									
	$("input[type=number][name=customer_lifetime]").val(event.detail.values[0]).change();	
	$('#hidRange4').val(event.detail.values[0]);									
}

// Question 6
function updateRange6(event)  {									
	$("input[type=number][name=customer_refer_rate]").val(event.detail.values[0]).change();
	$('#hidRange6').val(event.detail.values[0]);										
}

// Question 7
function updateRange7(event)  {	
	$('input[type=number][name=customer_referrals]').val(event.detail.values[0]).change();
	$('#hidRange7').val(event.detail.values[0]);																
}

// Question 8
function updateRange8(event)  {	
	$('input[type=number][name=referral_new_customer]').val(event.detail.values[0]).change();
	$('#hidRange8').val(event.detail.values[0]);																
}

function thousands_separators(num)  {
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
}