<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>Need an online shop fast? | Echo3</title>
	
	<meta name="keywords" content="Need an online shop fast? | Echo3">
    <meta name="description" content="Need an online shop fast? | Echo3">
    <meta name="author" content="Echo3 Media">
    <meta name="web_author" content="www.echo3.com.au">
    <meta name="date" content="01/04/2020" scheme="DD-MM-YYYY">
    
    <!-- Google Analytics -->
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-48309115-1', 'echo3.com.au');
	  ga('send', 'pageview');

	</script>
  
    <!-- Start of HubSpot Embed Code --> 
	<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4830934.js"></script> 
	<!-- End of HubSpot Embed Code -->
   
    <!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '1641499672684610');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=1641499672684610&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

    
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700" rel="stylesheet">   
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400,500,700" rel="stylesheet">
    <link href="/components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/landing/landing.min.css">
    
    <link rel="shortcut icon" href="/favicon.ico?">
    <link rel="apple-touch-icon" href="/apple-icon.png">
    
    <script src="/components/jquery/dist/jquery.min.js"></script>
    <script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
</head>

<body>

<div class="panel1">
    <div class="container mx-w-100">
        <div class="row">
           <div class="col-lg-12 col-md-12">
            	<div class="panelContact">        	
				   <a href="mailto:team@echo3.com.au" title="Email Us">                
					  <img src="/images/landing/icon-email.png" title="Email Us" alt="Email Us">                	
				   </a>	

				   <a href="tel:0391110011" title="Phone Us">                
					  <img src="/images/landing/icon-tel.png" title="Phone Us" alt="Phone Us">                	
				   </a>	
				</div>  
			</div>
				
			<div class="col-lg-7 col-md-12">
			    <div class="panelLogo">        	
				   <a href="https://www.echo3.com.au" title="Echo3 Media">                
					  <img src="/images/landing/logo-echo3.png" title="Echo3 Media" alt="Echo3 Media">                	
				   </a>	
				</div>								
			
				<h1><span>Need an online shop</span> fast<span class="span-red">?</span></h1>
				<p>We don't want to see the businesses around us close.</p>
				<p>In a bid to help, <b>we're offering 10 online shops at 50% discount</b>, to local businesses affected by COVID-19 in and around Doncaster, Manningham & Nillumbik.</p>	
			</div>
			
			<div class="col-lg-5 col-md-12">		        		    
			    <img src="/images/landing/main-image.png" title="Online Shop" alt="Online Shop" class="shop-img">  
			</div>
		</div>
	</div>
</div>

<div class="panel2">
    <div class="container">
        <div class="row">
			<div class="col-lg-5 col-md-12">
			    <div class="panel2-txt">
					<ul>
						<li class="tick">7-10 days delivery</li>
						<li class="tick">10 pages & 30 products setup and ready to go</li>
						<li class="tick">Built at cost as part of a community support program in this difficult time.</li>
						<!-- <li class="tick">Get trading for just $1,490 + 4 x monthly installments of $375/mth* (with discount)</li> -->
						<li class="tick">Payment plan available.</li>
						<li class="tick">Only 10 spots available!</li>
						<div class="link">Need a website without a shop? <a href="https://www.echo3.com.au/community-support-program-website">Click here</a></div>
					</ul>
					<div class="disclaimer">* Transactions fees payable to the selected Payment Gateway provider</div>
				</div>
			</div>
			
			<div class="col-lg-7 col-md-12">
		        <div class="landingForm-shop">
		            <!-- HubSpot Form -->
					<!--[if lte IE 8]>
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
					<![endif]-->
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
					<script>
					  hbspt.forms.create({
						portalId: "4830934",
						formId: "c4856228-9055-4dd2-875a-3d8059478b31"
					});
					</script>
				</div>
			     
			</div>
		</div>
	</div>
</div>

<div class="panel3">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
			   <div class="contact-logo"> 			          	
				   <a href="https://www.echo3.com.au" title="Echo3 Media">                
					  <img src="/images/landing/echo3-logo-bottom.png" title="Echo3 Media" alt="Echo3 Media">                	
				   </a>	
			   </div>
			      
			   <div class="contact-details">Echo3 Media Level 1, 128 Ayr Street, Doncaster VIC 3108<span>|</span>ABN 27 163 616 279</div>				   
			   <div class="contact-details"><strong>Phone</strong> <a href="tel:0391110011" title="Phone Us">9111 0011</a><span>|</span><strong>Email</strong> <a href="mailto:team@echo3.com.au" title="Email Us">team@echo3.com.au</a></div>
			   </div>
			</div>
        
			<div class="col-lg-12 col-md-12">
			    <div class="contact-local">Your local and trusted Digital Agency in Doncaster, serving the community since 2001.</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>