@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Modules</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/modules') }}"><i class="fa fa-question-circle"></i> Modules</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Module</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/modules/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $module->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('display_name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Display Name *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="display_name" placeholder="Display Name"
                                               value="{{ old('display_name',$module->display_name) }}">
                                        @if ($errors->has('display_name'))
                                            <small class="help-block">{{ $errors->first('display_name') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <hr>
                                <h4>Navigation</h4>
                                 <div class="form-group {{ ($errors->has('nav_headline')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Headline *</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="nav_headline" name="nav_headline" placeholder="Headline" value="{{ old('nav_headline',$module->nav_headline) }}">
                                        @if ($errors->has('nav_headline'))
                                            <small class="help-block">{{ $errors->first('nav_headline') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('nav_subheadline')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Sub Headline *</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="nav_subheadline" name="nav_subheadline" placeholder="Sub Headline" value="{{ old('nav_subheadline',$module->nav_subheadline) }}">
                                        @if ($errors->has('nav_subheadline'))
                                            <small class="help-block">{{ $errors->first('nav_subheadline') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('icon')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Icon Image</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="icon" name="icon" value="{{ old('icon',$module->icon) }}">
                                        <button id="icon-popup" type="button" class="btn btn-info btn-sm">Upload Image
                                        </button>
                                        @php
                                            $class = ' invisible';
                                            if($module->icon){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-icon" type="button" class="btn btn-danger btn-sm{{ $class }}">
                                            Remove Image
                                        </button>
                                        <br/><br/> <span id="added_icon">
									@if($module->icon)
                                                <image src="{{ url('').$module->icon }}"/>
                                            @endif
									</span>
                                        @if ($errors->has('icon'))
                                            <small class="help-block">{{ $errors->first('icon') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/modules') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
			
			$("#icon-popup").click(function () {
                openIconPopup();
            });

            $("#remove-icon").click(function () {
                $('#added_iicon').html('');
                $('#remove-icon').addClass('invisible')
                $('#icon').val('');
            });
        });
		
		function openIconPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_icon').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-icon').removeClass('invisible');
                        $('#icon').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_icon').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-icon').removeClass('invisible');
                        $('#icon').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection