@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/popups') }}"><i class="far fa-file-powerpoint"></i> {{ $display_name }}</a></li>
                <li class="active">Add New</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/popups/store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Title"
                                               value="{{ old('title') }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div id="fb-editor" class="box-body"></div>
                                
                                <div class="form-group {{ ($errors->has('image')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="image" name="image"
                                               value="{{ old('image') }}">
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
                                            Image
                                        </button>
                                        @php
                                            $class = ' invisible';
                                            if(old('image')){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-image" type="button"
                                                class="btn btn-danger btn-sm{{ $class }}">Remove Image
                                        </button>
                                        <br/><br/>
                                        <span id="added_image">
                                        @if(old('image'))
                                                <image src="{{ old('image') }}"/>
                                            @endif
                                        </span>
                                        @if ($errors->has('image'))
                                            <small class="help-block">{{ $errors->first('image') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <!--<div class="form-group">
                                    <label class="col-sm-2 control-label">Popup Position *</label>
                                    <div class="col-sm-10">
                                       <select id="position" name="position">
                                       	   <option value="top-left">Top Left</option>
                                       	   <option value="top-right">Top Rigth</option>
                                       	   <option value="bottom-left">Bottom Left</option>
                                       	   <option value="bottom-right">Bottom Right</option>
                                       </select>
                                    </div>
                                </div>-->
                                
                                @php
                                    $status = 'active';
                                    if(count($errors)>0){
                                       if(old('live')=='on'){
                                        $status = 'active';
                                       }else{
                                        $status = '';
                                       }
                                    }
                                @endphp
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status *</label>
                                    <div class="col-sm-10">
                                        <label>
                                            <input class="page_status" type="checkbox" data-toggle="toggle"
                                                   data-size="mini"
                                                   id="live" name="live" {{ $status == 'active' ? ' checked' : null }}>
                                        </label>
                                    </div>
                                </div>

                                <div class="box-footer">                                    
                                    <a href="{{ url('dreamcms/popups') }}" id="cancel" type="button" class="btn btn-info pull-right">Cancel</a>
                                    <button id="clear" type="button" class="btn btn-info pull-right">Clear</button>
                                    <button id="save" type="button" class="btn btn-info pull-right">Save</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/components/formBuilder/dist/form-builder.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();
           

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
			
			$("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#image').val('');
            });
			
			// Form Builder
			var options = {               
                disableFields : ['autocomplete','button','file','hidden'],
                disabledAttrs: ['access'],
                showActionButtons: false,
            };
            var formBuilder = $('#fb-editor').formBuilder(options);
			
			$('#save').click(function(){					
                $.ajax({
                    type: "POST",
                    url: '{{ url("dreamcms/popups/store") }}',
                    data:  {
                        'title':$('#title').val(),
						'live':$('#live').prop('checked'),
						'form':formBuilder.actions.getData('json'),
						'image':$('#image').val(),
						'position':$('#position').val(),
                    },					
                    success: function (response) {					
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('{{ $display_name }} form saved');
							$(location).attr('href', '{{ url("dreamcms/popups") }}')
                        }

                        if(response.status=="fail"){
                            toastr.options = {"closeButton": true}
                            toastr.error(response.message);
                        }
                    }
                });
            });
			
			$('#clear').click(function(){
                formBuilder.actions.clearFields();
            });

        });
		
		function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#image').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#image').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection