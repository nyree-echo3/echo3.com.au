@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/pages') }}"><i class="fas fa-file-alt"></i> {{ $display_name }}</a></li>
                <li><a href="{{ url('dreamcms/pages/categories') }}">Categories</a></li>
                <li class="active">Add New</li>
            </ol>
        </section>

        <section class="content">
            <div class="col-sm-12 col-md-10 col-lg-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Category</h3>
                    </div>

                    <form method="post" class="form-horizontal" action="{{ url('dreamcms/pages/store-category') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="box-body">

                            <div class="form-group {{ ($errors->has('name')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Category Name *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name"
                                           placeholder="Category Name" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <small class="help-block">{{ $errors->first('name') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ ($errors->has('slug')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">SEO Name *</label>
                                <div class="col-sm-10">

                                    <div class="input-group">
                                        <input type="text" id="slug" name="slug" class="form-control"
                                               value="{{ old('slug') }}" readonly>
                                        <span class="input-group-btn">
                                          <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                  data-target="#change-slug">Change SEO Name
                                          </button>
                                        </span>
                                    </div>

                                    @if ($errors->has('slug'))
                                        <small class="help-block">{{ $errors->first('slug') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ ($errors->has('special_url')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Special URL</label>

                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">{{ env('APP_URL') }}/</span>
                                        <input type="text" name="special_url" class="form-control" value="{{ old('special_url') }}">
                                    </div>                                    
                                    @if ($errors->has('special_url'))
                                        <small class="help-block">{{ $errors->first('special_url') }}</small>
                                    @endif
                                </div>
                            </div>                                                        
                                
                                <div class="form-group {{ ($errors->has('meta_title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Title</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" id="meta_title" name="meta_title" class="form-control"
                                                   placeholder="Meta Title"
                                                   value="{{ old('meta_title') }}" maxlength="56">
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      id="copy-title" name="copy-title">Copy From Name
                                              </button>
                                            </span>
                                        </div>
                                        <div id="c_count_meta_title">0 characters | 56 characters left</div>
                                        @if ($errors->has('meta_title'))
                                            <small class="help-block">{{ $errors->first('meta_title') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('meta_keywords')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Keywords</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="meta_keywords"
                                                  placeholder="Meta Keywords">{{ old('meta_keywords') }}</textarea>
                                        @if ($errors->has('meta_keyword'))
                                            <small class="help-block">{{ $errors->first('meta_keywords') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ ($errors->has('meta_description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Description</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="meta_description" name="meta_description"
                                                  maxlength="250"
                                                  placeholder="Meta Description">{{ old('meta_description') }}</textarea>
                                        <div id="c_count_meta_description">0 characters | 250 characters left | 0
                                            words
                                        </div>
                                        @if ($errors->has('meta_description'))
                                            <small class="help-block">{{ $errors->first('meta_description') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('body')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Body</label>

                                    <div class="col-sm-10">
                                        <textarea id="body" name="body" rows="10" cols="80"
                                                  style="height: 500px;">{{ old('body') }}</textarea>
                                        @if ($errors->has('body'))
                                            <small class="help-block">{{ $errors->first('body') }}</small>
                                        @endif
                                    </div>
                                </div>

                            <div class="form-group {{ ($errors->has('thumbnail')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Header Image</label>
                                <div class="col-sm-10">
                                    <input type="hidden" id="header" name="header" value="{{ old('header') }}">
                                    <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload Image
                                    </button>
                                    @php
                                        $class = ' invisible';
                                        if(old('header')){
                                            $class = '';
                                        }
                                    @endphp
                                    <button id="remove-image" type="button" class="btn btn-danger btn-sm{{ $class }}">
                                        Remove Image
                                    </button>
                                    <br/><br/>
                                    <span id="added_image">
									@if(old('header'))
                                            <image src="{{ url('dreamcms/').old('header') }}"/>
                                        @endif
									</span>
                                    @if ($errors->has('header'))
                                        <small class="help-block">{{ $errors->first('header') }}</small>
                                    @endif
                                </div>
                            </div>  
                                                                                                                                                                    
                            <hr>
							<h3 class="box-title">Navigation</h3>

							<div class="form-group {{ ($errors->has('nav_headline')) ? ' has-error' : '' }}">
								<label class="col-sm-2 control-label">Headline *</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="nav_headline" name="nav_headline" placeholder="Headline" value="{{ old('nav_headline') }}">
									@if ($errors->has('nav_headline'))
										<small class="help-block">{{ $errors->first('nav_headline') }}</small>
									@endif
								</div>
							</div>

							<div class="form-group {{ ($errors->has('nav_subheadline')) ? ' has-error' : '' }}">
								<label class="col-sm-2 control-label">Sub Headline *</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="nav_subheadline" name="nav_subheadline" placeholder="Sub Headline" value="{{ old('nav_subheadline') }}">
									@if ($errors->has('nav_subheadline'))
										<small class="help-block">{{ $errors->first('nav_subheadline') }}</small>
									@endif
								</div>
							</div>
                                
                           <div class="form-group {{ ($errors->has('icon')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Icon Image</label>
                                <div class="col-sm-10">
                                    <input type="hidden" id="icon" name="icon" value="{{ old('icon') }}">
                                    <button id="icon-popup" type="button" class="btn btn-info btn-sm">Upload Image
                                    </button>
                                    @php
                                        $class = ' invisible';
                                        if(old('icon')){
                                            $class = '';
                                        }
                                    @endphp
                                    <button id="remove-icon" type="button" class="btn btn-danger btn-sm{{ $class }}">
                                        Remove Image
                                    </button>
                                    <br/><br/>
                                    <span id="added_icon">
									@if(old('icon'))
                                            <image src="{{ url('dreamcms/').old('icon') }}"/>
                                        @endif
									</span>
                                    @if ($errors->has('icon'))
                                        <small class="help-block">{{ $errors->first('icon') }}</small>
                                    @endif
                                </div>
                            </div>                                                        

                           
                            <div class="box-footer">
                                <a href="{{ url('dreamcms/pages/categories') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal" value="{{ old('slug') }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace('body');
			
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('#name').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
                $('#slug').val(slug);
            });

            $('#slug-modal').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
            });

            $("#save-seo").click(function () {
                var slug = $('#slug-modal').val();
                $('#slug').val(slug);
                $('#change-slug').modal('toggle');
            });

            $("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#header').val('');
            });
			
			$("#icon-popup").click(function () {
                openIconPopup();
            });

            $("#remove-icon").click(function () {
                $('#added_icon').html('');
                $('#remove-icon').addClass('invisible')
                $('#icon').val('');
            });
			
			var maxLengthTitle = 56;
            $('#meta_title').keydown(function () {
                var textlen = maxLengthTitle - $(this).val().length;
                $('#c_count_meta_title').text($(this).val().length + " characters | " + textlen + " characters left");

            });

            var maxLengthDescription = 250;
            $('#meta_description').keyup(function () {
                var textlen = maxLengthDescription - $(this).val().length;
                var words = $(this).val().trim().split(" ").length;
                $('#c_count_meta_description').text($(this).val().length + " characters | " + textlen + " characters left | " + words + " words");
            });
			
			$('#copy-title').click(function () {
                $('#meta_title').val($('#name').val().substr(0, maxLengthTitle));
                $('#meta_title').trigger("keyup", {which: 50});
            });
        });

        function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#header').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#header').val(evt.data.resizedUrl);
                    });
                }
            });
		}
		
		function openIconPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_icon').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-icon').removeClass('invisible');
                        $('#icon').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_icon').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-icon').removeClass('invisible');
                        $('#icon').val(evt.data.resizedUrl);
                    });
                }
            });	
		}
			
		
    </script>
@endsection
