@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/ui-darkness/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Home Page Sliders</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                <li class="active">Home Page Slider</li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    @can('edit-home-page-slider')
                    <div class="pull-right box-tools">
                        <a href="{{ url('dreamcms/settings/add-home-slider') }}" type="button" class="btn btn-info btn-sm"
                           data-widget="add">Add New Image
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                    @endcan
                </div>
                <div class="box-body" id="sortable">
                    @if(count($images))

                        @foreach($images as $image)
                        <div class="image-box" id='item_{{ $image->id }}'>
                            <div class="image-box-toolbox">

                                @can('edit-home-page-slider')
                                <a href="{{ url('dreamcms/settings/'.$image->id.'/edit-home-slider') }}" class="tool" data-toggle="tooltip" title="Edit">
                                    <i class="fa fa-edit"></i>
                                </a>
                                @endcan
                                @can('delete-home-page-slider')
                                <a href="{{ url('dreamcms/settings/'.$image->id.'/delete-home-slider') }}"
                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                @endcan
                                @can('edit-home-page-slider')
                                <div class="pull-right" style="margin-right: 10px">
                                  <input id="img_{{ $image->id }}" data-id="{{ $image->id }}" class="image_status" type="checkbox" data-toggle="toggle" data-size="mini"{{ $image->status == 'active' ? ' checked' : null }}>
                               </div>
                               @endcan

                            </div>
                            <div class="image-box-img">
                                <image src="{{ url('/').$image->location }}" class="image-item" />
                            </div>
                            <div class="image-box-footer">                               
                            </div>
                        </div>
                        @endforeach

                    @else
                        No records
                    @endif
                </div>                
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $( "#sortable" ).sortable({
                update: function(event, ui) {
                    $.ajax({
                        type:'POST',
                        url:'/dreamcms/settings/image-sort-home-slider',
                        data: $(this).sortable('serialize'),
                        success:function(response){
                            if(response.status=="success"){
                                toastr.options = {"closeButton": true}
                                toastr.success('Saved');
                            }
                        }
                    });
                }
            });

            $("#pagination_count").select2({
                minimumResultsForSearch: -1
            });

            $("#pagination_count").change(function() {
                $("#pagination_count_form").submit();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('.image_status').change(function() {			
                $.ajax({
                    type: "POST",
                    url: "/dreamcms/settings/"+$(this).data('id')+"/change-status-home-slider",
                    data:  {
                        'status':$(this).prop('checked')
                    },
                    success: function (response) {
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('Status has been changed');
                        }
                    }
                });
            });

        });
    </script>
@endsection