@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Settings</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                <li class="active">Social Media</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Social Media Values</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/settings/social-media-update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">

                                <div class="form-group {{ ($errors->has('social_facebook')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Facebook</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="social_facebook" placeholder="Facebook" value="{{ $social_facebook->value }}">
                                        @if ($errors->has('social_facebook'))
                                            <small class="help-block">{{ $errors->first('social_facebook') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('social_twitter')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Twitter</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="social_twitter" placeholder="Twitter" value="{{ $social_twitter->value }}">
                                        @if ($errors->has('social_twitter'))
                                            <small class="help-block">{{ $errors->first('social_twitter') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('social_linkedin')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">LinkedIn</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="social_linkedin" placeholder="LinkedIn" value="{{ $social_linkedin->value }}">
                                        @if ($errors->has('social_linkedin'))
                                            <small class="help-block">{{ $errors->first('social_linkedin') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('social_googleplus')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Google Plus</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="social_googleplus" placeholder="Google Plus" value="{{ $social_googleplus->value }}">
                                        @if ($errors->has('social_googleplus'))
                                            <small class="help-block">{{ $errors->first('social_googleplus') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('social_instagram')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Instagram</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="social_instagram" placeholder="Instagram" value="{{ $social_instagram->value }}">
                                        @if ($errors->has('social_instagram'))
                                            <small class="help-block">{{ $errors->first('social_instagram') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('social_pinterest')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Pinterest</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="social_pinterest" placeholder="Pinterest" value="{{ $social_pinterest->value }}">
                                        @if ($errors->has('social_pinterest'))
                                            <small class="help-block">{{ $errors->first('social_pinterest') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('social_youtube')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Youtube</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="social_youtube" placeholder="Youtube" value="{{ $social_youtube->value }}">
                                        @if ($errors->has('social_youtube'))
                                            <small class="help-block">{{ $errors->first('social_youtube') }}</small>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {           
			$('.datepicker').datepicker({
              autoclose: true,
			  format: 'dd/mm/yyyy'
            });
        });
    </script>
@endsection