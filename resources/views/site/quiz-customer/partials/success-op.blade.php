@section('styles')	  
    <link rel="stylesheet" href="{{ asset('css/site/quiz-customer.css?v2.56') }}">
	<link rel="stylesheet" href="{{ asset('css/site/quiz-customer-op.css?v1.1') }}">		
@endsection

<div class="blog-masthead ">
	<div class="container">

		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 blog-main blog-calculator">
			    <div class="blog-post">					
										
					<div id="divCalculator">
					    <div class="divCalculator-h2">Calculate my Customer Lifetime Value (CLV)</div>		
					    
						<div class="submitted-message">
							<p>Thanks for submitting your inputs. Your results will be emailed to you shortly! In the meantime, would you like to try the <a href="{{ url('') }}/whats-a-new-customer-worth-to-you-op">calculator again</a> for one of your other products or services?</p>
						</div>																													
					</div><!-- /.divCalculator -->
												
					</div>								
				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->	
</div><!-- /.blog-masthead -->
