@section('styles')	  
    <link rel="stylesheet" href="{{ asset('css/site/rangeSlider.css') }}">
	<link rel="stylesheet" href="{{ asset('css/site/quiz-customer.css?v2.58') }}">		
@endsection


	<div class="blog-masthead ">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 blog-main blog-calculator">
				<div class="blog-post">
					<div id="divCalculatorText">
					{!! $quiz_customer !!}
					</div>

					<div id="divCalculator">
					    <div class="divCalculator-h2">Calculate my Customer Lifetime Value (CLV)</div>

						<!-- Hubspot Form -->
						<!--[if lte IE 8]>
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
						<![endif]-->
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						<script>
						  hbspt.forms.create({
							portalId: "4830934",
							formId: "e4cdbbe0-8f53-4648-bf14-510e43599715",
							onFormReady: function($form, ctx){
								testt();
							}  
						});
						</script>
					</div><!-- /.divCalculator -->
												
					</div>								
				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
    
    <!--
    <div class="calculatorImg">
	   <img src="{{ url('') }}/images/site/calculator-owl.jpg" title="Owl" alt="Owl">
	</div>
	--> 
	
</div><!-- /.blog-masthead -->

@section('inline-scripts-quiz-customer')
	<script src="{{ asset('js/site/quiz-customer.js?v5.00') }}"></script>
	<script src="{{ asset('js/site/rangeSlider.js?v=1.00') }}"></script>
	<script>

		function testt(){
			$('.hs-product_or_service_offering').addClass('hs-form-field-active');

			htmlRange3 = '<div class="input divRange"><input type="hidden" id="hidRange3" value="0" onchange="hidRange3_onchange()"><div id="range3" class="divRangeScale"><div class="divRangeCentre">Number of times a year</div></div></div>';
			$( ".hs_customer_purchase_average" ).append(htmlRange3);
			$('#range3').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 1, min: 0, max: 12});
			$('#range3').rangeSlider('onChange', event => updateRange3(event));

			htmlRange4 = '<div class="input divRange"><input type="hidden" id="hidRange4" value="0"><div id="range4" class="divRangeScale"></div><div class="divRangeCentre">Number of Years</div></div>';
			$( ".hs_customer_lifetime" ).append(htmlRange4);
			$('#range4').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 1, min: 0, max: 10});
			$('#range4').rangeSlider('onChange', event => updateRange4(event));

			htmlRange6 = '<div class="input divRange"><input type="hidden" id="hidRange6" value="0"><div id="range6" class="divRangeScale"></div><div class="divRangeLeft">Not Likely</div><div class="divRangeRight">Highly Likely</div></div>';
			$( ".hs_customer_refer_rate" ).append(htmlRange6);
			$('#range6').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 10, min: 0, max: 100});
			$('#range6').rangeSlider('onChange', event => updateRange6(event));

			htmlRange7 = '<div class="input divRange"><input type="hidden" id="hidRange7" value="0"><div id="range7" class="divRangeScale"></div><div class="divRangeCentre">Number of referrals</div></div>';
			$( ".hs_customer_referrals" ).append(htmlRange7);
			$('#range7').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 1, min: 0, max: 10});
			$('#range7').rangeSlider('onChange', event => updateRange7(event));

			htmlRange8 = '<div class="input divRange"><input type="hidden" id="hidRange8" value="0"><div id="range8" class="divRangeScale"></div><div class="divRangeLeft">Not Likely</div><div class="divRangeRight">Highly Likely</div></div>';
			$( ".hs_referral_new_customer" ).append(htmlRange8);
			$('#range8').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 10, min: 0, max: 100});
			$('#range8').rangeSlider('onChange', event => updateRange8(event));

			htmlBefore  = '<div class="div_calculate">';
			htmlBefore += '<a class="btnCompute" href="" id="btnQuestion1">Tell me how much a new customer is worth!</a>';
			htmlBefore += '</div>';


			htmlAfter  = '<div class="div_contact_txt">';
			htmlAfter += '<input type="hidden" id="hidCurrentQuestion" value="product_or_service_offering">';
			htmlAfter += "<p><b>Surprised?</b><br>This is the value each new customer brings you over their lifetime with you. Feel free to play around with different products or services you offer to see how your customer's lifetime value differs.</p>";
			htmlAfter += '<div class="hs-form-button">';
			htmlAfter += '</div>';
			htmlAfter += '</div>';
			htmlAfter += '<div class="calculatorImg">';
			htmlAfter += '<img src="{{ url('') }}/images/site/calculator-owl.jpg" title="Owl" alt="Owl">';
			htmlAfter += '</div>';

			$( "h3" ).wrap( "<div class='div_contact_hdr'></div>" );
			$( "h4" ).wrap( "<div class='div_contact_hdr'></div>" );
			$( ".hs_customer_worth" ).wrap( "<div class='div_customer_worth'></div>" );
			$( ".hs_firstname" ).wrap( "<div class='div_contact'></div>" );
			$( ".hs_lastname" ).wrap( "<div class='div_contact'></div>" );
			$( ".hs_email" ).wrap( "<div class='div_contact'></div>" );
			$( ".hs_phone" ).wrap( "<div class='div_contact'></div>" );
			$( ".hs_country_name" ).wrap( "<div class='div_contact'></div>" );
			$( ".hs_australian_state" ).wrap( "<div class='div_contact'></div>" );
			$( ".hs_submit" ).wrap( "<div class='div_contact_submit'></div>" );
			$( ".div_contact_submit" ).append("<div class='div_contact_disclaimer'>* We'll also occasionally send you newsletters or specials from Echo3. Your details are never shared & you can unsubscribe at any time.</div>");

			$( ".hs_referral_new_customer" ).after(htmlBefore);
			$( ".hs_customer_worth" ).prepend('<h3>Every new customer is worth... drum roll please...</h3><div class="txt_customer_worth "><input type="textbox" name="txtCustomerLifetimeValue" value=""></div>');
			$( ".hs_customer_worth" ).append(htmlAfter);

			// Next Buttons
			$( ".hs_product_or_service_offering" ).append("<div class='btnNext'><a href='' id='btnQuestion1'><i class='fas fa-chevron-down'></i></a></div>");
			$( ".hs_average_sale_for_new_customer" ).append("<div class='btnNext'><a href='' id='btnQuestion2'><i class='fas fa-chevron-down'></i></a></div>");
			$( ".hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc" ).append("<div class='btnNext'><a href='' id='btnQuestion3'><i class='fas fa-chevron-down'></i></a></div>");
			$( ".hs_customer_purchase_average" ).append("<div class='btnNext'><a href='' id='btnQuestion4'><i class='fas fa-chevron-down'></i></a></div>");
			$( ".hs_purchase_value_average" ).append("<div class='btnNext'><a href='' id='btnQuestion5'><i class='fas fa-chevron-down'></i></a></div>");
			$( ".hs_customer_lifetime" ).append("<div class='btnNext'><a href='' id='btnQuestion6'><i class='fas fa-chevron-down'></i></a></div>");
			$( ".hs_customer_refer_rate" ).append("<div class='btnNext'><a href='' id='btnQuestion7'><i class='fas fa-chevron-down'></i></a></div>");
			$( ".hs_customer_referrals" ).append("<div class='btnNext'><a href='' id='btnQuestion8'><i class='fas fa-chevron-down'></i></a></div>");
			$( ".hs_referral_new_customer" ).append("<div class='btnNext'><a href='' id='btnQuestion9'><i class='fas fa-chevron-down'></i></a></div>");

			// Inputs onchange
			$('input[name="customer_worth"]' ).change(function() {
				if ($('input[name="customer_worth"]').val() != "")  {
					$('.div_customer_worth' ).addClass("show");
				}	else  {
					$('.div_customer_worth' ).removeClass("show");
				}

			});

			$(".btnCompute").click(function(e){
				prevQuestion = $('#hidCurrentQuestion').val();

				$('.hs_' + prevQuestion).removeClass('hs-form-field-active');

				if (prevQuestion == 'product_or_service_offering')  {
					$('.hs_' + prevQuestion).addClass('hs-form-field-answered');
				} else if (prevQuestion == 'after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' )  {
					$('.hs_' + prevQuestion).addClass('hs-form-field-answered');
				} else  {
					$('.hs_' + prevQuestion).addClass('hs-form-field-answered');
				}

				calculateCustomerWorth(true);

				e.preventDefault();
			});

			$(".btnYes").click(function(e){
				$('.calculatorImg' ).addClass("calculatorImg-reposition");
				$('.div_contact' ).addClass("show");
				$('.div_contact_submit' ).addClass("show");
				$('.div_contact_hdr' ).addClass("show");

				$('html, body').animate({scrollTop: ($('.btnYes').offset().top)},500);

				e.preventDefault();
			});

			$(".hs-form-field").click(function(e){
				prevQuestion = $('#hidCurrentQuestion').val();

				//if (prevQuestion == 'product_or_service_offering' && $('input[name ="' + prevQuestion + '"]').val() != "")  {
				if (prevQuestion == 'product_or_service_offering')  {
					$('.hs_' + prevQuestion).addClass('hs-form-field-answered');
					//} else if (prevQuestion == 'after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' && $('input[name ="' + prevQuestion + '"]').is(':checked') )  {
				} else if (prevQuestion == 'after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' )  {
					$('.hs_' + prevQuestion).addClass('hs-form-field-answered');
					//} else if ($('input[name ="' + prevQuestion + '"]').val() > 0)  {
				} else  {
					$('.hs_' + prevQuestion).addClass('hs-form-field-answered');
				}

				if ($(this).attr('class') != "hs_australian_state hs-australian_state hs-fieldtype-select field hs-form-field" && $(this).attr('class') != "hs_country_name hs-country_name hs-fieldtype-select field hs-form-field")  {
					$('.hs-form-field').removeClass('hs-form-field-active');
					$(this).addClass('hs-form-field-active');

					if ($(this).attr('class') != "hs_customer_worth hs-customer_worth hs-fieldtype-number field hs-form-field hs-form-field-active")  {
						$('html, body').animate({scrollTop: ($(this).offset().top-100)},500);
					}
				}
				$('#hidCurrentQuestion').val($(this).attr('class').split(' ')[0].substring(3)).change();
			});

			$("input[name='average_sale_for_new_customer']").click(function () {
				$(this).select();
			});

			$("input[name='purchase_value_average']").click(function () {
				$(this).select();
			});

			// Detect "enter" key and go to next question
			// ******************************************
			// Q1
			$("input[name=product_or_service_offering]").keydown(function (e) {
				if (e.keyCode == 13) {
					$('.hs_product_or_service_offering').removeClass("hs-form-field-active");
					$('html, body').animate({scrollTop: ($('.hs_average_sale_for_new_customer').offset().top-100)},500);
					setTimeout( () => { $('.hs_average_sale_for_new_customer' ).addClass("hs-form-field-active") }, 500 );
				}
			});

			// Q2
			$("input[name=average_sale_for_new_customer]").keydown(function (e) {
				if (e.keyCode == 13) {
					$('.hs_average_sale_for_new_customer').removeClass("hs-form-field-active");
					$('html, body').animate({scrollTop: ($('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc').offset().top-100)},500);
					setTimeout( () => { $('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' ).addClass("hs-form-field-active") }, 500 );
				}
			});

			//Q3
			$(
					'input[name="after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc"]'
			).change(function() {
				if ($(this).val() == "No")  {
					$('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' ).removeClass("hs-form-field-active");
					$('html, body').animate({scrollTop: ($('.hs_customer_refer_rate').offset().top-100)},500);
					setTimeout( () => { $('.hs_customer_refer_rate' ).addClass("hs-form-field-active") }, 500 );
				} else {
					$('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' ).removeClass("hs-form-field-active");
					$('html, body').animate({scrollTop: ($('.hs_customer_purchase_average').offset().top-100)},500);
					setTimeout( () => { $('.hs_customer_purchase_average' ).addClass("hs-form-field-active") }, 500 );
				}

			});

			// Q4

			// Q5
			$("input[name=purchase_value_average ]").keydown(function (e) {
				if (e.keyCode == 13) {
					$('.hs_purchase_value_average ').removeClass("hs-form-field-active");
					$('html, body').animate({scrollTop: ($('.hs_customer_lifetime').offset().top-100)},500);
					setTimeout( () => { $('.hs_customer_lifetime ' ).addClass("hs-form-field-active") }, 500 );
				}
			});

			// Q6, Q7, Q8, Q9

			// Next Button Clicks
			//*******************
			// Q1
			$("#btnQuestion1").click(function(e){
				$('.hs_product_or_service_offering').removeClass("hs-form-field-active");
				$('html, body').animate({scrollTop: ($('.hs_average_sale_for_new_customer').offset().top-100)},500);
				setTimeout( () => { $('.hs_average_sale_for_new_customer' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q2
			$("#btnQuestion2").click(function(e){
				$('.hs_average_sale_for_new_customer').removeClass("hs-form-field-active");
				$('html, body').animate({scrollTop: ($('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc').offset().top-100)},500);
				setTimeout( () => { $('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q3
			$("#btnQuestion3").click(function(e){
				$('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' ).removeClass("hs-form-field-active");
				$('html, body').animate({scrollTop: ($('.hs_customer_purchase_average').offset().top-100)},500);
				setTimeout( () => { $('.hs_customer_purchase_average' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q4
			$("#btnQuestion4").click(function(e){
				$('.hs_customer_purchase_average' ).removeClass("hs-form-field-active");
				$('html, body').animate({scrollTop: ($('.hs_purchase_value_average').offset().top-100)},500);
				setTimeout( () => { $('.hs_purchase_value_average' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q5
			$("#btnQuestion5").click(function(e){
				$('.hs_purchase_value_average').removeClass("hs-form-field-active");
				$('html, body').animate({scrollTop: ($('.hs_customer_lifetime').offset().top-100)},500);
				setTimeout( () => { $('.hs_customer_lifetime' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q6
			$("#btnQuestion6").click(function(e){
				$('.hs_customer_lifetime').removeClass("hs-form-field-active");
				$('html, body').animate({scrollTop: ($('.hs_customer_refer_rate').offset().top-100)},500);
				setTimeout( () => { $('.hs_customer_refer_rate' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q7
			$("#btnQuestion7").click(function(e){
				$('.hs_customer_refer_rate').removeClass("hs-form-field-active");
				$('html, body').animate({scrollTop: ($('.hs_customer_referrals').offset().top-100)},500);
				setTimeout( () => { $('.hs_customer_referrals' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q8
			$("#btnQuestion8").click(function(e){
				$('.hs_customer_referrals').removeClass("hs-form-field-active");
				$('html, body').animate({scrollTop: ($('.hs_referral_new_customer').offset().top-100)},500);
				setTimeout( () => { $('.hs_referral_new_customer' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q9
			$("#btnQuestion9").click(function(e){
				$('.hs_referral_new_customer').removeClass("hs-form-field-active");
				$('html, body').animate({scrollTop: ($('.div_calculate').offset().top-100)},500);
				//setTimeout( () => { $('.hs_referral_new_customer' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

		}
	</script>
@endsection
