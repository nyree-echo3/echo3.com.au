@section('styles')	  
    <link rel="stylesheet" href="{{ asset('css/site/rangeSlider.css') }}">
	<link rel="stylesheet" href="{{ asset('css/site/quiz-customer.css?v2.58') }}">		
@endsection


	<div class="blog-masthead ">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 blog-main blog-calculator">
				<div class="blog-post">
					<div id="divCalculatorText">
					{!! $quiz_customer !!}
					</div>

					<div id="divCalculator">
					    <div class="divCalculator-h2">Calculate my Customer Lifetime Value (CLV)</div>

						<!-- Hubspot Form -->
						<!--[if lte IE 8]>
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
						<![endif]-->
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						<script>
						  hbspt.forms.create({
							portalId: "4830934",
							formId: "e4cdbbe0-8f53-4648-bf14-510e43599715",
							onFormReady: function(jQueryform, ctx){
								testt();
							}  
						});
						</script>
					</div><!-- /.divCalculator -->
												
					</div>								
				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
    
    <!--
    <div class="calculatorImg">
	   <img src="{{ url('') }}/images/site/calculator-owl.png" title="Owl" alt="Owl">
	</div>
	--> 
	
</div><!-- /.blog-masthead -->

@section('inline-scripts-quiz-customer')
	<script src="{{ asset('js/site/quiz-customer.js?v4.38') }}"></script>
	<script src="{{ asset('js/site/rangeSlider.js?v=0.5') }}"></script>
	<script>

		function testt(){
			jQuery('.hs-product_or_service_offering').addClass('hs-form-field-active');

			htmlRange3 = '<div class="input divRange"><input type="hidden" id="hidRange3" value="0" onchange="hidRange3_onchange()"><div id="range3" class="divRangeScale"><div class="divRangeCentre">Number of times a year</div></div></div>';
			jQuery( ".hs_customer_purchase_average" ).append(htmlRange3);
			jQuery('#range3').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 1, min: 0, max: 12});
			jQuery('#range3').rangeSlider('onChange', event => updateRange3(event));

			htmlRange4 = '<div class="input divRange"><input type="hidden" id="hidRange4" value="0"><div id="range4" class="divRangeScale"></div><div class="divRangeCentre">Number of Years</div></div>';
			jQuery( ".hs_customer_lifetime" ).append(htmlRange4);
			jQuery('#range4').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 1, min: 0, max: 10});
			jQuery('#range4').rangeSlider('onChange', event => updateRange4(event));

			htmlRange6 = '<div class="input divRange"><input type="hidden" id="hidRange6" value="0"><div id="range6" class="divRangeScale"></div><div class="divRangeLeft">Not Likely</div><div class="divRangeRight">Highly Likely</div></div>';
			jQuery( ".hs_customer_refer_rate" ).append(htmlRange6);
			jQuery('#range6').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 10, min: 0, max: 100});
			jQuery('#range6').rangeSlider('onChange', event => updateRange6(event));

			htmlRange7 = '<div class="input divRange"><input type="hidden" id="hidRange7" value="0"><div id="range7" class="divRangeScale"></div><div class="divRangeCentre">Number of referrals</div></div>';
			jQuery( ".hs_customer_referrals" ).append(htmlRange7);
			jQuery('#range7').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 1, min: 0, max: 10});
			jQuery('#range7').rangeSlider('onChange', event => updateRange7(event));

			htmlRange8 = '<div class="input divRange"><input type="hidden" id="hidRange8" value="0"><div id="range8" class="divRangeScale"></div><div class="divRangeLeft">Not Likely</div><div class="divRangeRight">Highly Likely</div></div>';
			jQuery( ".hs_referral_new_customer" ).append(htmlRange8);
			jQuery('#range8').rangeSlider({ skin: 'red', direction: 'horizontal', scale: true}, {values: [0], step: 10, min: 0, max: 100});
			jQuery('#range8').rangeSlider('onChange', event => updateRange8(event));

			htmlBefore  = '<div class="div_calculate">';
			htmlBefore += '<a class="btnCompute" href="" id="btnQuestion1">Tell me how much a new customer is worth!</a>';
			htmlBefore += '</div>';


			htmlAfter  = '<div class="div_contact_txt">';
			htmlAfter += '<input type="hidden" id="hidCurrentQuestion" value="product_or_service_offering">';
			htmlAfter += "<p><b>Surprised?</b><br>This is the value each new customer brings you over their lifetime with you. Feel free to play around with different products or services you offer to see how your customer's lifetime value differs.</p>";
			htmlAfter += '<div class="hs-form-button">';
			htmlAfter += '</div>';
			htmlAfter += '</div>';
			htmlAfter += '<div class="calculatorImg">';
			htmlAfter += '<img src="{{ url('') }}/images/site/calculator-owl.png" title="Owl" alt="Owl">';
			htmlAfter += '</div>';

			jQuery( "h3" ).wrap( "<div class='div_contact_hdr'></div>" );
			jQuery( "h4" ).wrap( "<div class='div_contact_hdr'></div>" );
			jQuery( ".hs_customer_worth" ).wrap( "<div class='div_customer_worth'></div>" );
			jQuery( ".hs_firstname" ).wrap( "<div class='div_contact'></div>" );
			jQuery( ".hs_lastname" ).wrap( "<div class='div_contact'></div>" );
			jQuery( ".hs_email" ).wrap( "<div class='div_contact'></div>" );
			jQuery( ".hs_phone" ).wrap( "<div class='div_contact'></div>" );
			jQuery( ".hs_country_name" ).wrap( "<div class='div_contact'></div>" );
			jQuery( ".hs_australian_state" ).wrap( "<div class='div_contact'></div>" );
			jQuery( ".hs_submit" ).wrap( "<div class='div_contact_submit'></div>" );
			jQuery( ".div_contact_submit" ).append("<div class='div_contact_disclaimer'>* We'll also occasionally send you newsletters or specials from Echo3. Your details are never shared & you can unsubscribe at any time.</div>");

			jQuery( ".hs_referral_new_customer" ).after(htmlBefore);
			jQuery( ".hs_customer_worth" ).prepend('<h3>Every new customer is worth... drum roll please...</h3><div class="txt_customer_worth "><input type="textbox" name="txtCustomerLifetimeValue" value=""></div>');
			jQuery( ".hs_customer_worth" ).append(htmlAfter);

			// Next Buttons
			jQuery( ".hs_product_or_service_offering" ).append("<div class='btnNext'><a href='' id='btnQuestion1'><i class='fas fa-chevron-down'></i></a></div>");
			jQuery( ".hs_average_sale_for_new_customer" ).append("<div class='btnNext'><a href='' id='btnQuestion2'><i class='fas fa-chevron-down'></i></a></div>");
			jQuery( ".hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc" ).append("<div class='btnNext'><a href='' id='btnQuestion3'><i class='fas fa-chevron-down'></i></a></div>");
			jQuery( ".hs_customer_purchase_average" ).append("<div class='btnNext'><a href='' id='btnQuestion4'><i class='fas fa-chevron-down'></i></a></div>");
			jQuery( ".hs_purchase_value_average" ).append("<div class='btnNext'><a href='' id='btnQuestion5'><i class='fas fa-chevron-down'></i></a></div>");
			jQuery( ".hs_customer_lifetime" ).append("<div class='btnNext'><a href='' id='btnQuestion6'><i class='fas fa-chevron-down'></i></a></div>");
			jQuery( ".hs_customer_refer_rate" ).append("<div class='btnNext'><a href='' id='btnQuestion7'><i class='fas fa-chevron-down'></i></a></div>");
			jQuery( ".hs_customer_referrals" ).append("<div class='btnNext'><a href='' id='btnQuestion8'><i class='fas fa-chevron-down'></i></a></div>");
			jQuery( ".hs_referral_new_customer" ).append("<div class='btnNext'><a href='' id='btnQuestion9'><i class='fas fa-chevron-down'></i></a></div>");

			// Inputs onchange
			jQuery('input[name="customer_worth"]' ).change(function() {
				if (jQuery('input[name="customer_worth"]').val() != "")  {
					jQuery('.div_customer_worth' ).addClass("show");
				}	else  {
					jQuery('.div_customer_worth' ).removeClass("show");
				}

			});

			jQuery(".btnCompute").click(function(e){
				prevQuestion = jQuery('#hidCurrentQuestion').val();

				jQuery('.hs_' + prevQuestion).removeClass('hs-form-field-active');

				if (prevQuestion == 'product_or_service_offering')  {
					jQuery('.hs_' + prevQuestion).addClass('hs-form-field-answered');
				} else if (prevQuestion == 'after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' )  {
					jQuery('.hs_' + prevQuestion).addClass('hs-form-field-answered');
				} else  {
					jQuery('.hs_' + prevQuestion).addClass('hs-form-field-answered');
				}

				calculateCustomerWorth(true);

				e.preventDefault();
			});

			jQuery(".btnYes").click(function(e){
				jQuery('.calculatorImg' ).addClass("calculatorImg-reposition");
				jQuery('.div_contact' ).addClass("show");
				jQuery('.div_contact_submit' ).addClass("show");
				jQuery('.div_contact_hdr' ).addClass("show");

				jQuery('html, body').animate({scrollTop: (jQuery('.btnYes').offset().top)},500);

				e.preventDefault();
			});

			jQuery(".hs-form-field").click(function(e){
				prevQuestion = jQuery('#hidCurrentQuestion').val();

				//if (prevQuestion == 'product_or_service_offering' && jQuery('input[name ="' + prevQuestion + '"]').val() != "")  {
				if (prevQuestion == 'product_or_service_offering')  {
					jQuery('.hs_' + prevQuestion).addClass('hs-form-field-answered');
					//} else if (prevQuestion == 'after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' && jQuery('input[name ="' + prevQuestion + '"]').is(':checked') )  {
				} else if (prevQuestion == 'after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' )  {
					jQuery('.hs_' + prevQuestion).addClass('hs-form-field-answered');
					//} else if (jQuery('input[name ="' + prevQuestion + '"]').val() > 0)  {
				} else  {
					jQuery('.hs_' + prevQuestion).addClass('hs-form-field-answered');
				}

				if (jQuery(this).attr('class') != "hs_australian_state hs-australian_state hs-fieldtype-select field hs-form-field" && jQuery(this).attr('class') != "hs_country_name hs-country_name hs-fieldtype-select field hs-form-field")  {
					jQuery('.hs-form-field').removeClass('hs-form-field-active');
					jQuery(this).addClass('hs-form-field-active');

					if (jQuery(this).attr('class') != "hs_customer_worth hs-customer_worth hs-fieldtype-number field hs-form-field hs-form-field-active")  {
						jQuery('html, body').animate({scrollTop: (jQuery(this).offset().top-100)},500);
					}
				}
				jQuery('#hidCurrentQuestion').val(jQuery(this).attr('class').split(' ')[0].substring(3)).change();
			});

			jQuery("input[name='average_sale_for_new_customer']").click(function () {
				jQuery(this).select();
			});

			jQuery("input[name='purchase_value_average']").click(function () {
				jQuery(this).select();
			});

			// Detect "enter" key and go to next question
			// ******************************************
			// Q1
			jQuery("input[name=product_or_service_offering]").keydown(function (e) {
				if (e.keyCode == 13) {
					jQuery('.hs_product_or_service_offering').removeClass("hs-form-field-active");
					jQuery('html, body').animate({scrollTop: (jQuery('.hs_average_sale_for_new_customer').offset().top-100)},500);
					setTimeout( () => { jQuery('.hs_average_sale_for_new_customer' ).addClass("hs-form-field-active") }, 500 );
				}
			});

			// Q2
			jQuery("input[name=average_sale_for_new_customer]").keydown(function (e) {
				if (e.keyCode == 13) {
					jQuery('.hs_average_sale_for_new_customer').removeClass("hs-form-field-active");
					jQuery('html, body').animate({scrollTop: (jQuery('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc').offset().top-100)},500);
					setTimeout( () => { jQuery('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' ).addClass("hs-form-field-active") }, 500 );
				}
			});

			//Q3
			jQuery(
					'input[name="after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc"]'
			).change(function() {
				if (jQuery(this).val() == "No")  {
					jQuery('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' ).removeClass("hs-form-field-active");
					jQuery('html, body').animate({scrollTop: (jQuery('.hs_customer_refer_rate').offset().top-100)},500);
					setTimeout( () => { jQuery('.hs_customer_refer_rate' ).addClass("hs-form-field-active") }, 500 );
				} else {
					jQuery('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' ).removeClass("hs-form-field-active");
					jQuery('html, body').animate({scrollTop: (jQuery('.hs_customer_purchase_average').offset().top-100)},500);
					setTimeout( () => { jQuery('.hs_customer_purchase_average' ).addClass("hs-form-field-active") }, 500 );
				}

			});

			// Q4

			// Q5
			jQuery("input[name=purchase_value_average ]").keydown(function (e) {
				if (e.keyCode == 13) {
					jQuery('.hs_purchase_value_average ').removeClass("hs-form-field-active");
					jQuery('html, body').animate({scrollTop: (jQuery('.hs_customer_lifetime').offset().top-100)},500);
					setTimeout( () => { jQuery('.hs_customer_lifetime ' ).addClass("hs-form-field-active") }, 500 );
				}
			});

			// Q6, Q7, Q8, Q9

			// Next Button Clicks
			//*******************
			// Q1
			jQuery("#btnQuestion1").click(function(e){
				jQuery('.hs_product_or_service_offering').removeClass("hs-form-field-active");
				jQuery('html, body').animate({scrollTop: (jQuery('.hs_average_sale_for_new_customer').offset().top-100)},500);
				setTimeout( () => { jQuery('.hs_average_sale_for_new_customer' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q2
			jQuery("#btnQuestion2").click(function(e){
				jQuery('.hs_average_sale_for_new_customer').removeClass("hs-form-field-active");
				jQuery('html, body').animate({scrollTop: (jQuery('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc').offset().top-100)},500);
				setTimeout( () => { jQuery('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q3
			jQuery("#btnQuestion3").click(function(e){
				jQuery('.hs_after_the_initial_purchase_are_there_subsequent_products_or_services_that_a_new_customer_would_purc' ).removeClass("hs-form-field-active");
				jQuery('html, body').animate({scrollTop: (jQuery('.hs_customer_purchase_average').offset().top-100)},500);
				setTimeout( () => { jQuery('.hs_customer_purchase_average' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q4
			jQuery("#btnQuestion4").click(function(e){
				jQuery('.hs_customer_purchase_average' ).removeClass("hs-form-field-active");
				jQuery('html, body').animate({scrollTop: (jQuery('.hs_purchase_value_average').offset().top-100)},500);
				setTimeout( () => { jQuery('.hs_purchase_value_average' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q5
			jQuery("#btnQuestion5").click(function(e){
				jQuery('.hs_purchase_value_average').removeClass("hs-form-field-active");
				jQuery('html, body').animate({scrollTop: (jQuery('.hs_customer_lifetime').offset().top-100)},500);
				setTimeout( () => { jQuery('.hs_customer_lifetime' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q6
			jQuery("#btnQuestion6").click(function(e){
				jQuery('.hs_customer_lifetime').removeClass("hs-form-field-active");
				jQuery('html, body').animate({scrollTop: (jQuery('.hs_customer_refer_rate').offset().top-100)},500);
				setTimeout( () => { jQuery('.hs_customer_refer_rate' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q7
			jQuery("#btnQuestion7").click(function(e){
				jQuery('.hs_customer_refer_rate').removeClass("hs-form-field-active");
				jQuery('html, body').animate({scrollTop: (jQuery('.hs_customer_referrals').offset().top-100)},500);
				setTimeout( () => { jQuery('.hs_customer_referrals' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q8
			jQuery("#btnQuestion8").click(function(e){
				jQuery('.hs_customer_referrals').removeClass("hs-form-field-active");
				jQuery('html, body').animate({scrollTop: (jQuery('.hs_referral_new_customer').offset().top-100)},500);
				setTimeout( () => { jQuery('.hs_referral_new_customer' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

			// Q9
			jQuery("#btnQuestion9").click(function(e){
				jQuery('.hs_referral_new_customer').removeClass("hs-form-field-active");
				jQuery('html, body').animate({scrollTop: (jQuery('.div_calculate').offset().top-100)},500);
				//setTimeout( () => { jQuery('.hs_referral_new_customer' ).addClass("hs-form-field-active") }, 500 );

				e.stopPropagation();
				e.preventDefault();
			});

		}
	</script>
@endsection
