@php
	// Set Meta Tags
	$meta_title_inner = "What's a new customer worth to you? | Echo3";
	$meta_keywords_inner = "How much is a new customer worth? Try our Customer Lifetime Value (CLV) Calculator. | Echo3";
	$meta_description_inner = "How much is a new customer worth? Try our Customer Lifetime Value (CLV) Calculator. | Echo3";
	
	$page_name = "calculator";
@endphp

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')
				       
@include('site/quiz-customer/partials/quiz-customer')
			

@endsection
