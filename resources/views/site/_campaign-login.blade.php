@extends('site/layouts/app')

@section('content')

@php
   $header1 = "Client Login";
   $header2 = "Login here to access your email marketing campaigns and reports.";
   
   $formHTML  = '';
   $formHTML .= '<form action="https://echo3.createsend.com/login.aspx" method="post" class="frm-general">';
   $formHTML .= '<div>';
   $formHTML .= '<div><input type="text" class="txt-input" name="username" id="username" placeholder="enter username" /></div>';
   $formHTML .= '<div><input type="password" name="password" id="password" class="txt-input" placeholder="enter password" /></div>';
   $formHTML .= '<input type="submit" class="btn-submit" value="LOGIN" />';
   $formHTML .= '</div>';
   $formHTML .= '</form>';
   $formHTML .= '<p></p>';
   $formHTML .= '<p>If you need any help, please <a href="' . url('') . '/contact">contact us</a>.</p>';
               
@endphp

@include('site/partials/carousel-form')

@endsection
