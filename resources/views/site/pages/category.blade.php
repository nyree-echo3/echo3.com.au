<?php
// Set Meta Tags
$meta_title_inner = $category->meta_title;
$meta_keywords_inner = $category->meta_keywords;
$meta_description_inner = $category->meta_description;
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')
    @include('site/partials/sidebar-page-categories')          
    
    @if ($category->body != "")
		<div id="blog-masthead" class="blog-masthead">
			<div class="container">	
			    <div class="blog-masthead-hdr" data-aos="zoom-in-up" data-aos-duration="2000">		
					<h1>{!! $category->name !!}</h1>			
				</div>
						
				{!! $category->body !!}

			</div>
		</div> 
    @endif
    
    @if (isset($page) && $page->popup_type != "")    
        @include('site/partials/popup') 
    @endif
@endsection
