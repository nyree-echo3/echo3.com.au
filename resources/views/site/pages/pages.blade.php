<?php
// Set Meta Tags
if ($page_slug != "")  {
	$meta_title_inner = $page->meta_title;
	$meta_keywords_inner = $page->meta_keywords;
	$meta_description_inner = $page->meta_description;
} else  {
	$meta_title_inner = $category->meta_title;
	$meta_keywords_inner = $category->meta_keywords;
	$meta_description_inner = $category->meta_description;	
}
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')    
        
    <div id="blog-masthead" class="blog-masthead ">
        <div class="container"> 
            <div class="row"> 
                <div class="blog-masthead-content">  
					<div class="blog-masthead-content-menu">     
					@include('site/partials/sidebar-pages')   
					</div>

					<div class="blog-masthead-content-txt">   									   									
					{!! $page->body !!}
					</div>
				</div>
           
            </div><!-- /.row -->     
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->   
        
    @if ($page->id == 8)
        @include('site/partials/panel-meeting')
        @include('site/partials/panel-slider')
        @include('site/partials/panel-team')
    @endif
    @if (isset($page) && $page->popup_type != "")    
        @include('site/partials/popup') 
    @endif
    
@endsection