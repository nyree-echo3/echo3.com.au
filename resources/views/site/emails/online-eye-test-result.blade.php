<!DOCTYPE html>
<html>
<head>
    <style>
        body { font: 14px/18px normal Open Sans,Arial,sans-serif; color: #333333;}
    </style>
</head>
<body>
<table width="600" align="center">
    <tr>
        <td><img src="{{ url('') }}/images/site/online-eye-test-email/header.jpg"></td>
    </tr>
    <tr>
        <td>
            <p>Hi there</p>
            <p>Well done on completing your personalEYES Online Eye Test! As the one of the most experienced corrective eye surgery groups in Australasia, we're pleased to be able to support you on your journey towards 20 20 vision.</p>
            <p style="color: #0071ad;"><strong>Here are your online test results:</strong></p>
            <p>{{ $result }}</p>
            <p style="color: #0071ad;"><strong>Want to learn more about how LASIK works?</strong></p>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <a href="{{ url('') }}/lasik" target="_blank">
                <img src="{{ url('') }}/images/site/online-eye-test-email/LASIKVIDEOpreview.jpg" width="400" />
            </a>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table width="200" align="center" style="margin-top:5px; margin-bottom:5px;">
                <tr>
                    <td style="background-color: #dddddd; text-align: center; font-size: 12px; font-weight: bold; padding: 15px 15px 15px 15px;">
                        <a href="{{ url('') }}/lasik" style="color: #0071ad; text-decoration: none;">Watch this short video here</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <p style="color: #0071ad;"><strong>What to calculate how much you could be saving with LASIK?</strong></p>
            <p>Use our simple online calculator to see the savings you could be making with our affordable laser eye surgery.</p>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table width="250" align="center" style="margin-top:5px; margin-bottom:5px;">
                <tr>
                    <td style="background-color: #dddddd; text-align: center; font-size: 12px; font-weight: bold; padding: 15px 15px 15px 15px;">
                        <a href="{{ url('') }}/costs/cost-savings-calculator" style="color: #0071ad; text-decoration: none;">Click here for online calculator</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><p>Feel free to hit reply, hop onto our website chat or give us a call on 1300 683 937 if you have any questions!</p></td>
    </tr>
    <tr>
        <td><p>Warm regards, <br />The team at personalEYES<p></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <img src="{{ url('') }}/images/site/online-eye-test-email/PE-logo.png" width="200" />
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><a href="{{ url('') }}" target="_blank" style="font-size: 12px;">{{ url('') }}</a></td>
    </tr>
    <tr>
        <td>
            <p style="font-size: 12px;">
            Toll Free 1300 68 3937 [1300 Nu Eyes]<br />
            P (02) 8833 7111<br />
            F (02) 8833 7112 or 1800 789 077<br />
            E <a href="mailto:vision@personaleyes.com.au">vision@personaleyes.com.au</a><br />
            PO Box 301 Parramatta, NSW, 2150
            <p>
        </td>
    </tr>
</table>
</body>
</html>
