<?php
// Set Meta Tags
$meta_title_inner = "Email Marketing Client Login Echo3 Media";
$meta_keywords_inner =  "Email Marketing Client Login Echo3 Media";
$meta_description_inner = "Email Marketing Client Login Echo3 Media";
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')    
        
    <div id="blog-masthead" class="blog-masthead ">
        <div class="container"> 
            <div class="row"> 
                <div class="blog-masthead-content">  
					<div class="blog-masthead-content-menu">     
					   @include('site/partials/sidebar-campaign-login')   
					</div>

					<div class="blog-masthead-content-frm"> 
				   <h3>Login here to access your email marketing campaigns and reports.</h3>		  									   									
					   <div class="home-form frm-general">	                           					
            
						   <form action="https://echo3.createsend.com/login.aspx" method="post" class="frm-general">
						   <div>
						   <div><input type="text" class="txt-input" name="username" id="username" placeholder="what's your username?" /></div>
						   <div><input type="password" name="password" id="password" class="txt-input" placeholder="and what's your secret password?" /></div>
						   <input type="submit" class="btn-submit" value="LOGIN" />
						   </div>
						   </form>
						   <p></p>
						   <p>If you need any help, please <a href="{{ url('') }}/contact">contact us</a>.</p>

					   </div>
					</div>
				</div>
           
            </div><!-- /.row -->     
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->   
    
    @if (isset($page) && $page->popup_type != "")    
        @include('site/partials/popup') 
    @endif
    
@endsection
