<?php
// Set Meta Tags
$meta_title_inner = $news_item->meta_title;
$meta_keywords_inner = $news_item->meta_keywords;
$meta_description_inner = $news_item->meta_description;
?>
@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div id="blog-masthead" class="blog-masthead">
	<div class="container">
            <div class="blog-masthead-hdr" data-aos="zoom-in-up" data-aos-duration="2000">		                   
			   <h1>{{ $news_item->title }}</h1>
			</div>
           
            <div class="row">

                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">

                    <div class="blog-post">                      
                        
                        <div class="container">
                           <div class="row">
                              <div class="col-lg-8 no-gutters nopadding">
                       								
								<div class="blog-author">{{ ($news_item->author != "" ? $news_item->author : "Echo3") }} | {{ date("j M Y", strtotime($news_item->start_date)) }}</div>

								@if ($news_item->thumbnail <> "")
									<div class='news-item-img'>   
									  <img src='{{ $news_item->thumbnail }}' alt='{{ $news_item->title }}' title="{{ $news_item->title }}">                         								                                           
									</div>
								@endif

								<div class='news-item'>                            
									{!! $news_item->body !!}                                           
								</div>

								<div class='btn-back'>
								    @php
								    $href = "";
								    $service_slug = Session::get('services_slug');
								    if (isset($service_slug))  {
								       $href = "/" . Session::get('services_slug');								    
								    }
								    @endphp
									<a class='btn-back' href='javascript: window.history.go(-1);'><i class='fa fa-chevron-left'></i> back</a>
								</div>
                            
                             </div>
                             
                             <div class="col-lg-1"></div>
                             
                             <div class="col-lg-3 nopadding">
                                @if (isset($cta_side))
									<div class="news-item-cta">								   
									   <div class="container no-gutters nopadding">
										  <div class="row no-gutters nopadding">
											 <div class="col-lg-12">

											   <div class="news-cta">
												   <div class="card-body">
													   <div class="panel-news-cta">														   
															@if ($cta_side->image != "")											   
																<div class="div-img">
																   <img src="{{ url('') }}/{{ $cta_side->image }}" alt="{{$cta_side->title}}" title="{{$cta_side->title}}" />	
																</div>			
															@endif	                                    

															<div class="panel-news-cta-title">{{$cta_side->title}}</div>
															<div class="panel-news-cta-button">
															   <a href="{{ $cta_side->url }}">{{ $cta_side->button }}</a>
															</div>															
														</div>
													</div>
												 </div>

											 </div>
										  </div>
									   </div>                                                                       
									</div>
                                @endif
                               
                                <div class="news-item-other-hdr">
                                  <h2>Other articles of interest</h2>
								</div>
                               
                                @foreach ($news_additional as $news_additional_item)
                                <div class="news-item-other">
								   <div class="container no-gutters nopadding">
                                      <div class="row no-gutters nopadding">
                                         <div class="col-lg-12">
                                          
                                           <div class="news-list">
											   <div class="card-body">
												   <div class="panel-news-item">
													   <a  class="projects-more"  href="{{ url('') }}/news/{{ $news_additional_item->category->slug }}/{{$news_additional_item->slug}}">
															@if ($news_additional_item->thumbnail != "")											   
																<div class="div-img">
																   <img src="{{ url('') }}/{{ $news_additional_item->thumbnail }}" alt="{{$news_additional_item->title}}" title="{{$news_additional_item->title}}" />	
																</div>			
															@endif	                                    

															<div class="panel-news-item-title">{{$news_additional_item->title}}</div>
															<div class="panel-news-item-shortdesc">{!! $news_additional_item->short_description !!}</div>

															<div class="panel-news-item-readmore">Read More ></div>													                                               
														</a>
													</div>
												</div>
											 </div>
											
										 </div>
									  </div>
								   </div>                                                                       
								</div>
								@endforeach
								
							 </div>
			      
		                     <!-- CTAs -->
		                     @if (isset($cta_bottom))
			                 <div class="col-lg-8 no-gutters nopadding">                                
								<div class="news-item-cta">								   
								   <div class="container no-gutters nopadding">
									  <div class="row no-gutters nopadding">
										 <div class="col-lg-12">

										   <div class="news-cta news-cta-bottom">
											   <div class="card-body card-body-cta-bottom">
												   <div class="panel-news-cta">	
												        <div class="div-img div-cta-bottom">													   
														@if ($cta_bottom->image != "")											   
															<div class="div-img-bottom">
															   <img src="{{ url('') }}/{{ $cta_bottom->image }}" alt="{{$cta_bottom->title}}" title="{{$cta_bottom->title}}" />	
															</div>			
														@endif	                                    

													    <div class="div-cta-txt">
															<div class="panel-news-cta-title">{{$cta_bottom->title}}</div>
															<div class="panel-news-cta-button">
															   <a href="{{ $cta_bottom->url }}">{{ $cta_bottom->button }}</a>
															</div>								
													    </div>
													   </div>							
													</div>
												</div>
											 </div>

										 </div>
									  </div>
								   </div>                                                                       
								</div>                                                                
                             </div>
                             @endif
                             <!-- End - CTAs -->                                                          
			      
				      
					      </div>
					   </div>

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->
            </div><!-- /.row -->
        </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@if (isset($news_item) && $news_item->popup_type != "")    
	@php
	   $page = $news_item;
	@endphp

	@include('site/partials/popup') 
@endif

@endsection  
