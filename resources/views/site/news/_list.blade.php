<?php
// Set Meta Tags
$meta_title_inner = "Echo3 Media " . ($category_name != "Latest" ? $category_name : "");
$meta_keywords_inner =  "Echo3 Media, " . ($category_name != "Latest" ? " - " . $category_name : "");
$meta_description_inner = "Echo3 Media " . ($category_name != "Latest" ? " - " . $category_name : "");
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')
@include('site/partials/sidebar-news')

<div id="blog-masthead" class="blog-masthead blog-news">
	<div class="container">                
        <div class="news-list">
            @if ($category_name == "Latest")
				<div class="blog-masthead-hdr" data-aos="zoom-in-up" data-aos-duration="2000">		
					<h1>{!! $category_name !!}</h1>			
				</div>
			@endif
        
			<div class="card-columns">

					@foreach($items as $item)    
						<div class="card">										
							<div class="card-body">
							<div class="panel-news-item">	
								<a  class="projects-more"  href="{{ url('') }}/news/{{ $item->category->slug }}/{{$item->slug}}">
									@if ($item->thumbnail != "")											   
										<div class="div-img">
										   <img src="{{ url('') }}/{{ $item->thumbnail }}" alt="{{$item->title}}" />	
										</div>			
									@endif	                                    

									<div class="panel-news-item-title"><h2>{{$item->title}}</h2></div>
									<div class="panel-news-item-shortdesc">{!! $item->short_description !!}</div>

									<div class="panel-news-item-readmore">Read More <img src="{{ url('') }}/images/site/arrow-red.png" title="Arrow" alt="Arrow"></div>													                                               
								</a>
								  </div>  
							</div>



						</div>
					@endforeach   

			</div><!-- .card-columns -->
		</div><!--  .container news-list -->
	</div>                    

	</div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection  
					