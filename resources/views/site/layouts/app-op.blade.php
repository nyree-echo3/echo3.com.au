@php
    if(isset($mode) && $mode == 'preview')
      $meta_title_inner = "Preview Page";
@endphp

<!doctype html>
<html lang="en">
<head>   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="{{ (isset($meta_keywords_inner) ? $meta_keywords_inner : $meta_keywords) }}">
    <meta name="description" content="{{ (isset($meta_description_inner) ? $meta_description_inner : $meta_description) }}">
    <meta name="author" content="Echo3 Media">
    <meta name="web_author" content="www.echo3.com.au">
    <meta name="date" content="{{ $live_date }}" scheme="DD-MM-YYYY">
    <meta name="robots" content="{{ (isset($meta_robots) ? $meta_robots : "all") }}">      
   
    <title>{{ (strip_tags(isset($meta_title_inner) ? $meta_title_inner : $meta_title)) }}</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700" rel="stylesheet">   
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400,500,700" rel="stylesheet">
    
    <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/bootstrap-4-navbar.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/base/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/ckeditor/plugins/mjTab/frontend/mjTab.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/general.css?v=2.96') }}">    
    <link rel="stylesheet" href="{{ asset('css/site/animate.css') }}">
    <link href="{{ asset('/components/css-hamburgers/dist/hamburgers.css') }}" rel="stylesheet">
	<link href="{{ asset('/components/aos/dist/aos.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/site/popup.css?v=0.2') }}">
    <link rel="stylesheet" href="{{ asset('css/site/popups.css?v=0.8') }}">
    
    <link rel="shortcut icon" href="{{ url('') }}/favicon.ico?">
    <link rel="apple-touch-icon" href="{{ url('') }}/apple-icon.png">

    @yield('styles')

    @if(env('APP_ENV')=='production')
    <!-- Google Analytics -->
    {!! $google_analytics !!}
    @endif        
	
	<!-- Twitter Cards -->
	<meta name="twitter:card" content="summary" />
	
	<!-- Facebook Open Graph -->
	<meta property="og:url"                content="{{ url()->current() }}" />
	<meta property="og:type"               content="{{ (isset($og_type) ? $og_type : "website") }}" />
	<meta property="og:title"              content="{{ (isset($meta_title_inner) ? $meta_title_inner : $meta_title) }}" />
	<meta property="og:description"        content="{{ (isset($meta_description_inner) ? $meta_description_inner : $meta_description) }}" />
	<meta property="og:image"              content="{{ url('') }}{{ (isset($og_image) ? $og_image : "/images/site/og-logo.jpg") }}" />

    <!-- Canonical Tag -->
    <link rel="canonical" href="{{ url()->current() }}" />
    
</head>
<body>
<button id="btnTopPage" title="Go to top">Back to top <i class="fas fa-long-arrow-alt-right"></i></button>  
@include('site/partials/header')
@yield('content')    	

@include('site/partials/footer')

<script src="{{ asset('/components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/components/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('/ckeditor/plugins/mjTab/frontend/mjTab.js') }}"></script>
<script src="{{ asset('js/site/bootstrap-4-navbar.js') }}"></script>
<script src="{{ asset('js//site/affix.js') }}"></script>
<script src="{{ asset('js/site/button-topofpage.js') }}"></script>
<script src="{{ asset('/components/aos/dist/aos.js') }}"></script>
<script src="{{ asset('/js//site/nav.js') }}"></script>
	
<script type="text/javascript">
	function isScrolledIntoView(element) {
		var scrollBottomPosition = $(window).scrollTop() + $(window).height();
		return ($(element).offset().top < scrollBottomPosition);
	}
	
	 $( document ).ready(function() {    		  
        AOS.init();		 					    
    });	
</script>
  
@yield('inline-scripts-quiz-customer-op')
@yield('scripts')
@yield('inline-scripts')
@yield('inline-scripts-header')
@yield('inline-scripts-nav')
@yield('inline-scripts-instagram')
@yield('inline-scripts-popups')
@yield('inline-scripts-popup-contact')
@yield('inline-scripts-popup-contact-op')
@if (url()->current() == url(''))
   @yield('inline-scripts-popup-newsletter')
@else
   @yield('inline-scripts-popup-newsletter-inside')
@endif

</body>
</html>