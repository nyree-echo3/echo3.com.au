<?php 
   // Set Meta Tags
   $meta_title_inner = $product_item->ext_title; 
   $meta_keywords_inner = $product_item->meta_keywords; 
   $meta_description_inner = $product_item->meta_description;  
?>
@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/baguettebox.js/src/baguetteBox.css') }}">
@endsection


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-products')        
        
        <div class="col-sm-8 blog-main">
           <section class="project-block cards-project">
              <div class="container">	  
               
                 <div class="blog-post">                          
                    <h1 class="blog-post-title">Shop</h1>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        
						<div class='project-item'>
						   <div class='project-item-txt'>
							  <h2>{{ $product_item->name }}</h2>
							  
							  <div class="product-item-price">
                                  <span class="price">{{ format_price($product_item->price) }}</span>
                              </div>
                              
                              <div class="product-item-description">
							     {!! $product_item->description !!}
							  </div>
							  
							  <div class="product-item-shipping">
							     <h3>INCLUDES FREE SHIPPING</h3>
							  </div>
								  
							  <form action="{{ route('cart.add', $product_item) }}" method="post">
                                 {{ csrf_field() }}
                              
                                 <button type="submit" class="btn-cart">Add to cart</button>
                              </form>
						   </div>

						   @if (count($display_product_item->images) > 0)
							   <div class='project-item-img'>
								    @foreach($display_product_item->images as $image)           	
										<div class="card border-0 transform-on-hover">	
											<div class='project-list-item-img'>
											    <a class="lightbox" href="{{ url('') }}/{{ $image->location }}" data-caption="{{$product_item->name}}">
													<img src="{{ url('') }}/{{ $image->location }}" alt="{{$product_item->name}}" title="{{$product_item->name}}" class="card-img-top">
												</a>																				
											</div>
										</div>	
									@endforeach 
								</div>
						  @endif 					       
						</div>
         	     
          	        <div class='btn-back'>
           	           <a class='btn-back' href='{{ url('') }}/products/{{ $display_product_item->category->slug }}'><i class='fa fa-chevron-left'></i> back</a>	
			       </div>
			       
			       @include('site/partials/helper-sharing')	            
			   </div>
           </section>      	   
          	 
            					  								  			            
       </div><!-- /.blog-post -->         
    </div><!-- /.blog-main -->        

  </div><!-- /.row -->

</div><!-- /.container -->

@endsection


@section('scripts')
    <script src="{{ asset('/components/baguettebox.js/src/baguetteBox.js') }}"></script>
@endsection


@section('inline-scripts')
   <script type="text/javascript">
        $(document).ready(function () {       
           baguetteBox.run('.cards-project', { animation: 'slideIn'});
        });
    </script>			
@endsection