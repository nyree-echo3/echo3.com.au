<?php 
   // Set Meta Tags
   $meta_title_inner = "Change My Password | " . $company_name; 
   $meta_keywords_inner = "Change My Password " . $company_name; 
   $meta_description_inner = "Change My Password " . $company_name; 
?>

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-members-portal')        
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post">            
            <h1 class="blog-post-title">Change My Password</h1>                                                           
            @include('flash::message')
                        
				<form id="frmRegister" method="POST" action="{{ url('') }}/members-portal/save-password">
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">				    				    
				    
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Current Password *</label>
						<div class="col-md-9">
							<input type="password" class="form-control" name="passwordCurrent" placeholder="Your current password" value="{{ old('passwordCurrent') }}" />
							@if ($errors->has('passwordCurrent'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('passwordCurrent') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
										
					<div class="form-group row">
						<label class="col-md-3 col-form-label">New Password *</label>
						<div class="col-md-9">
							<input type="password" class="form-control" name="password" placeholder="Your new password" />
							
							<div id="progressBarDiv">
								Strength
								<div class="progress mt-2" id="progressBar">
									<div class="progress-bar bg-secondary progress-bar-animate" style="width: 100%"></div>
								</div>
							</div>
							
							@if ($errors->has('password'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('password') }}</div>
                                </div>
                            @endif												
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Confirm New Password *</label>
						<div class="col-md-9">
							<input type="password" class="form-control" name="passwordConfirm" placeholder="Confirm your new password" />
						</div>           
					</div>
										
					<div class="form-group row">
						<div class="col-md-9 offset-md-3">
							<button type="submit" class="btn-checkout" name="signup" value="Sign up">Submit</button>
						</div>
					</div>
				</form>
     
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->        
@endsection

                        
@section('scripts')                                                                        
<script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
<script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.4.2/zxcvbn.js"></script>
@endsection

@section('inline-scripts')

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e) { 			
		const strongPassword = function() {
			return {
				validate: function(input) {
					// input.value is the field value
					// input.options are the validator options

					const value = input.value;
					if (value === '') {
						return {
							valid: true,
						};
					}

					const result = zxcvbn(value);
					const score = result.score;
					const message = result.feedback.warning || 'The password is weak';

					// By default, the password is treat as invalid if the score is smaller than 3
					// We allow user to change this number via options.minimalScore
					const minimalScore = input.options && input.options.minimalScore
										? input.options.minimalScore
										: 3;

					if (score < minimalScore) {
						return {
							valid: false,
							// Yeah, this will be set as error message
							message: message,
							meta: {
								// This meta data will be used later
								score: score,
							},
						}
					}
				},
			};
		};
			        
		const form = document.getElementById('frmRegister');
		FormValidation.formValidation(form, {
			fields: {				
				passwordCurrent: {
					validators: {
						notEmpty: {
							message: 'The current password is required'
						}
					}
				},        				
				password: {
					validators: {
						notEmpty: {
							message: 'The new password is required'
						},
						checkPassword: {
                                message: 'The password is too weak',
                                minimalScore: 2,
                            }
					}
				},        
				passwordConfirm: {
					validators: {
						notEmpty: {
							message: 'The new password confirmation is required'
						},
						identical: {
                            compare: function() {
                                return form.querySelector('[name="password"]').value;
                            },
                            message: 'The password and its confirm are not the same'
                        }
					}
				},       								
			},
			plugins: {
				trigger: new FormValidation.plugins.Trigger(),
				bootstrap: new FormValidation.plugins.Bootstrap(),
				submitButton: new FormValidation.plugins.SubmitButton(),
				defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				icon: new FormValidation.plugins.Icon({
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh',
				}),
                },
            }
        )
        .registerValidator('checkPassword', strongPassword)
		
        .on('core.validator.validating', function(e) {
            if (e.field === 'password' && e.validator === 'checkPassword') {				
                document.getElementById('progressBarDiv').style.opacity = '1';				
            }
        })
        .on('core.validator.validated', function(e) {
            if (e.field === 'password' && e.validator === 'checkPassword') {
                const progressBar = document.getElementById('progressBar');

                if (e.result.meta) {
                    // Get the score which is a number between 0 and 4
                    const score = e.result.meta.score;
                    
                    // Update the width of progress bar
                    const width = (score == 0) ? '1%' : score * 25 + '%';
                    progressBar.style.opacity = 1;
                    progressBar.style.width = width;
                } else {
                    progressBar.style.opacity = 0;
                    progressBar.style.width = '0%';
                }
            }
        });		
});
</script>

@endsection