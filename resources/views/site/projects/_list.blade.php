<?php
// Set Meta Tags
$meta_title_inner = "Projects" . ($category_name != "Latest Work" ? " - " . str_replace(" &", "", $category_name) : "") . " | Echo3 Media";
$meta_keywords_inner = "Echo3 Media, projects";
$meta_description_inner = "Projects" . ($category_name != "Latest Work" ? " - " . str_replace(" &", "", $category_name) : "") . " -  Echo3 Media";
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')
    @include('site/partials/sidebar-projects')
   
    <div id="blog-masthead" class="blog-masthead blog-projects">
        <div class="container">              
			          
			@if(isset($items))
				 <div class="projectsWrapper">
			        @if ($category_name == "Latest Work")         
					   <h1>{!! $category_name !!}</h1>						
					@endif
			   
				   <div class="container-fluid p-0">
					  <div class="row no-gutters">         
						 @foreach($items as $item)       	 
							  <div class="col-lg-6 no-gutters projectsRow">
								   <a href="{{ url('') }}/projects/{{ $item->category->slug }}/{{ $item->slug }}" class="home-projects-item-a" >
								   <div class="home-projects-item">	
									   @if (strtolower($item->title) == "personaleyes | laser eye surgery")				   		                        					      
										 <div class="home-projects-item-video">
											<iframe src="https://player.vimeo.com/video/374081225?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
										 </div>
									   @elseif (count($item->images) > 0)						   							   				          
										   <img src="{{ url('') }}/{{$item->images[0]->location}}" alt="{{ $item->title }}" />							     
									   @endif

									   <div class="home-projects-item-overlay">
										  <!-- <div class="home-projects-item-overlay-txt">
											 {{ $item->title }}								     
										  </div> -->

										  <div class="home-projects-item-overlay-txt2">
											 <div class="home-projects-item-overlay-line1">
												{{ $item->title }}
												<div class="home-projects-item-overlay-line2"></div>		
											 </div>								     					     
											 <div class="home-projects-item-overlay-line3">{!! $item->short_description !!}</div>	
										  </div>
									   </div>					   					 					   					 

								   </div>
								   </a>
							  </div>
						 @endforeach 	

						</div>

						<!-- Pagination -->
						<div id="pagination">{{ $items->links() }}</div>
				   </div>
				</div>
			@endif
          
   
        </div><!-- /.container -->
    </div>
@endsection