<?php
// Set Meta Tags
$meta_title_inner = $project_item->meta_title;
$meta_keywords_inner = $project_item->meta_keywords;
$meta_description_inner = $project_item->meta_description;
?>

@extends('site/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/baguettebox.js/src/baguetteBox.css') }}">
@endsection

@section('content')

    @include('site/partials/carousel-inner')

    <div id="blog-masthead" class="blog-masthead">
        <div class="container">
              
			<div class="blog-masthead-hdr" data-aos="zoom-in-up" data-aos-duration="2000">		
				<h1>{{ $project_item->title }}</h1>			
			</div>

            <div class='row no-gutters'>			
			   <div class='col-12'>
				  {!! $project_item->description !!}
			   </div>

			   @if (count($project_item->images) > 0 && $project_item->slug != "infinivox")				   
				   @foreach($project_item->images as $image)           																					
					  <div class='col-lg-4 col-sm-12 project-imgs'>
						 <img src="{{ url('') }}{{$image->location}}" alt="{{$project_item->title}}" title="{{$project_item->title}}" class="card-img-top">																												
					  </div>
					@endforeach					
			   @endif

			</div>

			<div class='btn-back'>
			   <a class='btn-back' href='{{ url('') }}/projects/{{ $project_item->category->slug }}'><i class='fa fa-chevron-left'></i> back</a>	
		   </div>
			       
        </div><!-- /.container -->
    </div>
@endsection			  


@section('scripts')
    <script src="{{ asset('/components/baguettebox.js/src/baguetteBox.js') }}"></script>
@endsection


@section('inline-scripts')
   <script type="text/javascript">
        $(document).ready(function () {       
           baguetteBox.run('.cards-project', { animation: 'slideIn'});
        });
    </script>			
@endsection