<?php
// Set Meta Tags
$meta_title_inner = $location->meta_title;
$meta_keywords_inner = $location->meta_keywords;
$meta_description_inner = $location->meta_description;
?>
@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">
                    <div class="blog-post">                       
                        <div class="row">
                            @if($location->thumbnail)
                                <div class="col-sm-12 col-md-7">
                                    @else
                                        <div class="col-12">
                                            @endif

                                            <div class="location-address">
                                                {{ strip_tags($location->address) }} {{ $location->address2 }}, {{ $location->suburb }} {{ $location->state }} {{ $location->postcode }}
                                            </div>

                                            @if ($location->phone)
                                                <div class="location-address">
                                                    <a href='tel:{{ str_replace(")", "", str_replace("(", "", str_replace(" ", "", $location->phone))) }}'>
                                                        <i class="fa fa-phone"></i> {{$location->phone}}
                                                    </a>

                                                    @if ($location->fax)
                                                        <span class="location-fax"><i class="fas fa-fax"></i> {{ $location->fax }}</span>
                                                    @endif
                                                </div>
                                            @endif

                                            @if ($location->address)
                                                <div class="location-address">
                                                    <a href='mailto:{{ $location->email }}'>
                                                        <i class="fas fa-envelope"></i> {{ $location->email }}
                                                    </a>
                                                </div>
                                            @endif

                                            @if ($location->website != "" && $location->fileName != "")
                                                <div class="location-address">
                                                    <a href='{{ $location->website }}' target='_blank'><i class="fas fa-map-marker-alt"></i> FIND US</a>
                                                    <a class="location-fax" href='{{ $location->fileName }}' target='_blank'><i class="fas fa-directions"></i> DIRECTIONS</a>
                                                </div>
                                            @endif

                                        <!-- @if ($location->phone)<div class="location-appointment">To make an appointment, please call us on <a href="tel:{{ str_replace(' ', '', $location->phone) }}">{{ $location->phone }}</a></div> --> @endif
                                        <!-- <a href="{{ url('') }}{{ $location->fileName }}" target="_blank">Location Details [PDF]</a>-->

                                            @if ($location->directions)
                                                <div class="location-directions">
                                                    <h2>Directions</h2>
                                                    {!! $location->directions !!}
                                                </div>
                                            @endif

                                        </div>

                                        @if($location->thumbnail)
                                            <div class="col-sm-12 col-md-5">
                                                <img src='{{ $location->thumbnail }}' alt='PersonalEYES {{ $location->name }}' title="PersonalEYES {{ $location->name }}"/>
                                            </div>
                                        @endif

                                </div>

                                <div class="location-description">
                                    {!! $location->description !!}
                                </div>

                                @if ($location->map)
                                    <div class="location-map">{!! $location->map !!}</div>
                                @endif

                                <div class='btn-back'>
                                    <a class='btn-back' href='javascript: window.history.go(-1);'><i class='fa fa-chevron-left'></i> back</a>
                                </div>

                                @if (isset($location) && $location->popup_type != "")
                                    @php
                                        $page = $location;
                                    @endphp

                                    @include('site/partials/popup')
                                @endif

                        </div><!-- /.blog-post -->
                    </div><!-- /.blog-main -->
                </div><!-- /.row -->
            </div>
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection
