<?php
// Set Meta Tags
$meta_title_inner = "Meet The Team | Echo3 Media";
$meta_keywords_inner = "Meet the passionate, skilled and lively team behind the scenes at Echo3.";
$meta_description_inner = "Meet the passionate, skilled and lively team behind the scenes at Echo3.";
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div id="blog-masthead" class="blog-masthead">
        <div class="container">
            @if ( $category->description != "")
                 <div class="blog-masthead-hdr" data-aos="zoom-in-up" data-aos-duration="2000">		
                    <h1>Meet Us</h1>
				    {!! $category->description !!}
			    </div>
			@endif
           
            <div class="team-item">
                <div class="row">                                  
					@if($items)                            
						@foreach($items as $item)                              
							<div class="col-lg-4">
								 <div class="team-a">
									 <a href='#'>		
										 <div class="team-div-img">									  							  
											 <div class="div-img">
												<img src="{{ url('') }}/{{ $item->photo }}" alt="{{ $item->name }}" title="{{ $item->name }}"> 
												<img src="{{ url('') }}/{{ str_replace(".jpg", "-animal.jpg", $item->photo) }}" alt="{{ $item->name }}" title="{{ $item->name }}"> 											
											 </div>												
										 </div>

										 <div class="team-txt">
											 <div class="team-name-band-name"><h2>{{ $item->name }} <span>"{{ $item->job_title }}"</span></h2></div>																								<div class="team-name-band-description">{!! $item->short_description !!}</div>																								
										 </div>		
									 </a>										 																	     
								 </div>								     									  										 				 	
							</div>                       
						@endforeach                       
					@endif
                 
                </div><!-- /.row -->
            </div><!-- /.teams -->

        </div><!-- /.container -->
    </div>
@endsection
