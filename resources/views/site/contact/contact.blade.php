<?php
// Set Meta Tags
$meta_title_inner = "Contact Echo3 Media";
$meta_keywords_inner =  "Contact Echo3 Media";
$meta_description_inner = "Contact Echo3 Media";
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')    
        
    <div id="blog-masthead" class="blog-masthead ">
        <div class="container"> 
            <div class="row"> 
                <div class="blog-masthead-content">  
					<div class="blog-masthead-content-menu">     
					   @include('site/partials/sidebar-contact')   
					</div>

					<div class="blog-masthead-content-frm">   									   									
					   <div class="home-form frm-general">								
            
						   <!--[if lte IE 8]>
						   <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
						   <![endif]-->
						   <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						   <script>
						   hbspt.forms.create({
						   portalId: "4830934",
						   formId: "b12e30e2-c3b4-4e86-8915-aa01f52c3968"
						   });
						   </script> 

						   <!--<div class="contact-details">
							  {!! $contact_details !!}
						   </div>-->

					   </div>
					</div>
				</div>
           
            </div><!-- /.row -->     
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->   
    
    @if (isset($page) && $page->popup_type != "")    
        @include('site/partials/popup') 
    @endif
    
@endsection