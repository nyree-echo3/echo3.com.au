<?php
// Set Meta Tags
$meta_title_inner = "Newsletter Echo3 Media";
$meta_keywords_inner =  "Newsletter Echo3 Media";
$meta_description_inner = "Newsletter Echo3 Media";
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')    
        
    <div id="blog-masthead" class="blog-masthead ">
        <div class="container"> 
            <div class="row"> 
                <div class="blog-masthead-content">  
					<div class="blog-masthead-content-menu">     
					   @include('site/partials/sidebar-newsletter')   
					</div>

					<div class="blog-masthead-content-frm">   									   									
					   <div class="home-form frm-general">	
                           <h2>Become an Echo3 insider</h2>							
                           <p>If you’re itching to make change happen, then get ready for epic tactics & the latest offerings to boost your brand & online presence. And don’t worry, you won’t be bombarded with emails. Once a month - you’ll receive great tips that inspire action, pointing you in the right direction.</p>
                           
						   <script type="text/javascript" src="https://forms.ontraport.com/v2.4/include/formEditor/genbootstrap.php?method=script&uid=p2c220110f1&version=1"></script>						  

					   </div>
					</div>
				</div>
           
            </div><!-- /.row -->     
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->   
    
    @if (isset($page) && $page->popup_type != "")    
        @include('site/partials/popup') 
    @endif
    
   
@endsection