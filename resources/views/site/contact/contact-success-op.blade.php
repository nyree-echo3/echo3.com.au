<?php
// Set Meta Tags
$meta_title_inner = "Contact Echo3 Media";
$meta_keywords_inner =  "Contact Echo3 Media";
$meta_description_inner = "Contact Echo3 Media";
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')    
        
    <div id="blog-masthead" class="blog-masthead ">
        <div class="container"> 
            <div class="row"> 
                <div class="blog-masthead-content">  
					<div class="blog-masthead-content-menu">     
					   @include('site/partials/sidebar-contact')   
					</div>

					<div class="blog-masthead-content-frm">   									   														  
					   <div class="hbspt-form">
						  <div class="submitted-message">
							 <p>Thanks for making contact with us.</p>
							 <p>We'll be in touch to schedule your free 30 minute strategy session online or in-person to find out if we're a match made in heaven.</p>

						   </div>							
					   </div>					
					</div>
				</div>
           
            </div><!-- /.row -->     
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->   
    
    @if (isset($page) && $page->popup_type != "")    
        @include('site/partials/popup') 
    @endif
    
   
@endsection