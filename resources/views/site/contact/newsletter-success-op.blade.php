<?php
// Set Meta Tags
$meta_title_inner = "Newsletter Echo3 Media";
$meta_keywords_inner =  "Newsletter Echo3 Media";
$meta_description_inner = "Newsletter Echo3 Media";
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')    
        
    <div id="blog-masthead" class="blog-masthead ">
        <div class="container"> 
            <div class="row"> 
                <div class="blog-masthead-content">  
					<div class="blog-masthead-content-menu">     
					   @include('site/partials/sidebar-newsletter')   
					</div>

					<div class="blog-masthead-content-frm">   									   														   	
					   <div class="hbspt-form">
						  <div class="submitted-message">
						       <h1>Become an Echo3 insider</h1>	
							   <p>Thanks for subscribing!  You’ll receive great tips that inspire action, pointing you in the right direction.  And don’t worry, you won’t be bombarded with emails.</p>					
						   </div>							
					   </div>					
					</div>
				</div>
           
            </div><!-- /.row -->     
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->   
    
    @if (isset($page) && $page->popup_type != "")    
        @include('site/partials/popup') 
    @endif
    
   
@endsection