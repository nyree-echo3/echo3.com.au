<?php
// Set Meta Tags
$meta_title_inner = "Contact Echo3 Media";
$meta_keywords_inner =  "Contact Echo3 Media";
$meta_description_inner = "Contact Echo3 Media";
?>

@extends('site/layouts/app')

@section('content')

@php
   $header1 = $module->nav_headline;
   $header2 = ""; //$module->nav_subheadline;
   
   $formHTML  = '';
   $formHTML .= '<div class="home-form frm-general">';								
            
   $formHTML .= '<!--[if lte IE 8]>';
   $formHTML .= '<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>';
   $formHTML .= '<![endif]-->';
   $formHTML .= '<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>';
   $formHTML .= '<script>';
   $formHTML .= 'hbspt.forms.create({';
   $formHTML .= 'portalId: "4830934",';
   $formHTML .= 'formId: "b12e30e2-c3b4-4e86-8915-aa01f52c3968"';
   $formHTML .= '});';
   $formHTML .= '</script> '; 
			
   $formHTML .= '<div class="contact-details">';
   $formHTML .= $contact_details;
   $formHTML .= ' </div>';
            
   $formHTML .= '</div>';
               
@endphp

<div class="divContact">
   @include('site/partials/carousel-form')
</div>

@endsection  

