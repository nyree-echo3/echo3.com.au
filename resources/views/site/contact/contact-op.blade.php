<?php
// Set Meta Tags
$meta_title_inner = "Contact Echo3 Media";
$meta_keywords_inner =  "Contact Echo3 Media";
$meta_description_inner = "Contact Echo3 Media";
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')    
        
    <div id="blog-masthead" class="blog-masthead ">
        <div class="container"> 
            <div class="row"> 
                <div class="blog-masthead-content">  
					<div class="blog-masthead-content-menu">     
					   @include('site/partials/sidebar-contact')   
					</div>

					<div class="blog-masthead-content-frm">   									   									
					   <div class="home-form frm-general">								
            
						   <script type="text/javascript" src="https://forms.ontraport.com/v2.4/include/formEditor/genbootstrap.php?method=script&uid=p2c220110f2&version=1"></script>

						   <!--<div class="contact-details">
							  {!! $contact_details !!}
						   </div>-->

					   </div>
					</div>
				</div>
           
            </div><!-- /.row -->     
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->   
    
    @if (isset($page) && $page->popup_type != "")    
        @include('site/partials/popup') 
    @endif
    
   
@endsection