@extends('site/layouts/app')

@section('content')

	@include('site/partials/carousel')
	@include('site/partials/index-home')
    @include('site/partials/index-ctas')
    @include('site/partials/index-projects')
	@include('site/partials/index-contact')	
	@include('site/partials/index-testimonials')
	<!--@include('site/partials/index-instagram')-->
	@include('site/partials/index-clients')
	
@endsection

@section('scripts')   
    <script type="text/javascript">	  	
	   $(window).scroll(function(){		   
		   $('#chevron-down').fadeOut();		  
	   });			   
	</script>		
@endsection
