@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">               
        <div class="col-sm-9 blog-main">

          <div class="blog-post">            
            <h1 class="blog-post-title">Heading 1 lorem ipsum</h1>
            
			<p>P Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>

			<blockquote>
			<p>Blockquote Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
			</blockquote>

			<ul>
				<li>List Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
				<li>List Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
			</ul>
			
			<hr />
			<h2>Heading 2 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h2>

			<h3>Heading 3 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h3>
           
            <h4>Heading 4 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h4>
            
            <h5>Heading 5 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h5>
           
            <div class="disclaimer"><p>* As of October 2018</p></div>  
            
            <h2>Extra Styles</h2>
            
            <div class="box-red"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="box-red"><p>Your text goes inside this tag.</p></div>' }}
		    </div>
           
           <div class="box-round-red"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="box-round-red"><p>Your text goes inside this tag.</p></div>' }}
		    </div>
          
            <div class="box-dotted"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="box-dotted"><p>Your text goes inside this tag.</p></div>' }}
		    </div>
           
            <div class="cta-red"><a href="https://www.echo3.com.au/whats-a-new-customer-worth-to-you">Work out your CLV with our handy calculator here</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-red"><a href="https://www.echo3.com.au/whats-a-new-customer-worth-to-you">Work out your CLV with our handy calculator here</a></div>' }}
            </div>
            
            <div class="cta-white"><a href="https://www.echo3.com.au/whats-a-new-customer-worth-to-you">Work out your CLV with our handy calculator here</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-white"><a href="https://www.echo3.com.au/whats-a-new-customer-worth-to-you">Work out your CLV with our handy calculator here</a></div>' }}
            </div>
            
            <!--
            <div class="cta-red"><a href="#modalContactOP">Setup your free strategy call with us now</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-red"><a href="#modalContactOP">Setup your free strategy call with us now</a></div>' }}
            </div>
            
            <div class="cta-red"><a href="#modalNewslettersOP">Subscribe to our news</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-red"><a href="#modalNewslettersOP">Subscribe to our news</a></div>' }}
            </div>
            -->
                        
            <div class="cta-calculator">
				<div class="popups-img"><img alt="Calculator" title="Calculator" src="/media/Popups/slider-calculator.png" /></div>

				<div class="popups-txt">
					<div class="popups-txt-bubble">
					   <h2>How much should I spend on marketing?</h2>					   
					</div>										
				</div>
				
				<div class="popups-cta"><a href="https://www.echo3.com.au/whats-a-new-customer-worth-to-you">Use Our Handy Calculator</a></div>
			</div>			              
            
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-calculator">' }}<br>
				&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div class="popups-img"><img alt="Calculator" title="Calculator" src="/media/Popups/slider-calculator.png" /></div>' }}<br>
				&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div class="popups-txt">' }}<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div class="popups-txt-bubble">' }}<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<h2>How much should I spend on marketing?</h2>' }}<br>					   
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '</div>' }}<br>										
				&nbsp;&nbsp;&nbsp;&nbsp;{{ '</div>' }}<br>				
				&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div class="popups-cta"><a href="https://www.echo3.com.au/whats-a-new-customer-worth-to-you">Use Our Handy Calculator</a></div>' }}<br>
			    {{ '</div>	' }}					
			</div>                             
            
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->


    
@endsection
