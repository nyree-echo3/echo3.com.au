 <div class="home-ctas">   
   <div class="container">
	  <div class="row">         		      	 
			  <div class="col-lg-6 col-border-right">
	               <div data-aos="fade-up" data-aos-duration="2000">
					   <div class="home-ctas-block">
						   <h2>What is a new customer worth to you?</h2>

						   <a href="{{ url('') }}/whats-a-new-customer-worth-to-you" class="home-ctas-a" >
							   <div class="home-ctas-a-block">
								   <div class="home-ctas-a-img">
									  <img src="{{ url('') }}/images/site/homepage-panel-calculator.gif" title="Customer Lifetime Value Calculator" alt="Customer Lifetime Value Calculator" width="604" height="425">
								   </div>

								   <div class="home-ctas-a-txt">Try our handy calculator </div>
							   </div>
						   </a>
					   </div>
				   </div>
			  </div>
			  
			  <div class="col-lg-6">
	               <div data-aos="fade-up" data-aos-duration="2000">
					   <div class="home-ctas-block">
						   <h2>Do you have a business in Manningham or Nillumbik?</h2>

						   <a href="{{ url('') }}/community-support-program-website" class="home-ctas-a" >
							   <div class="home-ctas-a-block">
								   <div class="home-ctas-a-img">
									  <img src="{{ url('') }}/images/site/homepage-panel-opt-in.gif" title="Community Support Program" alt="Community Support Program" width="604" height="425">
								   </div>

								   <div class="home-ctas-a-txt">Find out about our community support program</div>
							   </div>
						   </a>
					   </div>
				   </div>
			  </div>						

		</div>
   </div>
</div>
