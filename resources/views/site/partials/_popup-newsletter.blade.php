<!-- The Modal -->
<div class="modal fade modal-Generic" id="modalNewsletters">
  <div class="modal-dialog modal-lg modal-dialog-centered">	     
      
      <div class="modal-content">
      <div class="container no-gutters">      	 	 
      	 <div class="row no-gutters">     	         	          	          	    
      	    <div class="col-lg-12">
     	    	       	    	 
				  <div class="modal-body">
					<div class="modal-body-body">
                        <div class="modal-btn">
					        <a class="btnMore" id="btnMoreNews" href='#' data-dismiss="modal"><img src="{{ url('') }}/images/site/cross.png" title="Close" alt="Close"></a>
				        </div> 
			        
				        <div  class="home-form">
			                 <img src="{{ url('') }}/images/site/icon-letstalk.png" title="Let's talk!" alt="Let's talk!">  
				             <!--<img src="{{ url('') }}/images/site/icon-becomeanecho3insider.png" title="Become an Echo3 insider" alt="Become an Echo3 insider">  -->
							 <h2 class="home-h2"><span>Become an</span> Echo3 insider</h2>						 
							 <p>If you’re itching to make change happen, then get ready for epic tactics & the latest offerings to boost your brand & online presence.  And don’t worry, you won’t be bombarded with emails. Once a month - you’ll  receive great tips that inspire action, pointing you in the right direction.</p>							 
				        
					        <!-- HubSpot Form - START -->						
							<!--[if lte IE 8]>
							<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
							<![endif]-->
							<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
							<script>
							  hbspt.forms.create({
								portalId: "4830934",
								formId: "90dcee85-db07-49c4-908c-c813e0db0b03"
							});
							</script>   				         
						    <!-- HubSpot Form - END -->		
                        </div>  

					</div><!-- .modal-body-body -->
				  </div><!-- .modal-body -->
				
      	    </div><!-- .col-lg-12 -->
		  </div><!-- .row -->
		</div><!-- .containter -->        	          	
      </div><!-- .modal-content --> 
      </div><!-- .modal-dialog -->   	           
	</div><!-- .modal -->


@section('inline-scripts-popup-newsletter')
	<script type="text/javascript">
		jQuery.noConflict();	
		
		jQuery('a[href$="#modalNewsletters"]').on( "click", function() {		  
		   //jQuery.noConflict();	
		   //$('#modalNewsletters').modal();		   
			
		   jQuery('#modalNewsletters').addClass("show")					
		   jQuery('#modalNewsletters').fadeTo( "fast", 1 );
		   jQuery('#modalNewsletters').show();	
		});
		
		jQuery('#btnMoreNews').on( "click", function(e) {
			jQuery('#modalNewsletters').fadeTo("slow", 0);
			jQuery('#modalNewsletters').hide();

			e.preventDefault();
		});
	</script>

@endsection


@section('inline-scripts-popup-newsletter-inside')
	<script type="text/javascript">
		jQuery.noConflict();	
		
		jQuery('a[href$="#modalNewsletters"]').on( "click", function() {		  
		   //jQuery.noConflict();	
		   //$('#modalNewsletters').modal();		   
			
			jQuery('#modalNewsletters').addClass("show")					
			jQuery('#modalNewsletters').fadeTo( "fast", 1 );
			jQuery('#modalNewsletters').show();
		});
		
		jQuery('#btnMoreNews').on( "click", function(e) {
			jQuery('#modalNewsletters').fadeTo("slow", 0);
			jQuery('#modalNewsletters').hide();

			e.preventDefault();
		});
	</script>

@endsection