<div class="coronavirusUpdates">	    
	<div class='coronavirusUpdates-txt'><i class="fas fa-exclamation-circle"></i> COVID-19 Information for businesses in Nillumbik and Manningham -> <a href="{{url('') }}/community-support-program-website">click here</a></div>
	<div class='coronavirusUpdates-txt-resp'><i class="fas fa-exclamation-circle"></i> COVID-19 Information -> <a href="{{url('') }}/community-support-program-website">click here</a></div>
</div>