@php
    $randomVideo = rand(1,3); 
   
    $header_image = "";
	if (isset($page) && $page->thumbnail != "") {
		$header_image = $page->thumbnail;	 
	} elseif (isset($header_image_ctrl)) {
	    $header_image = $header_image_ctrl;
	} elseif (isset($module)) {   	   	   
		$header_image = $module->header_image;	   
		$category_name = $module->nav_headline;
		$short_description = $module->nav_subheadline;
	}			
	
@endphp

<div class="panelSliders">  	                   	   
	<div class="panelSlider">  
        <div class="vimeo-full-width">
			@if ($randomVideo == 1)
			   <iframe src="https://player.vimeo.com/video/392352247?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="panelSliderVideo"></iframe>
			@elseif ($randomVideo == 2)
			   <iframe src="https://player.vimeo.com/video/392355252?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="panelSliderVideo"></iframe>
			@else
			   <iframe src="https://player.vimeo.com/video/392355568?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="panelSliderVideo"></iframe>
			@endif
		</div>
          
        <div class="panelSliderPanel panelSliderPanel-noAnimation">   
			<div class="panelSlidersImageBackground">      
				<img src="{{ url('') }}/images/site/bg-shape.png" alt="Header Image Background" title="Header Image Background"> 
			</div>

			<div class="panelSlidersImage"> 
				@if ( $header_image != "")            
				   <img src="{{ url('') }}/{{ $header_image }}" alt="Header Image" title="Header Image">
				@endif
			</div>   

			<div class="panelSlidersCaption">                                    
				@if ( $header1 != "")
					<div class="sliderCaption-h1">{!! $header1 !!}<span class="sliderCaption-h1-stop">.</span></div> 
				@endif

				@if ( isset($header2) && $header2 != "")
					<div class="sliderCaption-h2">{!! $header2 !!}</div> 
				@endif

				<div class="sliderCaption-cta">
					{!! $formHTML !!}
				</div>             

			</div> 
		</div> 
	</div>  
	
	<!--<div id="chevron-down" class="chevron-down">
		<a href='#blog-masthead'><img src="{{ url('') }}/images/site/down-arrow.png" title="Down Arrow" alt="Down Arrow"></a>
	</div>-->   		   		
</div>

@section('scripts')   
    <script type="text/javascript">	  	
	   $(window).scroll(function(){		   
		   $('#chevron-down').fadeOut();
	   });			   
	</script>		
@endsection
