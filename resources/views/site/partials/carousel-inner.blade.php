@php
    //$randomVideo = rand(1,3);
    $randomVideo = 0; 
   
    $header_image = "";
	if (isset($page) && $page->thumbnail != "") {
		$header_image = $page->thumbnail;	 
	} elseif (isset($header_image_ctrl)) {
	    $header_image = $header_image_ctrl;
	} elseif (isset($module)) {   	 
		$header_image = $module->header_image;	   
		$category_name = $module->nav_headline;
		$short_description = $module->nav_subheadline;
	}
		
	if (isset($category)) { ; 
	   $category_name = ($category->nav_headline != "" ? $category->nav_headline : $category->name);
	   $short_description = ($category->nav_headline != "" ? $category->nav_subheadline : $category->short_description);	   	
	}	
	
@endphp

<div class="panelSliders">  	                   	   
	<div class="panelSlider panelSliderInner">  
        <div class="vimeo-full-width">
            @if ($randomVideo == 0)  
               <iframe src="https://player.vimeo.com/video/405641534?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="panelSliderVideo"></iframe>
			@elseif ($randomVideo == 1)
			   <iframe src="https://player.vimeo.com/video/392352247?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="panelSliderVideo"></iframe>
			@elseif ($randomVideo == 2)
			   <iframe src="https://player.vimeo.com/video/392355252?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="panelSliderVideo"></iframe>
			@else
			   <iframe src="https://player.vimeo.com/video/392355568?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="panelSliderVideo"></iframe>
			@endif
		</div>
         
        <div class="panelSlidersImageBackground">      
				<img src="{{ url('') }}/images/site/waveyline.gif" alt="Swirly Line" title="Swirly Line"> 
			</div>
          
        <div class="panelSliderPanel">             			  			
			<div class="panelSlidersImage"> 
				@if ( $header_image != "")            
				   <img src="{{ url('') }}/{{ $header_image }}" alt="Header Image" title="Header Image">
				@endif
			</div>   

			<div class="panelSlidersCaption">                                    
				@if ( $category_name != "")
					<div class="sliderCaption-h1">{!! $category_name !!}<span class="sliderCaption-h1-stop">{{ (isset($page_name) && $page_name == "calculator" ? "?" : ".") }}</span></div> 
				@endif

				@if ( isset($short_description) && $short_description != "")
					<div class="sliderCaption-h2">{!! $short_description !!}</div> 
				@endif

				<div class="sliderCaption-cta">
					<a class="sliderCaptionCTA" href="#blog-masthead" role="button">Learn More <img src="{{ url('') }}/images/site/arrow-red.png" title="Arrow" alt="Arrow"></a>
				</div>             

			</div> 
		</div> 
	</div>  
	
	<!--<div id="chevron-down" class="chevron-down">
		<a href='#blog-masthead'><img src="{{ url('') }}/images/site/down-arrow.png" title="Down Arrow" alt="Down Arrow"></a>
	</div>-->   		   		
</div>

@section('scripts')   
    <script type="text/javascript">	  	
	   $(window).scroll(function(){		   
		   $('#chevron-down').fadeOut();
	   });			   
	</script>		
@endsection
