<div class="blog-sidebar sidebar-page">	     
	<div class="sidebar-module" data-aos="zoom-in-up" data-aos-duration="2000">
       @if ($page->icon != "")
		   <div class="page-icon">
			  <img src="{{ url('') }}/{{ $page->icon }}" alt="{{ $page->title }}" title="{{ $page->title }}">			  
		   </div>
	   @endif
	   
	   <h1>{{ $page->title }}</h1>

	   <ul>				   
		   {!! $side_nav !!}			   						
	   </ul>

	   <div id="chevron-down" class="chevron-down">
			<a href='#next'><img src="{{ url('') }}/images/site/down-arrow.png" title="Down Arrow" alt="Down Arrow"></a>
		</div>			   					   					   				
	</div>				
</div>
