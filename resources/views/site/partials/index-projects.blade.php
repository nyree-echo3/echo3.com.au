@if(isset($home_projects))
	 <div class="home-projects">
	   <div class="container-fluid p-0">
		  <div class="row no-gutters">         
			 @foreach($home_projects as $item)       	 
				  <div class="col-lg-6 no-gutters">
	                   <a href="{{ url('') }}/projects/{{ $item->category->slug }}/{{ $item->slug }}" class="home-projects-item-a" >
		               <div class="home-projects-item">		
		                   @if (strtolower($item->title) == "personaleyes | laser eye surgery")				   		                        					      
								 <div class="home-projects-item-video">
								 	<iframe src="https://player.vimeo.com/video/374081225?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
								 </div>
						   @elseif (count($item->images) > 0)						         					          
								<img src="{{ url('') }}/{{$item->images[0]->location}}" alt="{{ $item->title }}" title="{{ $item->title }}" width="950" height="486" />					      								 
						   @endif
						   
						   <div class="home-projects-item-overlay">
					          <div class="home-projects-item-overlay-txt">
						         {{ $item->title }}								     
							  </div>
							  
							  <div class="home-projects-item-overlay-txt2">
						         <div class="home-projects-item-overlay-line1">
						            {{ $item->title }}
						            <div class="home-projects-item-overlay-line2"></div>		
						         </div>								     					     
							     <div class="home-projects-item-overlay-line3">{!! $item->short_description !!}</div>	
							  </div>
						   </div>					   					 					   					 

						   <!--<p><a class="btn btn-secondary" href="{{ url('') }}/projects/{{ $item->category->slug }}/{{ $item->slug }}" role="button">View details &raquo;</a></p>-->
					   </div>
					   </a>
				  </div>
			 @endforeach 	

			</div>
	   </div>
	</div>
@endif