<div class="home-instagram">
    <div class="container"> 
       <div data-aos="zoom-in-right" data-aos-duration="2000">	 
		   <h2 class="home-h2"><span>What we're up to on</span> instagram</h2>
	   </div>
      
       <div class="row" id="instagram-panel-embedded">
       	
       </div>
	</div>
</div>
		
@section('inline-scripts-instagram')  
	<script src="{{ url('') }}/js/site/jquery-1.22.4.min.js"></script>

	<!-- Instagram Feed https://github.com/BanNsS1/jquery.instagramFeed -->
	<script src="{{ url('') }}/js/site/jquery.instagramFeed-v2.js?v=1.0"></script>

	
	<script>	
			(function($){		
				$(window).on('load', function(){
					$.instagramFeed({
						'username': 'echo3media',
						'container': "#instagram-panel-embedded",
						'display_profile': false,
						'display_biography': false,
						'display_gallery': true,
						'get_raw_json': false,
						'callback': null,
						'styling': false,
						'items': 20,
						'items_per_row': 5,
						'margin': 1 
					});

				});

			})(jQuery);				
		
		$( "#btnInstagramMore" ).click(function(event) {						
			$.instagramFeed({
				'username': 'echo3media',
				'container': "#instagram-panel-embedded2",
				'display_profile': false,
				'display_biography': false,
				'display_gallery': true,
				'get_raw_json': false,
				'callback': null,
				'styling': false,
				'items': 20,
				'items_per_row': 5,
				'margin': 1 
			});
			
			
			event.preventDefault();
		});
		
	</script>
@endsection