<nav>    
  <div class="navWrapper">
       <div class="navClose">
			<button id="btnNavClose" class="custom-toggler hamburger hamburger--collapse hamburger--accessible js-hamburger " type="button">
			  <span class="hamburger-box">
				<span class="hamburger-inner"></span>
			  </span>
			</button>
		</div>
  
	  <ul>
	    {!! $navigation !!} 	 
	  </ul>
  </div>
</nav>


@section('inline-scripts-nav')
    <script type="text/javascript"> 
		var $hamburger = $(".hamburger");
		
		$( "#btnNavClose" ).click(function() {
			$( ".btnNav" ).toggleClass( "is-active" );
        	$( "nav" ).removeClass( "show-menu" );
        });
    </script>
@endsection