@if ($side_nav != "")
	<div class="blog-sidebar">
	    <div class="container">	
	        <div class="blog-masthead-hdr" data-aos="zoom-in-up" data-aos-duration="2000">		
				<h1>{!! $category->name !!}</h1>			
			</div>
				        
			<div class="sidebar-module" data-aos="zoom-in-up" data-aos-duration="2000">
			   <ul>				   
				   {!! $side_nav !!}			   						
			   </ul>	   				
			</div>
		</div>
	</div>
@endif