<div class="container-fluid p-0 h-100" id="next"> 
	<div class="row align-items-center m-0 p-0 h-100 panel-meeting"> 			

		<div class="col-lg-6 col-md-12 p-0 m-0">
			<div class="panel-meeting-txt">
				<h2>Do you need an out-sourced marketing team that's more like an internal marketing department?</h2>

				<p>Well, that’s what we do. Embedding ourselves into our client’s businesses and actively using our full team of digital specialists on their behalf every day.</p>
				<p>At the cost of a single employee, our clients get access to their own team of 6 specialists spanning web development, digital marketing and design, copywriting and SEO. Just imagine what your business would look like with these skills at your finger tips every day!</p>
			</div>
		</div>

		<div class="col-lg-6 col-md-12 p-0 m-0">

			<div id="parallax" class="parallaxParent parallax-container">   
				<div class="parallax-img parallax-img-main">
					<div class="parallax-img-img"></div>		
				</div>					
			</div>				

		</div>

	</div>    
</div>						
													
<script type="text/javascript" src="{{ asset('/js/site/parallex/TweenMax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/ScrollMagic.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/animation.gsap.js') }}"></script>										
<script>
	// init controller
	var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

	// build scenes				
	new ScrollMagic.Scene({triggerElement: "#parallax"})
					.setTween("#parallax > div", {y: "80%", ease: Linear.easeNone})										
					.addTo(controller);	
</script>
