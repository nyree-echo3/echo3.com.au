@section('styles')
    <link rel="stylesheet" href="{{ asset('css/site/popup.css') }}">
@endsection
   
<!-- Modal - Popup -->

@php
   switch ($page->popup_position)  {
      case "top-left":
      case "bottom-left": 
         $slideout = "left";
         break;
         
      default:
         $slideout = "right";
         break;   
   }
@endphp

<div class="modal fade modal" id="modal-popups" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false" data-focus="false">
   <div class="modal-{{ $page->popup_position }} modal-inner">
   <div class="modal-btn">
	  <a class="" href='#' id="btnPopupClose" data-dismiss="modal"><img src="{{ url('') }}/images/site/popup-cross.png" title="Close" alt="Close"></a>
  </div>	

  <div class="modal-dialog modal-dialog-slideout modal-dialog-slideout-{{ $slideout }} modal-md" role="document">
	 <div class="modal-content">     
		<div class="modal-body">        
			{!! $popup->form !!}
		</div>      
	 </div>
   </div>
   </div>
</div>

@section('inline-scripts-popups')
    <script type="text/javascript">
        $(document).ready(function () {
			jQuery.noConflict();	
			
			setTimeout(function() { 
				
				startTime = {{ $page->popup_start }} * 60000; // Convert minutes to milliseconds
				endTime = {{ $page->popup_end }} * 60000; // Convert minutes to milliseconds			

				setTimeout(function(){			  
				   //$('#modal-popups').modal('show');
				   jQuery('#modal-popups').addClass("show")					
		   		   jQuery('#modal-popups').fadeTo( "slow", 1 );
		   		   jQuery('#modal-popups').show("slow");	
				}, startTime);  

				setTimeout(function(){			
				   //$('#modal-popups').modal('hide');
				   jQuery('#modal-popups').addClass("hide")					
		   		   jQuery('#modal-popups').fadeTo( "slow", 0 );
		   		   jQuery('#modal-popups').hide("slow");		
				}, endTime);
			
			    jQuery('#btnPopupClose').on( "click", function(e) {
					jQuery('#modal-popups').fadeTo("slow", 0);
					jQuery('#modal-popups').hide("slow");

					e.preventDefault();
				});
			
			}, 3000); //setTimeout		
        });
    </script>
@endsection	