@if (isset($project_categories))
	<div class="blog-sidebar">
	    <div class="container">	        
			<div class="sidebar-module" data-aos="zoom-in-up" data-aos-duration="2000">
			   <ul>	
			       @foreach($project_categories as $item)
			       
			           <li class="list-group-item {{ ($category_slug == $item->slug ? "active" : "") }}">
			              <a class="navsidebar" href="{{ url('').'/'.$item->url }}">
		                  
			                  <div class="menu-name">
		                          <span class="menu-img-resp">
		                          	 <img src="{{ url('').'/'.$item->icon }}" alt="{{ $item->name }}" title="{{ $item->name }}">
		                          </span>
		                          
			                      @if ($category_slug == $item->slug)
			                          <h1>{{ $item->name }}</h1>
			                      @else
			                          {{ $item->name }}
			                      @endif
			                  </div>
			                  
			                  @if ($item->icon != "")
								  <div class="menu-img">
									   <img src="{{ url('').'/'.$item->icon }}" alt="{{ $item->name }}" title="{{ $item->name }}">
								  </div>
			                  @endif
			                  
			                  <div class="menu-desc">
			                      {!! $item->description !!}
							  </div>
		              
		                      <div class="menu-more">
			                      View More
			                      <img src="{{ url('') }}/images/site/arrow-red.png" alt="Arrow" title="Arrow">
							  </div>
		              
			              </a>
			           </li>			   
			       @endforeach				   
			   </ul>	   				
			</div>
		</div>
	</div>
@endif