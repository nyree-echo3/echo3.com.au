<!-- The Modal -->
<div class="modal fade modal-Generic" id="modalContact">
  <div class="modal-dialog modal-lg modal-dialog-centered">	     
      
      <div class="modal-content">
      <div class="container no-gutters">      	 	 
      	 <div class="row no-gutters">     	         	          	          	    
      	    <div class="col-lg-12">
     	    	       	    	 
				  <div class="modal-body">
					<div class="modal-body-body">
                        <div class="modal-btn">
					        <a class="btnMore" id="btnMore" href='#' data-dismiss="modal"><img src="{{ url('') }}/images/site/cross.png" title="Close" alt="Close"></a>
				        </div> 
			        
				        <div  class="home-form">
				             <img src="{{ url('') }}/images/site/icon-letstalk.png" title="Let's talk!" alt="Let's talk!">  
							 <h2 class="home-h2"><span>Let's</span> talk!</h2>							 
							 <p>Schedule a free 30 minute strategy session online or in-person to find out if we're a match made in heaven.</p>							 
				        
					        <!-- HubSpot Form - START -->						
							<!--[if lte IE 8]>
							<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
							<![endif]-->
							<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
							<script>
							hbspt.forms.create({
							portalId: "4830934",
							formId: "b12e30e2-c3b4-4e86-8915-aa01f52c3968"
							});
							</script>  						         
						    <!-- HubSpot Form - END -->		
                        </div>  

					</div><!-- .modal-body-body -->
				  </div><!-- .modal-body -->
				
      	    </div><!-- .col-lg-12 -->
		  </div><!-- .row -->
		</div><!-- .containter -->        	          	
      </div><!-- .modal-content --> 
      </div><!-- .modal-dialog -->   	           
	</div><!-- .modal -->


@section('inline-scripts-popup-contact')
	<script type="text/javascript">
		jQuery.noConflict();
		
		jQuery('a[href$="#modalContact"]').on( "click", function() {		  
		   //jQuery.noConflict();	
		   jQuery('#modalContact').modal();		   
		});
		
		jQuery('a[href$="#modalContactHome"]').on( "click", function() {		  
		   //jQuery.noConflict();	
		   //jQuery('#modalContact').modal();		   
			
		   jQuery('#modalContact').addClass("show")					
		   jQuery('#modalContact').fadeTo( "fast", 1 );
		   jQuery('#modalContact').show();	
		});
		
		jQuery('#btnMore').on( "click", function(e) {
			jQuery('#modalContact').fadeTo("fast", 0);
			jQuery('#modalContact').hide();

			e.preventDefault();
		});
	</script>

@endsection