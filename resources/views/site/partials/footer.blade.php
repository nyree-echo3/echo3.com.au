<footer class='footer'> 
    <div class="footer-txt-top">
		<div class="d-flex flex-row justify-content-center">

			<div class="px-5 align-self-baseline">                                                                                   				                                
				@if ( $social_facebook != "")<a href="{{ $social_facebook }}" target="_blank" class="footer-txt-top-social"><img src="{{ url('') }}/images/site/icon-social-fb.gif" title="Facebook" alt="Facebook" width="31" height="31"></a> @endif
				@if ( $social_linkedin != "")<a href="{{ $social_linkedin }}" target="_blank" class="footer-txt-top-social"><img src="{{ url('') }}/images/site/icon-social-linkdin.gif" title="LinkedIn" alt="LinkedIn" width="31" height="31"></a> @endif     
				@if ( $social_instagram != "")<a href="{{ $social_instagram }}" target="_blank" class="footer-txt-top-social"><img src="{{ url('') }}/images/site/icon-social-insta.gif" title="Instagram" alt="Instagram" width="31" height="31"></a> @endif
				
				<div class='footer-newsletters'>
				<a href="{{ url('') }}/newsletter">Subscribe to our news</a>
				</div>
			</div> 

			<div class="px-5 align-self-baseline">                                                                                   
			<a href='https://www.google.com/maps?cid=6138776143076794837' target='_blank'><img src="{{ url('') }}/images/site/google-reviews.png" title="Google My Business Reviews" alt="Google My Business Reviews" width="84" height="42"></a>					
			</div>                       

			@if(count($navigation_footer))                            
				@foreach($navigation_footer as $nav_key => $nav_item)
					@if ($nav_item->display && $nav_item->text!='Articles')
						<div class="px-5 align-self-baseline">                                    							
							<a href="{{ url('') }}/{{ $nav_item->href }}">{{ $nav_item->text }}</a>														                                 
						</div>
					@endif
				@endforeach
			@endif		
		</div>
	</div>
	         	
	<div class="footer-txt-bot">
		<div class="footer-txt-bot-trad">Echo3 is located on the traditional lands of the Wurundjeri of the Kulin nation. We give our respect to the Elders past and present.</div>            
		<a href="{{ url('') }}" target="_blank">&copy; {{ date('Y') }} {{ $company_name }}</a> | 
		<a href="{{ url('') }}/campaign-login" rel=”nofollow”>Campaign Login</a>
		<!--<a href="{{ url('') }}/privacy" rel=”nofollow”>Privacy</a> | <a href="{{ url('') }}/terms-conditions" rel=”nofollow”>Terms</a>-->                

	</div>	
	
</footer>