<!-- The Modal -->
<div class="modal fade modal-Generic" id="modalContactOP">
  <div class="modal-dialog modal-lg modal-dialog-centered">	     
      
      <div class="modal-content">
      <div class="container no-gutters">      	 	 
      	 <div class="row no-gutters">     	         	          	          	    
      	    <div class="col-lg-12">
     	    	       	    	 
				  <div class="modal-body">
					<div class="modal-body-body">
                        <div class="modal-btn">
					        <a class="btnMore" id="btnMoreOP" href='#' data-dismiss="modal"><img src="{{ url('') }}/images/site/cross.png" title="Close" alt="Close"></a>
				        </div> 
			        
				        <div class="home-form">
				             <img src="{{ url('') }}/images/site/icon-letstalk.png" title="Let's talk!" alt="Let's talk!">  
							 <h2 class="home-h2"><span>Let's</span> talk!</h2>							 
							 <p>Schedule a free 30 minute strategy session online or in-person to find out if we're a match made in heaven.</p>							 
				        
				             <div class="ontraport-form">


<!-- Ontraport Form - HTML Version -->
<link rel="stylesheet" href="//app.ontraport.com/js/formeditor/moonrayform/paymentplandisplay/production.css" type="text/css" /><link rel="stylesheet" href="https://optassets.ontraport.com/opt_assets/css/form.default.min.css" type="text/css" /><link rel="stylesheet" href="https://forms.ontraport.com/v2.4/include/formEditor/gencss.php?uid=p2c220110f2" type="text/css" /><script type="text/javascript" src="https://forms.ontraport.com/v2.4/include/formEditor/genjs-v3.php?html=false&uid=p2c220110f2"></script><div class="moonray-form-p2c220110f2 ussr"><div class="moonray-form moonray-form-label-pos-stacked">
<form class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-text"><label for="mr-field-element-926043839469" class="moonray-form-label">Hi echo3, I'm...</label><input name="firstname" type="text" class="moonray-form-input" id="mr-field-element-926043839469" required value="" placeholder="enter your name"/></div>
<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-email"><label for="mr-field-element-364850070480" class="moonray-form-label">and I can be contacted at...</label><input name="email" type="email" class="moonray-form-input" id="mr-field-element-364850070480" required value="" placeholder="name@company.com.au"/></div>
<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-textarea"><label for="mr-field-element-469537582558" class="moonray-form-label">I'm interested in...</label><textarea name="notes" class="moonray-form-input" id="mr-field-element-469537582558" placeholder="enter your question here"></textarea></div>
<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-element-captcha"><script src="https://www.google.com/recaptcha/api.js" async defer></script><div class="g-recaptcha" data-callback="OPCapcha_filled" data-expired-callback="OPCapcha_expired" data-sitekey="6LfvVlsUAAAAAP1mfegQjrmI6YPQz3zWJ44123To"></div></div>
<div class="moonray-form-element-wrapper moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit"><input type="submit" name="submit-button" value="Let's get started!" class="moonray-form-input" id="mr-field-element-142652266554" src/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="afft_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="aff_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="sess_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="ref_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="own_" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="oprid" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="contact_id" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_source" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_medium" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_term" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_content" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_campaign" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="referral_page" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="_op_gclid" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="_op_gcid" type="hidden" value=""/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="uid" type="hidden" value="p2c220110f2"/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="uniquep2c220110f2" type="hidden" value="0"/></div>
<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="mopsbbk" type="hidden" value="01D62193FEE892ED84DD04BC:FDE5824AD818E7B136F5C471"/></div>
</form></div></div>								 
<!-- End Ontraport Form -->
						 
						 
							 </div>
                        </div>  

					</div><!-- .modal-body-body -->
				  </div><!-- .modal-body -->
				
      	    </div><!-- .col-lg-12 -->
		  </div><!-- .row -->
		</div><!-- .containter -->        	          	
      </div><!-- .modal-content --> 
      </div><!-- .modal-dialog -->   	           
	</div><!-- .modal -->


@section('inline-scripts-popup-contact-op')
	<script type="text/javascript">
		//$( document ).ready(function() {		
	    //    setTimeout(function() {
		
				jQuery.noConflict();	

				jQuery('a[href$="#modalContactOP"]').on( "click", function() {		  				   
					jQuery('#modalContactOP').addClass("show")					
					jQuery('#modalContactOP').fadeTo( "slow", 1 );
					jQuery('#modalContactOP').show();
				   
					//jQuery('#modalContactOP').modal();		   
				});

				jQuery('a[href$="#modalContactHome"]').on( "click", function() {		  
				   //jQuery.noConflict();	
				   //jQuery('#modalContactOP').modal();		   
				});
				
				jQuery('#btnMoreOP').on( "click", function(e) {
					jQuery('#modalContactOP').fadeTo("slow", 0);
					jQuery('#modalContactOP').hide();
					
					e.preventDefault();
				});
				
		//	}, 3000); //setTimeout	
		//});	
	</script>

@endsection