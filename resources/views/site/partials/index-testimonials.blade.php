@php
   $API_KEY = 'AIzaSyDL3TK-KHIaeOQBUUEn9NGsb8o7jypGpJM';
	
	$placeId = "ChIJk3lBpRpH1moR1SG8o2BQMVU"; // PlaceId for Echo3.  Go to https://developers.google.com/places/place-id
	
	$placeSearchURL = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" . $placeId . "&key=" . $API_KEY;
	$placeSearchJSON = file_get_contents($placeSearchURL);
	$googlePlaces = json_decode($placeSearchJSON);			
@endphp

<div class="home-testimonials">
    <div class="container">
		<div data-aos="fade-down" data-aos-duration="2000">	
			<img src="{{ url('') }}/images/site/icon-ourclientsshowinglove.gif" title="Our clients showing love" alt="Our clients showing love" width="202" height="205">  
			<h2 class="home-h2"><span>Our</span> clients<span>showing</span> <i class="fas fa-heart"></i></h2>
		</div>
		
		<div class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				@php
				   $counter = 0;
				   $gReviews = $googlePlaces->result->reviews;
				   shuffle($gReviews);
				@endphp
				   
				@foreach ( $gReviews as $reviews )    
					@php
					   $counter++;  
					@endphp

					<div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">					
						<p>{{ $reviews->text }}</p>
						<p class="person"><i class="fas fa-user"></i> {{ $reviews->author_name }}</p>
												
	                    <div class='reviews-rating'>
	                        Google {{ number_format($reviews->rating, 1) }}
	                        
							@for ($stars = 0; $stars < $reviews->rating; $stars++)
								<img src='{{ url('') }}/images/site/review-star.png' class='reviews-star' alt='Google Review Star' title='Google Review Star' width="75" height="75">   
							@endfor	
					    </div>
				    </div>
				@endforeach

			</div>
			
		</div>
		
		<div class='reviews-link'><a href='https://www.google.com/maps?cid=6138776143076794837' target='_blank'>More Google Reviews <i class="fas fa-long-arrow-alt-right"></i></a>
	</div>
</div>