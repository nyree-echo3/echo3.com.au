<div class="blog-sidebar sidebar-page">	     
	<div class="sidebar-module" data-aos="zoom-in-up" data-aos-duration="2000">      
		   <div class="page-icon">
			  <img src="{{ url('') }}/images/site/icon-contact.png" alt="Contact Us" title="Contact Us">			  
		   </div>	  
	   
	   <h1>Contact Us</h1>	
	   
	   <div class="contact-details">
		  {!! $contact_details !!}
	   </div>  		   					   					   				
	</div>				
</div>