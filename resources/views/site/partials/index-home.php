<div id="home-intro" class="home-intro">
    <div class="container">
        <div class="row">            
            <div class="col-lg-9">
                <div class="home-intro-main">
				   <div data-aos="zoom-in-up" data-aos-duration="2000">		
                       <?php echo $home_intro_text; ?>
				   </div>	   
				</div>                                 
            </div>            
        </div>                                                
    </div>
</div>
