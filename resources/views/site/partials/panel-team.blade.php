<div class="panel-meet-team">
	<div id="blog-masthead" class="blog-masthead ">
		<div class="container h-100 p-0"> 
			<div class="row align-items-center h-100 m-0"> 			

				<div class="col-lg-5 col-md-12 p-0">					
					<h2>Meet the team</h2>

					<p>We're a bit of a quirky bunch, heralding from all over the globe. We never say no to a slice of cake and are never too busy for old fashioned chat.</p>
					<a href="https://www.echo3.com.au/team">Have a stickybeak at our team ></a>	 				
				</div>

				<div class="col-lg-7 col-md-12 p-0">
					<p class="box-img"><img alt="Team" title="Team" src="/images/site/team-sketch-NEW.gif" /></p>
				</div>

			</div>    
		</div>
	</div>
</div>