@if (isset($home_clients))
	<div class="home-clients">
		<div data-aos="zoom-in-right" data-aos-duration="2000">	
		   <h2 class="home-h2"><span>Our</span> clients</h2>
		</div>
		
		<div class="container">
			<div class="row">         
				@foreach($home_clients as $item)       	 
					<div class="col-md-3 col-3 col-sm-2 py-4">																		
						<img src="{{ url('') }}/{{ $item->image }}" alt="{{ $item->name }}" title="{{ $item->name }}" class="home-clients-img" width="328" height="131">					
					</div>
				@endforeach 	
			</div>
		</div>
	</div>
@endif