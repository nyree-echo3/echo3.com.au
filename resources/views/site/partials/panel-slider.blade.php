<link rel="stylesheet" href="{{ asset('css/site/panel-slider.min.css') }}?v1.05">

<!-- Carousel -->
<div id="carouselAbout" class="carousel slide carousel-fade" data-ride="carousel" data-interval="10000">
  <div class="carousel-inner">
   
   <!-- Item 1 -->
    <div class="carousel-item active">
      <div class="mask flex-center">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
              <h2>What we <span>value</span><span class='fullstop'>.</span></h2>
              <p>Customer commitment, quality, integrity, teamwork & respect mean we develop relationships that make a positive difference in our customers' lives, while delivering premium value to our customers. Here are the values we strive for...<a href="#carouselAbout" data-slide="next">More ></a></p>	
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="slider-img">
            	   <img src="{{ asset('/images/site/values1.gif') }}" class="mx-auto" alt="slide" title="slide">
            	   <img src="{{ asset('/images/site/bg-shape1.gif') }}" class="mx-auto" alt="slide" title="slide">
            	</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Item 2 -->
    <div class="carousel-item">
      <div class="mask flex-center">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
              <h2>To be <span>authentic</span><span class='fullstop'>.</span></h2>
              <p>We're about as transparent as it comes - without being saucy. We pride ourselves on straight talking (no baffling techi-lingo or long winded auto-generated reports here). <a href="#carouselAbout" data-slide="next">More ></a></p>
            </div>
            
            <div class="col-lg-6 col-md-12">
            	<div class="slider-img">
            	   <img src="{{ asset('/images/site/values2.gif') }}" class="mx-auto" alt="slide" title="slide">
            	   <img src="{{ asset('/images/site/bg-shape2.gif') }}" class="mx-auto" alt="slide" title="slide">
            	</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Item 3 -->
    <div class="carousel-item">
      <div class="mask flex-center">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
              <h2><span>To have a</span> positive impact<span class="fullstop">.</span></h2>
              <p>We're a fun-loving bunch and we vow to use our superpowers for good, not evil. Every day is an opportunity to uplift our clients and their businesses... to have a positive impact in our community and also, within our own team. <a href="#carouselAbout" data-slide="next">More ></a></p>
            </div>
            
            <div class="col-lg-6 col-md-12">
            	<div class="slider-img">
            	   <img src="{{ asset('/images/site/values3.gif') }}" class="mx-auto" alt="slide" title="slide">
            	   <img src="{{ asset('/images/site/bg-shape3.gif') }}" class="mx-auto" alt="slide" title="slide">
            	</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Item 4 -->
    <div class="carousel-item">
      <div class="mask flex-center">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
              <h2><span>To be</span> innovative<span class="fullstop">.</span></h2>
              <p>To look at a problem and find the best route to success takes a special type of approach. It takes teamwork, creativity and strong understanding of the vast array of available technologies. We call that fun. <a href="#carouselAbout" data-slide="next">More ></a></p>
            </div>
            
            <div class="col-lg-6 col-md-12">
            	<div class="slider-img">
            	   <img src="{{ asset('/images/site/values4.gif') }}" class="mx-auto" alt="slide" title="slide">
            	   <img src="{{ asset('/images/site/bg-shape4.gif') }}" class="mx-auto" alt="slide" title="slide">
            	</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Item 5 -->
    <div class="carousel-item">
      <div class="mask flex-center">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
              <h2><span>To have a</span> big heart<span class="fullstop">.</span></h2>
              <p>Living and working by our internal compass - our social and environmental conscience bonds us as a team. <a href="#carouselAbout" data-slide="next">More ></a></p>
            </div>
            
            <div class="col-lg-6 col-md-12">
            	<div class="slider-img">
            	   <img src="{{ asset('/images/site/values5.gif') }}" class="mx-auto" alt="slide" title="slide">
            	   <img src="{{ asset('/images/site/bg-shape5.gif') }}" class="mx-auto" alt="slide" title="slide">
            	</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Item 6 -->
    <div class="carousel-item">
      <div class="mask flex-center">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
              <h2><span>To be</span> sustainable<span class="fullstop">.</span></h2>
              <p>Every day we're actively reducing our footprint and helping our clients do the same. We&rsquo;re proudly carbon-neutral and are working towards a full suite of carbon-neutral suppliers. <a href="#carouselAbout" data-slide="next">More ></a></p>
            </div>
            
            <div class="col-lg-6 col-md-12">
            	<div class="slider-img">
            	   <img src="{{ asset('/images/site/values6.gif') }}" class="mx-auto" alt="slide" title="slide">
            	   <img src="{{ asset('/images/site/bg-shape6.gif') }}" class="mx-auto" alt="slide" title="slide">
            	</div>
            </div>
          </div>
        </div>
      </div>
    </div>
 
    <!-- Item 7 -->
    <div class="carousel-item">
      <div class="mask flex-center">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
              <h2><span>To</span> give back<span class="fullstop">.</span></h2>
              <p>We champion equality and are active in our community, working pro-bono for good causes, supporting small businesses, and community initiatives that need to be heard. <a href="#carouselAbout" data-slide="next">More ></a></p>
            </div>
            
            <div class="col-lg-6 col-md-12">
            	<div class="slider-img">
            	   <img src="{{ asset('/images/site/values7.gif') }}" class="mx-auto" alt="slide" title="slide">
            	   <img src="{{ asset('/images/site/bg-shape7.gif') }}" class="mx-auto" alt="slide" title="slide">
            	</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Item 8 -->
    <div class="carousel-item">
      <div class="mask flex-center">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
              <h2><span>Let's</span> do this<span class="fullstop">!</span></h2>
              <p>We adore working with like-minded businesses, so if your brand is environmentally- and socially-aware (or wants to be), we want to hang out.</p>
            </div>
            
            <div class="col-lg-6 col-md-12">
            	<div class="slider-img">
            	   <img src="{{ asset('/images/site/values8.gif') }}" class="mx-auto" alt="slide" title="slide">
            	   <img src="{{ asset('/images/site/bg-shape8.gif') }}" class="mx-auto" alt="slide" title="slide">
            	</div>
            </div>
          </div>
        </div>
      </div>
    </div>
                                             
    </div> <!-- .carousel-inner -->  
</div> <!-- .carousel -->