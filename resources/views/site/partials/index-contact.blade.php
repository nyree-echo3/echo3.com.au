<div  class="home-contact">
    <div class="container">
		
       <div data-aos="fade-down" data-aos-duration="2000">		
			<img src="{{ url('') }}/images/site/icon-letstalk.gif" title="Let's talk!" alt="Let's talk!" width="245" height="176">  
			<h2 class="home-h2"><span>Let's</span> talk!</h2>
        </div>
		
        <p class="home-contact-bold">Our team specialise in getting real results for businesses just like yours. Why not schedule a free 30 minute strategy session online or in-person to find out if we're a match made in heaven.</p>
        
        <a href='{{ url('') }}/contact'>Let's Get Started <i class="fas fa-long-arrow-alt-right"></i></a>                                                                      
    </div>
</div>
