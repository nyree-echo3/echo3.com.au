<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark btco-hover-menu navbar-custom affix" data-toggle="affix">
    <div class="panelMax">
        <div class="mobile-contact-icon d-lg-none">
            <a href="{{ url('locations/contact-details') }}"><i class="fas fa-envelope"></i></a>
        </div>

        <div class="navbar-logo">
            <a href="{{ url('') }}" title="{{ $company_name }}">                
                <img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}" class="logo-lg">                
                <img src="{{ url('') }}/images/site/logo-sm.png" title="{{ $company_name }}" alt="{{ $company_name }}" class="logo-sm">
            </a>
        </div>        

        <div class="navbar-top">
            @include('site/partials/helper-contact')
        </div>

        <button class="navbar-toggler custom-toggler hamburger hamburger--collapse hamburger--accessible js-hamburger " type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	  <span class="hamburger-box">
		<span class="hamburger-inner"></span>
	  </span>
        </button>

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {{ (!isset($page_type) ? "active" : "") }}">
                    <a class="nav-link" href="{{ url('') }}"><i class='fa fa-home'></i><span class="sr-only">(current)</span></a>
                </li>
                {!! (isset($navigation_override) ? $navigation_override : $navigation) !!}                             
            </ul>
        </div>
    </div>   
</nav>


@section('inline-scripts-navigation')
    <script type="text/javascript">
        var $hamburger = $(".hamburger");
        $hamburger.on("click", function (e) {
            $hamburger.toggleClass("is-active");
            // Do something else, like open/close menu
        });
    </script>
@endsection