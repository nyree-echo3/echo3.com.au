<div  class="home-newsletter">
    <div class="container">
        <div data-aos="fade-down" data-aos-duration="2000">	
			<img src="{{ url('') }}/images/site/icon-becomeanecho3insider.png" title="Become an Echo3 insider" alt="Become an Echo3 insider">          
			<h2 class="home-h2 home-h2-white"><span>Become an</span> Echo3 insider</h2>
		</div>
       
        <p class="home-contact-bold">If you’re itching to make change happen, then get ready for epic tactics & the latest offerings to boost your brand & online presence.  And don’t worry, you won’t be bombarded with emails. Once a month - you’ll  receive great tips that inspire action, pointing you in the right direction.</p>
        
        <div  class="home-form">
			<!--[if lte IE 8]>
			<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
			<![endif]-->
			<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
			<script>
			  hbspt.forms.create({
				portalId: "4830934",
				formId: "90dcee85-db07-49c4-908c-c813e0db0b03"
			});
			</script>   
		</div>                                                           
    </div>
</div>
