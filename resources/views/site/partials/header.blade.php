<header data-toggle="affix">

    @include('site/partials/preview')
    
<div class="panelHeader">   
    <div class="panelHeaderMenu">
		<button id="btnNav" class="custom-toggler hamburger hamburger--collapse hamburger--accessible js-hamburger " type="button">
		  <span class="hamburger-box">
			<span class="hamburger-inner"></span>
		  </span>
		</button>
	</div>
	
	<div class="panelHeaderLogo">        	
	   <a href="{{ url('') }}" title="{{ $company_name }}">                
		  <img src="{{ url('') }}/images/site/logo.gif" title="{{ $company_name }}" alt="{{ $company_name }}" width="95" height="95">                	
	   </a>	
	</div>     
	
	<div class="panelHeaderContact">        	
	   <a href="{{ url('') }}/contact" title="Email Us">                
		  <img src="{{ url('') }}/images/site/icon-email.png" title="Email Us" alt="Email Us" width="41" height="42">                	
	   </a>	
	   
	   <a href="tel:{{ str_replace(' ', '', $phone_number) }}" title="Phone Us">                
		  <img src="{{ url('') }}/images/site/icon-tel.png" title="Phone Us" alt="Phone Us" width="42" height="41">                	
	   </a>	
	</div>      		 		
</div>
</header>

@include('site/partials/nav')

@section('inline-scripts-header')
    <script type="text/javascript">
        var $hamburger = $(".hamburger");
        $hamburger.on("click", function (e) {
            $hamburger.toggleClass("is-active");
            // Do something else, like open/close menu
			//$( "nav" ).removeClass( "hide-menu" );
			$( "nav" ).toggleClass( "show-menu" )
        });		
    </script>
@endsection