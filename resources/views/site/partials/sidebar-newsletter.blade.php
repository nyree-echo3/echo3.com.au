<div class="blog-sidebar sidebar-page">	     
	<div class="sidebar-module" data-aos="zoom-in-up" data-aos-duration="2000">      
		   <div class="page-icon">
			  <img src="{{ url('') }}/images/site/icon-letstalk.png" alt="Newsletter" title="Newsletter">			  
		   </div>	  
	   
	   <h1>Newsletter</h1>	
	   
	   <div class="contact-details">
		  {!! $contact_details !!}
	   </div>  		   					   					   				
	</div>				
</div>