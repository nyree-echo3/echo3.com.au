@php
   //$randomVideo = rand(1,3);  
   $randomVideo = 0;
@endphp

<div class="panelSliders">     	           		   	           	   
	<div class="panelSlider">                      
        <div class="vimeo-full-width">
            @if ($randomVideo == 0)
               <iframe src="https://player.vimeo.com/video/405641534?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="panelSliderVideo"></iframe>
			@elseif ($randomVideo == 1)
			   <iframe src="https://player.vimeo.com/video/392352247?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="panelSliderVideo"></iframe>
			@elseif ($randomVideo == 2)
			   <iframe src="https://player.vimeo.com/video/392355252?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="panelSliderVideo"></iframe>
			@else
			   <iframe src="https://player.vimeo.com/video/392355568?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="panelSliderVideo"></iframe>
			@endif
		</div>
    
        <div class="panelSliderPanel">
			<div class="panelSlidersImageBackground">      
				<img src="{{ url('') }}/images/site/bg-shape-with-shadow.png" alt="{{ $image->title }} title="{{ $image->title }}" width="1731" height="950"> 
			</div>

			<div class="panelSlidersImage">             
				<img src="{{ url('') }}/{{ $image->location }}" alt="{{ $image->title }}" title="{{ $image->title }}" width="1225" height="984">
			</div>   

			<div class="panelSlidersCaption">                                    
				@if ( $image->title != "")
					<div class="sliderCaption-h1">
					   {!! $image->title !!}				  
					   <span class="sliderCaption-h1-stop">.</span>								   
					</div> 
				@endif

				@if ( $image->description != "")
					<div class="sliderCaption-h2">{!! $image->description !!}</div> 
				@endif

				<div class="sliderCaption-cta">
					<a class="sliderCaptionCTA" href="{{ $image->url }}" role="button">{{ $image->urlname }} <img src="{{ url('') }}/images/site/arrow-red.png" title="Arrow" alt="Arrow" width="31" height="5"></a>
				</div>                         
			</div> 
		</div> 
	</div>     
	
	<div id="chevron-down" class="chevron-down">
		<a href='#home-intro'><img src="{{ url('') }}/images/site/down-arrow.png" title="Down Arrow" alt="Down Arrow" width="58" height="58"></a>
	</div>
</div>