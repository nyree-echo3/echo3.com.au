<div class="col-sm-3  blog-sidebar">
	<div class="sidebar-module">
		<h4>Members Portal</h4>	
		<ol class="list-group list-unstyled list-group-flush">
			<li>My Details</li>
			   <ul>
			   	  <li><a href="{{ url('') }}/members-portal/change-details">Change My Details</a></li>
			   	  <li><a href="{{ url('') }}/members-portal/change-password">Change My Password</a></li>
			   </ul>
			   
		    <li><a href="{{ url('') }}/logout">Logout</a></li>	   
		</ol>	
	</div>
</div>