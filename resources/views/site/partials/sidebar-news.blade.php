<div class="blog-sidebar sidebar-page">	     
	<div class="sidebar-module" data-aos="zoom-in-up" data-aos-duration="2000">
       @if ($category_icon != "")
		   <div class="page-icon">
			  <img src="{{ url('') }}/{{ $category_icon }}" alt="{{ $category_name }}" title="{{ $category_name }}">			  
		   </div>
	   @endif
	   
	   <h1>{{ $category_name }}</h1>

       @if (sizeof($side_nav) > 1) 
		   <ul>	
			   @foreach($side_nav as $item) 		

				   <li class="list-group-item {{ ($category_slug == $item->slug ? "active" : "") }} {{ (sizeof($side_nav) == 1 ? "active" : "") }}">
					  <a class="navsidebar" href="{{ url('').'/'.$item->url }}">

						  <div class="menu-name">
							  <span class="menu-img-resp">
								 <img src="{{ url('').'/'.$item->icon }}" alt="{{ $item->name }}" title="{{ $item->name }}">
							  </span>

							  {{ $item->name }}

						  </div>

						  @if ($item->icon != "")
							  <div class="menu-img">
								   <img src="{{ url('').'/'.$item->icon }}" alt="{{ $item->name }}" title="{{ $item->name }}">
							  </div>
						  @endif

						  <!--<div class="menu-desc">
							  {!! $item->description !!}
						  </div>-->

						  @if ($item->icon != "")
							  <div class="menu-more">
								  Learn More
								  <img src="{{ url('') }}/images/site/arrow-red.png" alt="Arrow" title="Arrow">
							  </div>
						  @endif

					  </a>
				   </li>			   
			   @endforeach		
		   </ul>
	   @endif	  		   					   					   				
	</div>				
</div>