<div class="home-instagram">
    <div class="container">   
        <div data-aos="zoom-in-right" data-aos-duration="2000">	 
		   <h2 class="home-h2"><span>What we're up to on</span> instagram</h2>
		</div>
		
		 <div class="instagramPanel">
		   <div id="instagram-panel-embedded" class="carousel-instagram-embedded"
			   data-cycle-pause-on-hover="true"
			   data-cycle-fx="carousel"
			   data-cycle-timeout="2000"
			   data-cycle-carousel-visible="5"
			   data-cycle-carousel-fluid="true"
			   data-cycle-slides="> span"
		   >
		   </div>
		</div>
	</div>
</div>

@section('inline-scripts-instagram')  
	<script src="{{ url('') }}/js/site/jquery-1.22.4.min.js"></script>

	<!-- Instagram Feed https://github.com/BanNsS1/jquery.instagramFeed -->
	<script src="{{ url('') }}/js/site/jquery.instagramFeed.js?v=1.2"></script>

	<!-- Slideshow Cycle http://jquery.malsup.com/cycle2/demo/-->
	<script type="text/javascript" src="{{ url('') }}/js/site/jquery.cycle2.js"></script>
	<script src="{{ url('') }}/js/site/jquery.cycle2.carousel.min.js"></script>

	<script>
		//$( document ).ready(function() {
			$('#instagram-panel-embedded').cycle(); 

			$.fn.cycle.defaults.autoSelector = '.carousel-instagram-embedded';

			(function($){		
				$(window).on('load', function(){
					$.instagramFeed({
						'username': 'echo3media',
						'container': "#instagram-panel-embedded",
						'display_profile': false,
						'display_biography': false,
						'display_gallery': true,
						'get_raw_json': false,
						'callback': null,
						'styling': false,
						'items': 20,
						'items_per_row': 5,
						'margin': 1,
						'lazy_load' : true,
						'image_size' : 240,
						'max_tries' : 8,
						'host' : 'https://images' + ~~(Math.random() * 3333) + '-focus-opensocial.googleusercontent.com/gadgets/proxy?container=none&url=https://www.instagram.com/',
					});

				});

			})(jQuery);		
		//});	
	</script>
@endsection